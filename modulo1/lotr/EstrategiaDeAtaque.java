import java.util.*;
public class EstrategiaDeAtaque implements Estrategia {
    private TipoEstrategia estrategia;
    public void alterarEstrategia( TipoEstrategia tipoEstrategia )
    {
        this.estrategia = tipoEstrategia;
    }
    
    public boolean estaVivo( Elfo elfo ) {
        return elfo.getVida() > 0;
    }
    
    public ArrayList<Elfo> getNoturnosPorUltimo( ArrayList<Elfo> atacantes ) {
        ArrayList<Elfo> noturnoAtras = new ArrayList<>();
        for( Elfo elfo : atacantes ) {
            if( estaVivo( elfo ) ) {
                if( elfo instanceof ElfoNoturno ) { //se ElfoNoturno eh um elfo
                    for( int i = noturnoAtras.size()-1; i < noturnoAtras.size(); i ++ ) {
                        noturnoAtras.add(elfo);
                    }
                }
                else {
                    return null;
                }
            }
        }
        return noturnoAtras;
    }
        
    public boolean elfosAptos( Elfo elfo ) {
        return elfo.getVida() > 0 && elfo.getQtdFlecha() > 0;
    }
    
    public ArrayList<Elfo> getPersonalizado( ArrayList<Elfo> atacantes ) {
        ArrayList<Elfo> listaPersonalizados = new ArrayList<>();
        for( Elfo elfo : atacantes ) {
            if( elfosAptos( elfo ) ) {
                if( elfo instanceof ElfoNoturno ) { //se ElfoNoturno eh um elfo
                    for( int i = 0; i < listaPersonalizados.size(); i ++ ) {
                        listaPersonalizados.add(elfo);
                    }
                } else {
                    return null;
                }
            }
        }
        double quantidadeElfosAptos = listaPersonalizados.size() * ( 30 / 100 );
        return listaPersonalizados;
    }
    
    public ArrayList<Elfo> getOrdemDeAtaque(ArrayList<Elfo> atacantes) {
        if( estrategia.NOTURNOS_ULTIMOS == estrategia.NOTURNOS_ULTIMOS ) {
            atacantes = getNoturnosPorUltimo( atacantes );
            return atacantes;
        }
        return null;
    }
}