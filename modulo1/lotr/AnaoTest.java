import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class AnaoTest {
    
    @Test
    public void anaoDeveNascerCom110DeVida() {
        Anao anao = new Anao( "Gimli" );
        assertEquals( 110.0, anao.getVida(), 0.00000001 );
        assertEquals( Status.RECEM_CRIADO, anao.getStatus() );
    }
    
    @Test
    public void anaoSofreDanoEFicaCom100DeVida() {
        Anao anao = new Anao( "Gimli" );
        anao.sofrerDano();
        assertEquals( 100.0, anao.getVida(), 0.00000001 );
        assertEquals( Status.SOFREU_DANO, anao.getStatus() );
    }
    
    @Test
    public void anaoSofreDano12VezesEFicaCom0DeVida() {
        Anao anao = new Anao( "Gimli" );
        for( int i = 0; i < 12; i++ ) {
            anao.sofrerDano();
        }
        assertEquals( 0.0, anao.getVida(), 0.00000001 );
        assertEquals( Status.MORTO, anao.getStatus() );
    }
    
    @Test
    public void anaoNasceComEscudoNoInventario() {
        Anao anao = new Anao( "Gimli" );
        Item esperado = new Item( 1, "Escudo" );
        Item resultado = anao.getInventario().obter(0);
        assertEquals( esperado, resultado );
    }
    
    @Test
    public void anaoEquipadoTomaMetadeDeDano() {
        Anao anao = new Anao( "Gimli" );
        anao.esquiparEDesequiparEscudo();
        anao.sofrerDano();
        assertEquals( 105.0, anao.getVida(), 0.00001 );
    }
    
    @Test
    public void anaoNaoEquipadoETomaDanoIntegral() {
        Anao anao = new Anao( "Gimli" );
        anao.sofrerDano();
        anao.sofrerDano();
        assertEquals( 105.0, anao.getVida(), 0.00001 );
    }
}