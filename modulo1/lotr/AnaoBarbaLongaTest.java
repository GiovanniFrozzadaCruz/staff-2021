import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
public class AnaoBarbaLongaTest
{
    @Test
    public void anaoDevePerderVida66PorCento() {
        DadoFalso dado = new DadoFalso();
        dado.simularValor( 4 );
        AnaoBarbaLonga balin = new AnaoBarbaLonga( "Balin", dado );
        balin.sofrerDano();
        assertEquals( 100.0, balin.getVida(), 1e-8 );
    }
    
    @Test
    public void anaoDevePerderVida33PorCento() {
        DadoFalso dado = new DadoFalso();
        dado.simularValor( 5 );
        AnaoBarbaLonga balin = new AnaoBarbaLonga( "Balin", dado );
        balin.sofrerDano();
        assertEquals( 110.0, balin.getVida(), 1e-8 );
    }
}