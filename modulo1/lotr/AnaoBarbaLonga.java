import java.util.*;
public class AnaoBarbaLonga extends Anao {
    private final ArrayList<String> VALORES_SORTEADOS = new ArrayList(Arrays.asList(2,6));
    private DadoD6 dado;
    private Sorteador sorteador;
    
    public AnaoBarbaLonga( String nome ) {
        super( nome );
        this.sorteador = new DadoD6();
    }
    
    public AnaoBarbaLonga( String nome, Sorteador sorteador ) {
        super( nome );
        this.sorteador = sorteador;
    }
    
    public void sofrerDano( int valor) {
        boolean devePerderVida = sorteador.sortear() <= 4;
        if( devePerderVida ) {
            super.sofrerDano();
        }
    }
}