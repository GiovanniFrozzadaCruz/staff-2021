import java.util.*;
public class ElfoDaLuz extends Elfo {
    private final double QTD_VIDA_GANHA;
    private int qtdAtaques;
   
    private final ArrayList<String> DESCRICOES_VALIDAS = new ArrayList<>(
        Arrays.asList(
            "Espada de Galvorn"
        )
    );
    
    {
        this.qtdAtaques = 0;
        this.QTD_VIDA_GANHA = 10.0;
    }
    
    public ElfoDaLuz( String nome ) {
        super( nome );
        qtdDano = 21;
        super.ganharItem( new ItemSempreExiste( 1, DESCRICOES_VALIDAS.get( 0 ) ) );
        
    }
    
    private boolean devePerderVida() {
        return qtdAtaques % 2 == 1;
    }
    
    private Item getEspada() {
        return this.getInventario().buscar( DESCRICOES_VALIDAS.get( 0 ) );
    }
    
    public void ganharVida() {
        super.vida += QTD_VIDA_GANHA;
    }
    
    public void atacarComEspada( Anao anao ) {
        Item espada = getEspada();
        if( espada.getQuantidade() > 0 ) {
            qtdAtaques++;
            anao.sofrerDano();
            if( this.devePerderVida()  ) 
                sofrerDano();
            else
                ganharVida(); 
        }
    }
       
}