public abstract class Personagem {
    protected int experiencia, qtdExperienciaPorAtaque;
    protected Inventario inventario;
    protected Status status;
    protected double vida;
    protected String nome;
    protected double qtdDano;
    
    {
        this.status = Status.RECEM_CRIADO;
        this.inventario = new Inventario();
        this.qtdExperienciaPorAtaque = 1;
        this.experiencia = 0;
        this.qtdDano = 0.0;
    }
    
    public Personagem( String nome ) {
        this.nome = nome;
    }
    
    public String getNome() {
        return this.nome;
    }
    
    public void setNome( String nome ) {
        this.nome = nome;
    }
    
    public int getExperiencia() {
        return this.experiencia;
    }
    
    public double getVida() {
        return this.vida;
    }
    
    public Status getStatus() {
        return this.status;
    }
    
    public Inventario getInventario() {
        return this.inventario;
    }
    
    public void ganharItem( Item item ) {
        this.inventario.adicionar( item );
    }
    
    public void removeItem( Item item ) {
        this.inventario.remover( item );
    }
    
    private void aumentarXP() {
        this.experiencia += qtdExperienciaPorAtaque;
    }
    
    private Status validacaoStatus() {
        return this.vida == 0 ? Status.MORTO : Status.SOFREU_DANO;
    }
        
    private boolean podeSofrerDano( ) {
        return this.vida > 0;
    }
    
    protected void sofrerDano() {
        if( this.podeSofrerDano() ) {
            this.vida -= qtdDano >= this.qtdDano ? this.qtdDano : 0;
            this.status = this.validacaoStatus();
        }
    }
    
    public abstract String imprimirONomeDaClase();
}