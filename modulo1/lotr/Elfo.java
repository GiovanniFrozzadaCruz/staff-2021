public class Elfo extends Personagem {
    private int indiceFlecha = 1;
    
    {
        this.indiceFlecha = 1;
    }
    
    public Elfo( String nome ) {
        super( nome );
        this.vida = 100.0;
        this.inventario.adicionar(new Item( 1, "Arco" ));
        this.inventario.adicionar(new Item( 2, "Flecha" ));
    }
        
    public Item getFlecha() {
        return this.inventario.obter(indiceFlecha);
    }
    
    protected int getQtdFlecha() {
        return this.getFlecha().getQuantidade();
    }
    
    private boolean podeAtirar() {
        return this.getQtdFlecha() > 0;
    }
    
    private void aumentarXP() {
        this.experiencia += qtdExperienciaPorAtaque;
    }
    
    public void atirarFlecha( Anao anao ) {
        if( this.podeAtirar() ){
            this.getFlecha().setQuantidade( this.getQtdFlecha() - 1 );
            this.aumentarXP();
            super.sofrerDano();
            anao.sofrerDano();
        }
    }
    
    public void atirarMultiplasFlechas( int vezes, Anao anao ) {
        for( int i = 0; i < vezes; i++ ){
            this.atirarFlecha(anao);
        }
    }
    
    public String imprimirONomeDaClase() {
        return "Elfo";
    }
}