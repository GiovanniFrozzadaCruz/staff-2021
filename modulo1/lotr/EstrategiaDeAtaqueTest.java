import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;
public class EstrategiaDeAtaqueTest
{
    @Test
    public void getNoturnosPorUltimo() {
        EstrategiaDeAtaque estrategiasDeAtaque = new EstrategiaDeAtaque();
        estrategiasDeAtaque.alterarEstrategia(TipoEstrategia.NOTURNOS_ULTIMOS);
        ArrayList<Elfo> atacantes = new ArrayList<>();
        Elfo verde1 = new ElfoVerde("Legolas");
        Elfo noturno1 = new ElfoNoturno("Legolas");
        Elfo verde2 = new ElfoVerde("Legolas");
        Elfo noturno2 = new ElfoNoturno("Legolas");
        Elfo noturno3 = new ElfoNoturno("Legolas");
        Elfo verde3 = new ElfoVerde("Legolas");
        Elfo verde4 = new ElfoVerde("Legolas");
        Elfo noturno4 = new ElfoNoturno("Legolas");
        Elfo verde5 = new ElfoVerde("Legolas");
        Elfo noturno5 = new ElfoNoturno("Legolas");
        atacantes.add(verde1);
        atacantes.add(noturno1);
        atacantes.add(verde2);
        atacantes.add(noturno2);
        atacantes.add(noturno3);
        atacantes.add(verde3);
        atacantes.add(verde4);
        atacantes.add(noturno4);
        atacantes.add(verde5);
        atacantes.add(noturno5);
        
        ArrayList<Elfo> elfosEsperados = new ArrayList<>(
             Arrays.asList(verde5, verde4, verde3, verde2,verde1, noturno1, noturno2, noturno3, noturno4, noturno5)
        );
        ArrayList<Elfo> atacantesOrdenados = estrategiasDeAtaque.getOrdemDeAtaque(atacantes);
        assertEquals(elfosEsperados, atacantesOrdenados);
    }
}
