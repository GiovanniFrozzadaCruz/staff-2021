public interface Tradutor
{
    String traduzir( String textoEmPortugues );
    int validador( String validar );
}