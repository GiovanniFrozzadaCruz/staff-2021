/*
//60c0c461d510e6d3e397e916
db.pais.insert({
        nome: "Inglaterra"
});
db.pais.find();

//60c0c4dcd510e6d3e397e917
db.estado.insert({
        pais: ObjectId("60c0c461d510e6d3e397e916"),
        nome: "Condado de Surrey"
});
db.estado.find();

//60c0c53dd510e6d3e397e918
db.cidade.insert({
        estado: ObjectId("60c0c4dcd510e6d3e397e917"),
        nome: "Little Whinging"
});

//60c0c667d510e6d3e397e919
db.bairro.insert({
        estado: ObjectId("60c0c4dcd510e6d3e397e917"),
        cidade: ObjectId("60c0c53dd510e6d3e397e918"),
        nome: "condado",
});

//60c0c6f0d510e6d3e397e91a
db.endereco.insert({
        logradouto: "Rua dos Algeneiros",
        numero: "4",
        complemento: "do jardim bonito",
        bairro: ObjectId("60c0c667d510e6d3e397e919"),
        cidade: ObjectId("60c0c53dd510e6d3e397e918"),
        estado: ObjectId("60c0c4dcd510e6d3e397e917"),
        pais: ObjectId("60c0c461d510e6d3e397e916"),
});
db.endereco.find();

//ADICIONANDO CLIENTE
//60c0c75cd510e6d3e397e91b
db.cliente.insert({
        cpf: "12345678911",
        nome: "Harry Potter",
        estadoCivil: "Casado",
        dataNascimento: "31/07/1980",
        endereco: ObjectId("60c0c6f0d510e6d3e397e91a")
});
db.cliente.find();

//60c0c797d510e6d3e397e91c
db.movimentacao.insert({
    tipo: "debito",
    valor: 2500.0
});
db.movimentacao.find();

//60c0ca18d510e6d3e397e91d
db.gerente.insert({
        cpf: "12345678912",
        nome: "Dumbledore",
        estadoCivil: "Solteiro",
        dataNascimento: "16/01/1881",
        codigoFuncionario: "0001",
        tipoGerente: "GC",
        endereco: ObjectId("60c0c6f0d510e6d3e397e91a")
});
db.gerente.find();


//60c0ca7fd510e6d3e397e91e
db.conta.insert({
        codigo: "0001",
        tipo: "PJ",
        saldo: 9999999,
        gerente: [
        ObjectId("60c0ca18d510e6d3e397e91d")
        ],
        cliente: [
        ObjectId("60c0c75cd510e6d3e397e91b"),
        ObjectId("60c0c75cd510e6d3e397e91b"),
        ObjectId("60c0c75cd510e6d3e397e91b")
        ],
        movimentacao: [
        ObjectId("60c0c797d510e6d3e397e91c")
        ]
});
db.conta.find();

//60c0cb15d510e6d3e397e91f
db.consolidacao.insert({
    saldoAtual: 999999999,
    saques: 1000,
    depositos: 500,
    numeroCorrentistas: 10
});
db.consolidacao.find();


db.agencia.insert([
    {
        //60c0cc0ad510e6d3e397e920
        codigo: "0001",
        nome: "Web",
        endereco: ObjectId("60c0c6f0d510e6d3e397e91a"),
        consolidacao: [
            ObjectId("60c0cb15d510e6d3e397e91f"),
            ObjectId("60c0cb15d510e6d3e397e91f"),
            ObjectId("60c0cb15d510e6d3e397e91f"),
            ObjectId("60c0cb15d510e6d3e397e91f"),
            ObjectId("60c0cb15d510e6d3e397e91f")
        ],
        conta: [
            ObjectId("60c0ca7fd510e6d3e397e91e"),
            ObjectId("60c0ca7fd510e6d3e397e91e"),
            ObjectId("60c0ca7fd510e6d3e397e91e"),
            ObjectId("60c0ca7fd510e6d3e397e91e"),
            ObjectId("60c0ca7fd510e6d3e397e91e"),
        ]
    },
    {
        //60c0cc0ad510e6d3e397e921
        codigo: "0002",
        nome: "California",
        endereco: ObjectId("60c0c6f0d510e6d3e397e91a"),
        consolidacao: [
            ObjectId("60c0cb15d510e6d3e397e91f"),
            ObjectId("60c0cb15d510e6d3e397e91f"),
            ObjectId("60c0cb15d510e6d3e397e91f"),
            ObjectId("60c0cb15d510e6d3e397e91f"),
            ObjectId("60c0cb15d510e6d3e397e91f")
        ],
        conta: [
            ObjectId("60c0ca7fd510e6d3e397e91e"),
            ObjectId("60c0ca7fd510e6d3e397e91e"),
            ObjectId("60c0ca7fd510e6d3e397e91e"),
            ObjectId("60c0ca7fd510e6d3e397e91e"),
            ObjectId("60c0ca7fd510e6d3e397e91e"),
        ]
        
    },
    {
        //60c0cc0ad510e6d3e397e922
        codigo: "0101",
        nome: "Londres",
        endereco: ObjectId("60c0c6f0d510e6d3e397e91a"),
        consolidacao: [
            ObjectId("60c0cb15d510e6d3e397e91f"),
            ObjectId("60c0cb15d510e6d3e397e91f"),
            ObjectId("60c0cb15d510e6d3e397e91f"),
            ObjectId("60c0cb15d510e6d3e397e91f"),
            ObjectId("60c0cb15d510e6d3e397e91f")
        ],
        conta: [
            ObjectId("60c0ca7fd510e6d3e397e91e"),
            ObjectId("60c0ca7fd510e6d3e397e91e"),
            ObjectId("60c0ca7fd510e6d3e397e91e"),
            ObjectId("60c0ca7fd510e6d3e397e91e"),
            ObjectId("60c0ca7fd510e6d3e397e91e"),
        ]
        
    },
    {
        //60c0cc0ad510e6d3e397e923
        codigo: "0001",
        nome: "Web",
        endereco: ObjectId("60c0c6f0d510e6d3e397e91a"),
        consolidacao: [
            ObjectId("60c0cb15d510e6d3e397e91f"),
            ObjectId("60c0cb15d510e6d3e397e91f"),
            ObjectId("60c0cb15d510e6d3e397e91f"),
            ObjectId("60c0cb15d510e6d3e397e91f"),
            ObjectId("60c0cb15d510e6d3e397e91f")
        ],
        conta: [
            ObjectId("60c0ca7fd510e6d3e397e91e"),
            ObjectId("60c0ca7fd510e6d3e397e91e"),
            ObjectId("60c0ca7fd510e6d3e397e91e"),
            ObjectId("60c0ca7fd510e6d3e397e91e"),
            ObjectId("60c0ca7fd510e6d3e397e91e"),
        ]
        
    },
    {
        //60c0cc0ad510e6d3e397e924
        codigo: "0001",
        nome: "Web",
        endereco: ObjectId("60c0c6f0d510e6d3e397e91a"),
        consolidacao: [
            ObjectId("60c0cb15d510e6d3e397e91f"),
            ObjectId("60c0cb15d510e6d3e397e91f"),
            ObjectId("60c0cb15d510e6d3e397e91f"),
            ObjectId("60c0cb15d510e6d3e397e91f"),
            ObjectId("60c0cb15d510e6d3e397e91f")
        ],
        conta: [
            ObjectId("60c0ca7fd510e6d3e397e91e"),
            ObjectId("60c0ca7fd510e6d3e397e91e"),
            ObjectId("60c0ca7fd510e6d3e397e91e"),
            ObjectId("60c0ca7fd510e6d3e397e91e"),
            ObjectId("60c0ca7fd510e6d3e397e91e"),
        ]
        
    },
    {
        //60c0cc0ad510e6d3e397e925
        codigo: "8761",
        nome: "Itu",
        endereco: ObjectId("60c0c6f0d510e6d3e397e91a"),
        consolidacao: [
            ObjectId("60c0cb15d510e6d3e397e91f"),
            ObjectId("60c0cb15d510e6d3e397e91f"),
            ObjectId("60c0cb15d510e6d3e397e91f"),
            ObjectId("60c0cb15d510e6d3e397e91f"),
            ObjectId("60c0cb15d510e6d3e397e91f")
        ],
        conta: [
            ObjectId("60c0ca7fd510e6d3e397e91e"),
            ObjectId("60c0ca7fd510e6d3e397e91e"),
            ObjectId("60c0ca7fd510e6d3e397e91e"),
            ObjectId("60c0ca7fd510e6d3e397e91e"),
            ObjectId("60c0ca7fd510e6d3e397e91e"),
        ]
        
    },
    {
        //60c0cc0ad510e6d3e397e926
        codigo: "4567",
        nome: "Hermana",
        endereco: ObjectId("60c0c6f0d510e6d3e397e91a"),
        consolidacao: [
            ObjectId("60c0cb15d510e6d3e397e91f"),
            ObjectId("60c0cb15d510e6d3e397e91f"),
            ObjectId("60c0cb15d510e6d3e397e91f"),
            ObjectId("60c0cb15d510e6d3e397e91f"),
            ObjectId("60c0cb15d510e6d3e397e91f")
        ],
        conta: [
            ObjectId("60c0ca7fd510e6d3e397e91e"),
            ObjectId("60c0ca7fd510e6d3e397e91e"),
            ObjectId("60c0ca7fd510e6d3e397e91e"),
            ObjectId("60c0ca7fd510e6d3e397e91e"),
            ObjectId("60c0ca7fd510e6d3e397e91e"),
        ]
    }
]);
db.agencia.find();


db.banco.insert(
    [
        {
            codigo: "001",
            nome: "Alfa",
            agencia: [
                ObjectId("60c0cc0ad510e6d3e397e920"),
                ObjectId("60c0cc0ad510e6d3e397e921"),
                ObjectId("60c0cc0ad510e6d3e397e922")
            ]
        },
        {
            codigo: "241",
            nome: "Beta",
            agencia: [
                ObjectId("60c0cc0ad510e6d3e397e923")                
            ]
        },
        {
            codigo: "307",
            nome: "Omega",
            agencia: [
                ObjectId("60c0cc0ad510e6d3e397e924"),
                ObjectId("60c0cc0ad510e6d3e397e925"),
                ObjectId("60c0cc0ad510e6d3e397e926")
            ]
        }
    ]
);
db.banco.find();
*/




