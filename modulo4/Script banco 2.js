db.createCollection("banco");
db.banco.insert(
	"codigo": "011",
	"nome": "Alfa",
	"codigoAgencia": [0],
	"cliente": [0],
	"consolidacao": [0],
);
db.banco.insert(
	"codigo": "241",
	"nome": "Beta",
	"codigoAgencia": [1],
	"cliente": [0],
	"consolidacao": [0],
);
db.banco.insert(
	"codigo": "307",
	"nome": "Omega",
	"codigoAgencia": [2],
	"cliente": [0],
	"consolidacao": [0],
);



db.createCollection("agencia");
db.agencia.insert(
   "codigo": "011",
   "nome": "Alfa",
   "enderecoAgencia": [0]
);
db.agencia.insert(
	"codigo": "0001",
	"nome": "Web",
	"enderecoAgencia": [1]
);
db.agencia.insert(
	"codigo": "0101",
   "nome": "Omega",
   "enderecoAgencia": [2]
);



db.createCollection("endereco");
db.endereco.insert(
	[
		"logradouro": "Rua Testando",
		"numero": 55,
		"complemento": "Loja 1",
		"cep": "00000-00",
		"bairro": "NA",
		"cidade": "NA",
		"estado": "NA",
		"pais": "Brasil"
	],
	[
		"logradouro": "Rua Testing",
		"numero": 122,
		"complemento": "",
		"cep": "00000-00",
		"bairro": "Between Hyde and Powell Streets",
		"cidade": "San Francisco",
		"estado": "California",
		"pais": "EUA"
	],
	[
		"logradouro": "Rua Tesing",
		"numero": 525,
		"complemento": "",
		"cep": "00000-00",
		"bairro": "Croydon",
		"cidade": "Londres",
		"estado": "Boroughs",
		"pais": "England"
	]
);

db.createCollection("consolidacao");
db.consolidacao.insert(
{
	  "tipo": "crebito",
	  "valor": 1200.00
	},
	{
	  "tipo": "a combinar",
	  "valor": 1000.00
	},
	{
	  "tipo": "dedito",
	  "valor": 100.00
	}
);

db.createCollection("cliente");
db.cliente.insert(
	"nome": "Cliente",
	"cpf": "000.000.000-00",
	"estadoCivil": "Em relacionamento com minha cama",
	"dataNascimento": "00/00/0000",
	"endereco": [0]
)


