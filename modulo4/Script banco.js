//db.banco.find();
db.banco.insert {
	{
	  "codigo": "011",
	  "nome": "Alfa",
	  "agencia": [
		 {
		   "codigo": "0001",
		   "nome": "Web",
		   "endereco": {
		     "logradouro": "Rua Testando",
		     "numero": 55,
		     "complemento": "Loja 1",
		     "cep": "00000-00",
		     "bairro": "NA",
		     "cidade": "NA",
		     "estado": "NA",
		     "pais": "Brasil"
		   },
		   "consolidacao": [
		     {
		       "saldoAtual": 0,
		       "saque": 0,
		       "deposito": 0,
		       "numeroCorrentistas": 0
		     }
		   ],
		   "conta": [
		     {
		       "codigo": "1",
		       "tipoConta": "PF",
		       "saldo": 0.0,
		       "gerente": [
		         {
		           "nome": "Gerente1",
		           "cpf": "000.000.000-00",
		           "estadoCivil": "Em relacionamento com minha cama",
		           "dataNascimento": "00/00/0000",
		           "codigoFuncionario": "0000011",
		           "tipoGerente": "GC",
		           "endereco": {
		             "logradouro": "Rua Testando",
		             "numero": 55,
		             "complemento": "Loja 1",
		             "cep": "00000-00",
		             "bairro": "NA",
		             "cidade": "NA",
		             "estado": "NA",
		             "pais": "Brasil"
		           }
		         }
		       ],
		       "cliente": [
		         {
		           "nome": "Cliente1",
		           "cpf": "000.000.000-00",
		           "estadoCivil": "Em relacionamento com minha cama",
		           "dataNascimento": "00/00/0000",
		           "endereco": {
						  "logradouro": "Rua do meio",
						  "numero": 2233,
						  "complemento": "",
						  "cep": "00000-00",
						  "bairro": "Caminito",
						  "cidade": "Buenos Aires",
						  "estado": "Buenos Aires",
						  "pais": "Argentina"
						},
		         }
		       ],
		       "cliente": [
		         {
		           "nome": "Cliente2",
		           "cpf": "000.000.000-00",
		           "estadoCivil": "Em relacionamento com minha cama",
		           "dataNascimento": "00/00/0000",
		           "endereco": {
						  "logradouro": "Rua do meio",
						  "numero": 2233,
						  "complemento": "",
						  "cep": "00000-00",
						  "bairro": "Caminito",
						  "cidade": "Buenos Aires",
						  "estado": "Buenos Aires",
						  "pais": "Argentina"
						},
		         }
		       ],
		       "cliente": [
		         {
		           "nome": "Cliente3",
		           "cpf": "000.000.000-00",
		           "estadoCivil": "Em relacionamento com minha cama",
		           "dataNascimento": "00/00/0000",
		           "endereco": {
						  "logradouro": "Rua do meio",
						  "numero": 2233,
						  "complemento": "",
						  "cep": "00000-00",
						  "bairro": "Caminito",
						  "cidade": "Buenos Aires",
						  "estado": "Buenos Aires",
						  "pais": "Argentina"
						},
		         }
		       ],
		       "cliente": [
		         {
		           "nome": "Cliente4",
		           "cpf": "000.000.000-00",
		           "estadoCivil": "Em relacionamento com minha cama",
		           "dataNascimento": "00/00/0000",
		           "endereco": {
						  "logradouro": "Rua do meio",
						  "numero": 2233,
						  "complemento": "",
						  "cep": "00000-00",
						  "bairro": "Caminito",
						  "cidade": "Buenos Aires",
						  "estado": "Buenos Aires",
						  "pais": "Argentina"
						},
		         }
		       ],
		       "cliente": [
		         {
		           "nome": "Cliente5",
		           "cpf": "000.000.000-00",
		           "estadoCivil": "Em relacionamento com minha cama",
		           "dataNascimento": "00/00/0000",
		           "endereco": {
						  "logradouro": "Rua do meio",
						  "numero": 2233,
						  "complemento": "",
						  "cep": "00000-00",
						  "bairro": "Caminito",
						  "cidade": "Buenos Aires",
						  "estado": "Buenos Aires",
						  "pais": "Argentina"
						},
		         }
		       ],
		       "cliente": [
		         {
		           "nome": "Cliente6",
		           "cpf": "000.000.000-00",
		           "estadoCivil": "Em relacionamento com minha cama",
		           "dataNascimento": "00/00/0000",
		           "endereco": {
						  "logradouro": "Rua do meio",
						  "numero": 2233,
						  "complemento": "",
						  "cep": "00000-00",
						  "bairro": "Caminito",
						  "cidade": "Buenos Aires",
						  "estado": "Buenos Aires",
						  "pais": "Argentina"
						},
		         }
		       ],
		       "cliente": [
		         {
		           "nome": "Cliente7",
		           "cpf": "000.000.000-00",
		           "estadoCivil": "Em relacionamento com minha cama",
		           "dataNascimento": "00/00/0000",
		           "endereco": {
						  "logradouro": "Rua do meio",
						  "numero": 2233,
						  "complemento": "",
						  "cep": "00000-00",
						  "bairro": "Caminito",
						  "cidade": "Buenos Aires",
						  "estado": "Buenos Aires",
						  "pais": "Argentina"
						},
		         }
		       ],
		       "cliente": [
		         {
		           "nome": "Cliente8",
		           "cpf": "000.000.000-00",
		           "estadoCivil": "Em relacionamento com minha cama",
		           "dataNascimento": "00/00/0000",
		           "endereco": {
						  "logradouro": "Rua do meio",
						  "numero": 2233,
						  "complemento": "",
						  "cep": "00000-00",
						  "bairro": "Caminito",
						  "cidade": "Buenos Aires",
						  "estado": "Buenos Aires",
						  "pais": "Argentina"
						},
		         }
		       ],
		       "cliente": [
		         {
		           "nome": "Cliente9",
		           "cpf": "000.000.000-00",
		           "estadoCivil": "Em relacionamento com minha cama",
		           "dataNascimento": "00/00/0000",
		           "endereco": {
						  "logradouro": "Rua do meio",
						  "numero": 2233,
						  "complemento": "",
						  "cep": "00000-00",
						  "bairro": "Caminito",
						  "cidade": "Buenos Aires",
						  "estado": "Buenos Aires",
						  "pais": "Argentina"
						},
		         }
		       ],
		       "cliente": [
		         {
		           "nome": "Cliente10",
		           "cpf": "000.000.000-00",
		           "estadoCivil": "Em relacionamento com minha cama",
		           "dataNascimento": "00/00/0000",
		           "endereco": {
						  "logradouro": "Rua do meio",
						  "numero": 2233,
						  "complemento": "",
						  "cep": "00000-00",
						  "bairro": "Caminito",
						  "cidade": "Buenos Aires",
						  "estado": "Buenos Aires",
						  "pais": "Argentina"
						},
		         }
		       ],
		       "movimentacoes": [
		         {
		           "tipo": "crebito",
		           "valor": 1200.00
		         },
		         {
		           "tipo": "a combinar",
		           "valor": 1000.00
		         },
		         {
		           "tipo": "dedito",
		           "valor": 100.00
		         }
		       ]
		     }
		   ]
		 }
	  ]
	}
	//db.banco.find({ nome: { $regex: /A/ }});
	//db.banco.find({ codigo: { $eq: "011" }}); =
	//db.banco.find({ codigo: { $ne: "011" }}); <>
	//db.banco.find({ "agencia.conta.movimentacoes.valor": { $gt: 10 }}); >
	//db.banco.find({ "agencia.conta.movimentacoes.valor": { $lt: 10 } }); <
	//db.banco.find({ "agencia.conta.movimentacoes.valor": { $lte: 10 } }); <=
	//db.banco.find({ "agencia.conta.movimentacoes.valor": { $in: { 10, 100 } } }); in 
	db.banco.find().sort({"agencia.conta.movimentacoes.valor": -1});

	/////////////////////
	{
	  "codigo": "011",
	  "nome": "Alfa",
	  "agencia": [
		 {
		   "codigo": "0002",
		   "nome": "California",
		   "endereco": {
		     "logradouro": "Rua Testing",
		     "numero": 122,
		     "complemento": "",
		     "cep": "00000-00",
		     "bairro": "Between Hyde and Powell Streets",
		     "cidade": "San Francisco",
		     "estado": "California",
		     "pais": "EUA"
		   },
		   "consolidacao": [
		     {
		       "saldoAtual": 0,
		       "saque": 0,
		       "deposito": 0,
		       "numeroCorrentistas": 0
		     }
		   ],
		   "conta": [
		     {
		       "codigo": "2",
		       "tipoConta": "PF",
		       "saldo": 0.0,
		       "gerente": [
		         {
		           "nome": "Gerente11",
		           "cpf": "000.000.000-00",
		           "estadoCivil": "Em relacionamento com minha cama",
		           "dataNascimento": "00/00/0000",
		           "codigoFuncionario": "0000012",
		           "tipoGerente": "GC",
		           "endereco": {
						  "logradouro": "Rua Testing",
						  "numero": 122,
						  "complemento": "",
						  "cep": "00000-00",
						  "bairro": "Between Hyde and Powell Streets",
						  "cidade": "San Francisco",
						  "estado": "California",
						  "pais": "EUA"
						},
		         }
		       ],
		       "cliente": [
		         {
		           "nome": "Cliente12",
		           "cpf": "000.000.000-00",
		           "estadoCivil": "Em relacionamento com minha cama",
		           "dataNascimento": "00/00/0000",
		           "endereco": {
						  "logradouro": "Rua do meio",
						  "numero": 2233,
						  "complemento": "",
						  "cep": "00000-00",
						  "bairro": "Caminito",
						  "cidade": "Buenos Aires",
						  "estado": "Buenos Aires",
						  "pais": "Argentina"
						},
		         }
		       ],
		       "cliente": [
		         {
		           "nome": "Cliente13",
		           "cpf": "000.000.000-00",
		           "estadoCivil": "Em relacionamento com minha cama",
		           "dataNascimento": "00/00/0000",
		           "endereco": {
						  "logradouro": "Rua do meio",
						  "numero": 2233,
						  "complemento": "",
						  "cep": "00000-00",
						  "bairro": "Caminito",
						  "cidade": "Buenos Aires",
						  "estado": "Buenos Aires",
						  "pais": "Argentina"
						},
		         }
		       ],
		       "cliente": [
		         {
		           "nome": "Cliente14",
		           "cpf": "000.000.000-00",
		           "estadoCivil": "Em relacionamento com minha cama",
		           "dataNascimento": "00/00/0000",
		           "endereco": {
						  "logradouro": "Rua do meio",
						  "numero": 2233,
						  "complemento": "",
						  "cep": "00000-00",
						  "bairro": "Caminito",
						  "cidade": "Buenos Aires",
						  "estado": "Buenos Aires",
						  "pais": "Argentina"
						},
		         }
		       ],
		       "cliente": [
		         {
		           "nome": "Cliente15",
		           "cpf": "000.000.000-00",
		           "estadoCivil": "Em relacionamento com minha cama",
		           "dataNascimento": "00/00/0000",
		           "endereco": {
						  "logradouro": "Rua do meio",
						  "numero": 2233,
						  "complemento": "",
						  "cep": "00000-00",
						  "bairro": "Caminito",
						  "cidade": "Buenos Aires",
						  "estado": "Buenos Aires",
						  "pais": "Argentina"
						},
		         }
		       ],
		       "cliente": [
		         {
		           "nome": "Cliente16",
		           "cpf": "000.000.000-00",
		           "estadoCivil": "Em relacionamento com minha cama",
		           "dataNascimento": "00/00/0000",
		           "endereco": {
						  "logradouro": "Rua do meio",
						  "numero": 2233,
						  "complemento": "",
						  "cep": "00000-00",
						  "bairro": "Caminito",
						  "cidade": "Buenos Aires",
						  "estado": "Buenos Aires",
						  "pais": "Argentina"
						},
		         }
		       ],
		       "cliente": [
		         {
		           "nome": "Cliente17",
		           "cpf": "000.000.000-00",
		           "estadoCivil": "Em relacionamento com minha cama",
		           "dataNascimento": "00/00/0000",
		           "endereco": {
						  "logradouro": "Rua do meio",
						  "numero": 2233,
						  "complemento": "",
						  "cep": "00000-00",
						  "bairro": "Caminito",
						  "cidade": "Buenos Aires",
						  "estado": "Buenos Aires",
						  "pais": "Argentina"
						},
		         }
		       ],
		       "cliente": [
		         {
		           "nome": "Cliente18",
		           "cpf": "000.000.000-00",
		           "estadoCivil": "Em relacionamento com minha cama",
		           "dataNascimento": "00/00/0000",
		           "endereco": {
						  "logradouro": "Rua do meio",
						  "numero": 2233,
						  "complemento": "",
						  "cep": "00000-00",
						  "bairro": "Caminito",
						  "cidade": "Buenos Aires",
						  "estado": "Buenos Aires",
						  "pais": "Argentina"
						},
		         }
		       ],
		       "cliente": [
		         {
		           "nome": "Cliente19",
		           "cpf": "000.000.000-00",
		           "estadoCivil": "Em relacionamento com minha cama",
		           "dataNascimento": "00/00/0000",
		           "endereco": {
						  "logradouro": "Rua do meio",
						  "numero": 2233,
						  "complemento": "",
						  "cep": "00000-00",
						  "bairro": "Caminito",
						  "cidade": "Buenos Aires",
						  "estado": "Buenos Aires",
						  "pais": "Argentina"
						},
		         }
		       ],
		       "cliente": [
		         {
		           "nome": "Cliente20,
		           "cpf": "000.000.000-00",
		           "estadoCivil": "Em relacionamento com minha cama",
		           "dataNascimento": "00/00/0000",
		           "endereco": {
						  "logradouro": "Rua do meio",
						  "numero": 2233,
						  "complemento": "",
						  "cep": "00000-00",
						  "bairro": "Caminito",
						  "cidade": "Buenos Aires",
						  "estado": "Buenos Aires",
						  "pais": "Argentina"
						},
		         }
		       ],
		       "movimentacoes": [
		         {
		           "tipo": "crebito",
		           "valor": 1200.00
		         },
		         {
		           "tipo": "a combinar",
		           "valor": 1000.00
		         },
		         {
		           "tipo": "dedito",
		           "valor": 100.00
		         }
		       ]
		     }
		   ]
		 }
	  ]
	}
	/////////////////////
	{
	  "codigo": "011",
	  "nome": "Alfa",
	  "agencia": [
		 {
		   "codigo": "0101",
		   "nome": "Londres",
		   "endereco": {
		     "logradouro": "Rua Tesing",
		     "numero": 525,
		     "complemento": "",
		     "cep": "00000-00",
		     "bairro": "Croydon",
		     "cidade": "Londres",
		     "estado": "Boroughs",
		     "pais": "England"
		   },
		   "consolidacao": [
		     {
		       "saldoAtual": 0,
		       "saque": 0,
		       "deposito": 0,
		       "numeroCorrentistas": 0
		     }
		   ],
		   "conta": [
		     {
		       "codigo": "4",
		       "tipoConta": "PF",
		       "saldo": 0.0,
		       "gerente": [
		         {
		           "nome": "Gerente3",
		           "cpf": "000.000.000-00",
		           "estadoCivil": "Em relacionamento com minha cama",
		           "dataNascimento": "00/00/0000",
		           "codigoFuncionario": "0000012",
		           "tipoGerente": "GC",
		           "endereco": {
						  "logradouro": "Rua Tesing",
						  "numero": 525,
						  "complemento": "",
						  "cep": "00000-00",
						  "bairro": "Croydon",
						  "cidade": "Londres",
						  "estado": "Boroughs",
						  "pais": "England"
						},
		         }
		       ],
		       "cliente": [
		         {
		           "nome": "Cliente21",
		           "cpf": "000.000.000-00",
		           "estadoCivil": "Em relacionamento com minha cama",
		           "dataNascimento": "00/00/0000",
		           "endereco": {
						  "logradouro": "Rua do meio",
						  "numero": 2233,
						  "complemento": "",
						  "cep": "00000-00",
						  "bairro": "Caminito",
						  "cidade": "Buenos Aires",
						  "estado": "Buenos Aires",
						  "pais": "Argentina"
						},
		         }
		       ],
		       "cliente": [
		         {
		           "nome": "Cliente22",
		           "cpf": "000.000.000-00",
		           "estadoCivil": "Em relacionamento com minha cama",
		           "dataNascimento": "00/00/0000",
		           "endereco": {
						  "logradouro": "Rua do meio",
						  "numero": 2233,
						  "complemento": "",
						  "cep": "00000-00",
						  "bairro": "Caminito",
						  "cidade": "Buenos Aires",
						  "estado": "Buenos Aires",
						  "pais": "Argentina"
						},
		         }
		       ],
		       "cliente": [
		         {
		           "nome": "Cliente23",
		           "cpf": "000.000.000-00",
		           "estadoCivil": "Em relacionamento com minha cama",
		           "dataNascimento": "00/00/0000",
		           "endereco": {
						  "logradouro": "Rua do meio",
						  "numero": 2233,
						  "complemento": "",
						  "cep": "00000-00",
						  "bairro": "Caminito",
						  "cidade": "Buenos Aires",
						  "estado": "Buenos Aires",
						  "pais": "Argentina"
						},
		         }
		       ],
		       "cliente": [
		         {
		           "nome": "Cliente24",
		           "cpf": "000.000.000-00",
		           "estadoCivil": "Em relacionamento com minha cama",
		           "dataNascimento": "00/00/0000",
		           "endereco": {
						  "logradouro": "Rua do meio",
						  "numero": 2233,
						  "complemento": "",
						  "cep": "00000-00",
						  "bairro": "Caminito",
						  "cidade": "Buenos Aires",
						  "estado": "Buenos Aires",
						  "pais": "Argentina"
						},
		         }
		       ],
		       "cliente": [
		         {
		           "nome": "Cliente25",
		           "cpf": "000.000.000-00",
		           "estadoCivil": "Em relacionamento com minha cama",
		           "dataNascimento": "00/00/0000",
		           "endereco": {
						  "logradouro": "Rua do meio",
						  "numero": 2233,
						  "complemento": "",
						  "cep": "00000-00",
						  "bairro": "Caminito",
						  "cidade": "Buenos Aires",
						  "estado": "Buenos Aires",
						  "pais": "Argentina"
						},
		         }
		       ],
		       "cliente": [
		         {
		           "nome": "Cliente26",
		           "cpf": "000.000.000-00",
		           "estadoCivil": "Em relacionamento com minha cama",
		           "dataNascimento": "00/00/0000",
		           "endereco": {
						  "logradouro": "Rua do meio",
						  "numero": 2233,
						  "complemento": "",
						  "cep": "00000-00",
						  "bairro": "Caminito",
						  "cidade": "Buenos Aires",
						  "estado": "Buenos Aires",
						  "pais": "Argentina"
						},
		         }
		       ],
		       "cliente": [
		         {
		           "nome": "Cliente27",
		           "cpf": "000.000.000-00",
		           "estadoCivil": "Em relacionamento com minha cama",
		           "dataNascimento": "00/00/0000",
		           "endereco": {
						  "logradouro": "Rua do meio",
						  "numero": 2233,
						  "complemento": "",
						  "cep": "00000-00",
						  "bairro": "Caminito",
						  "cidade": "Buenos Aires",
						  "estado": "Buenos Aires",
						  "pais": "Argentina"
						},
		         }
		       ],
		       "cliente": [
		         {
		           "nome": "Cliente28",
		           "cpf": "000.000.000-00",
		           "estadoCivil": "Em relacionamento com minha cama",
		           "dataNascimento": "00/00/0000",
		           "endereco": {
						  "logradouro": "Rua do meio",
						  "numero": 2233,
						  "complemento": "",
						  "cep": "00000-00",
						  "bairro": "Caminito",
						  "cidade": "Buenos Aires",
						  "estado": "Buenos Aires",
						  "pais": "Argentina"
						},
		         }
		       ],
		       "cliente": [
		         {
		           "nome": "Cliente29",
		           "cpf": "000.000.000-00",
		           "estadoCivil": "Em relacionamento com minha cama",
		           "dataNascimento": "00/00/0000",
		           "endereco": {
						  "logradouro": "Rua do meio",
						  "numero": 2233,
						  "complemento": "",
						  "cep": "00000-00",
						  "bairro": "Caminito",
						  "cidade": "Buenos Aires",
						  "estado": "Buenos Aires",
						  "pais": "Argentina"
						},
		         }
		       ],
		       "cliente": [
		         {
		           "nome": "Cliente30",
		           "cpf": "000.000.000-00",
		           "estadoCivil": "Em relacionamento com minha cama",
		           "dataNascimento": "00/00/0000",
		           "endereco": {
						  "logradouro": "Rua do meio",
						  "numero": 2233,
						  "complemento": "",
						  "cep": "00000-00",
						  "bairro": "Caminito",
						  "cidade": "Buenos Aires",
						  "estado": "Buenos Aires",
						  "pais": "Argentina"
						},
		         }
		       ],
		       "movimentacoes": [
		         {
		           "tipo": "crebito",
		           "valor": 1200.00
		         },
		         {
		           "tipo": "a combinar",
		           "valor": 1000.00
		         },
		         {
		           "tipo": "dedito",
		           "valor": 100.00
		         }
		       ]
		     }
		   ]
		 }
	  ]
	}
	/////////////////////
	{
	  "codigo": "241",
	  "nome": "Beta",
	  "agencia": [
		 {
		   "codigo": "0001",
		   "nome": "Web",
		   "endereco": {
		     "logradouro": "Rua Testando",
		     "numero": 55,
		     "complemento": "Loja 2",
		     "cep": "00000-00",
		     "bairro": "NA",
		     "cidade": "NA",
		     "estado": "NA",
		     "pais": "Brasil"
		   },
		   "consolidacao": [
		     {
		       "saldoAtual": 0,
		       "saque": 0,
		       "deposito": 0,
		       "numeroCorrentistas": 0
		     }
		   ],
		   "conta": [
		     {
		       "codigo": "4",
		       "tipoConta": "PF",
		       "saldo": 0.0,
		       "gerente": [
		         {
		           "nome": "Gerente4",
		           "cpf": "000.000.000-00",
		           "estadoCivil": "Em relacionamento com minha cama",
		           "dataNascimento": "00/00/0000",
		           "codigoFuncionario": "0000012",
		           "tipoGerente": "GC",
		           "endereco": {
						  "logradouro": "Rua Testando",
						  "numero": 55,
						  "complemento": "",
						  "cep": "00000-00",
						  "bairro": "NA",
						  "cidade": "NA",
						  "estado": "NA",
						  "pais": "Brasil"
						},
		         }
		       ],
		       "cliente": [
		         {
		           "nome": "Cliente31",
		           "cpf": "000.000.000-00",
		           "estadoCivil": "Em relacionamento com minha cama",
		           "dataNascimento": "00/00/0000",
		           "endereco": {
						  "logradouro": "Rua do meio",
						  "numero": 2233,
						  "complemento": "",
						  "cep": "00000-00",
						  "bairro": "Caminito",
						  "cidade": "Buenos Aires",
						  "estado": "Buenos Aires",
						  "pais": "Argentina"
						},
		         }
		       ],
		       "cliente": [
		         {
		           "nome": "Cliente32",
		           "cpf": "000.000.000-00",
		           "estadoCivil": "Em relacionamento com minha cama",
		           "dataNascimento": "00/00/0000",
		           "endereco": {
						  "logradouro": "Rua do meio",
						  "numero": 2233,
						  "complemento": "",
						  "cep": "00000-00",
						  "bairro": "Caminito",
						  "cidade": "Buenos Aires",
						  "estado": "Buenos Aires",
						  "pais": "Argentina"
						},
		         }
		       ],
		       "cliente": [
		         {
		           "nome": "Cliente33",
		           "cpf": "000.000.000-00",
		           "estadoCivil": "Em relacionamento com minha cama",
		           "dataNascimento": "00/00/0000",
		           "endereco": {
						  "logradouro": "Rua do meio",
						  "numero": 2233,
						  "complemento": "",
						  "cep": "00000-00",
						  "bairro": "Caminito",
						  "cidade": "Buenos Aires",
						  "estado": "Buenos Aires",
						  "pais": "Argentina"
						},
		         }
		       ],
		       "cliente": [
		         {
		           "nome": "Cliente34",
		           "cpf": "000.000.000-00",
		           "estadoCivil": "Em relacionamento com minha cama",
		           "dataNascimento": "00/00/0000",
		           "endereco": {
						  "logradouro": "Rua do meio",
						  "numero": 2233,
						  "complemento": "",
						  "cep": "00000-00",
						  "bairro": "Caminito",
						  "cidade": "Buenos Aires",
						  "estado": "Buenos Aires",
						  "pais": "Argentina"
						},
		         }
		       ],
		       "cliente": [
		         {
		           "nome": "Cliente35",
		           "cpf": "000.000.000-00",
		           "estadoCivil": "Em relacionamento com minha cama",
		           "dataNascimento": "00/00/0000",
		           "endereco": {
						  "logradouro": "Rua do meio",
						  "numero": 2233,
						  "complemento": "",
						  "cep": "00000-00",
						  "bairro": "Caminito",
						  "cidade": "Buenos Aires",
						  "estado": "Buenos Aires",
						  "pais": "Argentina"
						},
		         }
		       ],
		       "cliente": [
		         {
		           "nome": "Cliente36",
		           "cpf": "000.000.000-00",
		           "estadoCivil": "Em relacionamento com minha cama",
		           "dataNascimento": "00/00/0000",
		           "endereco": {
						  "logradouro": "Rua do meio",
						  "numero": 2233,
						  "complemento": "",
						  "cep": "00000-00",
						  "bairro": "Caminito",
						  "cidade": "Buenos Aires",
						  "estado": "Buenos Aires",
						  "pais": "Argentina"
						},
		         }
		       ],
		       "cliente": [
		         {
		           "nome": "Cliente37",
		           "cpf": "000.000.000-00",
		           "estadoCivil": "Em relacionamento com minha cama",
		           "dataNascimento": "00/00/0000",
		           "endereco": {
						  "logradouro": "Rua do meio",
						  "numero": 2233,
						  "complemento": "",
						  "cep": "00000-00",
						  "bairro": "Caminito",
						  "cidade": "Buenos Aires",
						  "estado": "Buenos Aires",
						  "pais": "Argentina"
						},
		         }
		       ],
		       "cliente": [
		         {
		           "nome": "Cliente38",
		           "cpf": "000.000.000-00",
		           "estadoCivil": "Em relacionamento com minha cama",
		           "dataNascimento": "00/00/0000",
		           "endereco": {
						  "logradouro": "Rua do meio",
						  "numero": 2233,
						  "complemento": "",
						  "cep": "00000-00",
						  "bairro": "Caminito",
						  "cidade": "Buenos Aires",
						  "estado": "Buenos Aires",
						  "pais": "Argentina"
						},
		         }
		       ],
		       "cliente": [
		         {
		           "nome": "Cliente39",
		           "cpf": "000.000.000-00",
		           "estadoCivil": "Em relacionamento com minha cama",
		           "dataNascimento": "00/00/0000",
		           "endereco": {
						  "logradouro": "Rua do meio",
						  "numero": 2233,
						  "complemento": "",
						  "cep": "00000-00",
						  "bairro": "Caminito",
						  "cidade": "Buenos Aires",
						  "estado": "Buenos Aires",
						  "pais": "Argentina"
						},
		         }
		       ],
		       "cliente": [
		         {
		           "nome": "Cliente40",
		           "cpf": "000.000.000-00",
		           "estadoCivil": "Em relacionamento com minha cama",
		           "dataNascimento": "00/00/0000",
		           "endereco": {
						  "logradouro": "Rua do meio",
						  "numero": 2233,
						  "complemento": "",
						  "cep": "00000-00",
						  "bairro": "Caminito",
						  "cidade": "Buenos Aires",
						  "estado": "Buenos Aires",
						  "pais": "Argentina"
						},
		         }
		       ],
		       "movimentacoes": [
		         {
		           "tipo": "crebito",
		           "valor": 1200.00
		         },
		         {
		           "tipo": "a combinar",
		           "valor": 1000.00
		         },
		         {
		           "tipo": "dedito",
		           "valor": 100.00
		         }
		       ]
		     }
		   ]
		 }
	  ]
	}
	/////////////////////

	{
	  "codigo": "307",
	  "nome": "Omega",
	  "agencia": [
		 {
		   "codigo": "0001",
		   "nome": "Web",
		   "endereco": {
		     "logradouro": "Rua Testando",
		     "numero": 55,
		     "complemento": "Loja 3",
		     "cep": "00000-00",
		     "bairro": "NA",
		     "cidade": "NA",
		     "estado": "NA",
		     "pais": "Brasil"
		   },
		   "consolidacao": [
		     {
		       "saldoAtual": 0,
		       "saque": 0,
		       "deposito": 0,
		       "numeroCorrentistas": 0
		     }
		   ],
		   "conta": [
		     {
		       "codigo": "5",
		       "tipoConta": "PF",
		       "saldo": 0.0,
		       "gerente": [
		         {
		           "nome": "Gerente5",
		           "cpf": "000.000.000-00",
		           "estadoCivil": "Em relacionamento com minha cama",
		           "dataNascimento": "00/00/0000",
		           "codigoFuncionario": "0000012",
		           "tipoGerente": "GC",
		           "endereco": {
						  "logradouro": "Rua Testando",
						  "numero": 55,
						  "complemento": "Loja 3",
						  "cep": "00000-00",
						  "bairro": "NA",
						  "cidade": "NA",
						  "estado": "NA",
						  "pais": "Brasil"
						},
		         }
		       ],
		       "cliente": [
		         {
		           "nome": "Cliente41",
		           "cpf": "000.000.000-00",
		           "estadoCivil": "Em relacionamento com minha cama",
		           "dataNascimento": "00/00/0000",
		           "endereco": {
						  "logradouro": "Rua do meio",
						  "numero": 2233,
						  "complemento": "",
						  "cep": "00000-00",
						  "bairro": "Caminito",
						  "cidade": "Buenos Aires",
						  "estado": "Buenos Aires",
						  "pais": "Argentina"
						},
		         }
		       ],
		       "cliente": [
		         {
		           "nome": "Cliente42",
		           "cpf": "000.000.000-00",
		           "estadoCivil": "Em relacionamento com minha cama",
		           "dataNascimento": "00/00/0000",
		           "endereco": {
						  "logradouro": "Rua do meio",
						  "numero": 2233,
						  "complemento": "",
						  "cep": "00000-00",
						  "bairro": "Caminito",
						  "cidade": "Buenos Aires",
						  "estado": "Buenos Aires",
						  "pais": "Argentina"
						},
		         }
		       ],
		       "cliente": [
		         {
		           "nome": "Cliente43",
		           "cpf": "000.000.000-00",
		           "estadoCivil": "Em relacionamento com minha cama",
		           "dataNascimento": "00/00/0000",
		           "endereco": {
						  "logradouro": "Rua do meio",
						  "numero": 2233,
						  "complemento": "",
						  "cep": "00000-00",
						  "bairro": "Caminito",
						  "cidade": "Buenos Aires",
						  "estado": "Buenos Aires",
						  "pais": "Argentina"
						},
		         }
		       ],
		       "cliente": [
		         {
		           "nome": "Cliente44",
		           "cpf": "000.000.000-00",
		           "estadoCivil": "Em relacionamento com minha cama",
		           "dataNascimento": "00/00/0000",
		           "endereco": {
						  "logradouro": "Rua do meio",
						  "numero": 2233,
						  "complemento": "",
						  "cep": "00000-00",
						  "bairro": "Caminito",
						  "cidade": "Buenos Aires",
						  "estado": "Buenos Aires",
						  "pais": "Argentina"
						},
		         }
		       ],
		       "cliente": [
		         {
		           "nome": "Cliente45",
		           "cpf": "000.000.000-00",
		           "estadoCivil": "Em relacionamento com minha cama",
		           "dataNascimento": "00/00/0000",
		           "endereco": {
						  "logradouro": "Rua do meio",
						  "numero": 2233,
						  "complemento": "",
						  "cep": "00000-00",
						  "bairro": "Caminito",
						  "cidade": "Buenos Aires",
						  "estado": "Buenos Aires",
						  "pais": "Argentina"
						},
		         }
		       ],
		       "cliente": [
		         {
		           "nome": "Cliente46",
		           "cpf": "000.000.000-00",
		           "estadoCivil": "Em relacionamento com minha cama",
		           "dataNascimento": "00/00/0000",
		           "endereco": {
						  "logradouro": "Rua do meio",
						  "numero": 2233,
						  "complemento": "",
						  "cep": "00000-00",
						  "bairro": "Caminito",
						  "cidade": "Buenos Aires",
						  "estado": "Buenos Aires",
						  "pais": "Argentina"
						},
		         }
		       ],
		       "cliente": [
		         {
		           "nome": "Cliente47",
		           "cpf": "000.000.000-00",
		           "estadoCivil": "Em relacionamento com minha cama",
		           "dataNascimento": "00/00/0000",
		           "endereco": {
						  "logradouro": "Rua do meio",
						  "numero": 2233,
						  "complemento": "",
						  "cep": "00000-00",
						  "bairro": "Caminito",
						  "cidade": "Buenos Aires",
						  "estado": "Buenos Aires",
						  "pais": "Argentina"
						},
		         }
		       ],
		       "cliente": [
		         {
		           "nome": "Cliente48",
		           "cpf": "000.000.000-00",
		           "estadoCivil": "Em relacionamento com minha cama",
		           "dataNascimento": "00/00/0000",
		           "endereco": {
						  "logradouro": "Rua do meio",
						  "numero": 2233,
						  "complemento": "",
						  "cep": "00000-00",
						  "bairro": "Caminito",
						  "cidade": "Buenos Aires",
						  "estado": "Buenos Aires",
						  "pais": "Argentina"
						},
		         }
		       ],
		       "cliente": [
		         {
		           "nome": "Cliente49",
		           "cpf": "000.000.000-00",
		           "estadoCivil": "Em relacionamento com minha cama",
		           "dataNascimento": "00/00/0000",
		           "endereco": {
						  "logradouro": "Rua do meio",
						  "numero": 2233,
						  "complemento": "",
						  "cep": "00000-00",
						  "bairro": "Caminito",
						  "cidade": "Buenos Aires",
						  "estado": "Buenos Aires",
						  "pais": "Argentina"
						},
		         }
		       ],
		       "cliente": [
		         {
		           "nome": "Cliente50",
		           "cpf": "000.000.000-00",
		           "estadoCivil": "Em relacionamento com minha cama",
		           "dataNascimento": "00/00/0000",
		           "endereco": {
						  "logradouro": "Rua do meio",
						  "numero": 2233,
						  "complemento": "",
						  "cep": "00000-00",
						  "bairro": "Caminito",
						  "cidade": "Buenos Aires",
						  "estado": "Buenos Aires",
						  "pais": "Argentina"
						},
		         }
		       ],
		       "movimentacoes": [
		         {
		           "tipo": "crebito",
		           "valor": 1200.00
		         },
		         {
		           "tipo": "a combinar",
		           "valor": 1000.00
		         },
		         {
		           "tipo": "dedito",
		           "valor": 100.00
		         }
		       ]
		     }
		   ]
		 }
	  ]
	}
	/////////////////////
	{
	  "codigo": "307",
	  "nome": "Omega",
	  "agencia": [
		 {
		   "codigo": "8761",
		   "nome": "Itu",
		   "endereco": {
		     "logradouro": "Rua do Meio",
		     "numero": 2233,
		     "complemento": "",
		     "cep": "00000-00",
		     "bairro": "Qualquer",
		     "cidade": "Itu",
		     "estado": "Sao Paulo",
		     "pais": "Brasil"
		   },
		   "consolidacao": [
		     {
		       "saldoAtual": 0,
		       "saque": 0,
		       "deposito": 0,
		       "numeroCorrentistas": 0
		     }
		   ],
		   "conta": [
		     {
		       "codigo": "6",
		       "tipoConta": "PF",
		       "saldo": 0.0,
		       "gerente": [
		         {
		           "nome": "Gerente6",
		           "cpf": "000.000.000-00",
		           "estadoCivil": "Em relacionamento com minha cama",
		           "dataNascimento": "00/00/0000",
		           "codigoFuncionario": "0000012",
		           "tipoGerente": "GC",
		           "endereco": {
						  "logradouro": "Rua do Meio",
						  "numero": 2233,
						  "complemento": "",
						  "cep": "00000-00",
						  "bairro": "Qualquer",
						  "cidade": "Itu",
						  "estado": "Sao Paulo",
						  "pais": "Brasil"
						},
		         }
		       ],
		       "cliente": [
		         {
		           "nome": "Cliente51",
		           "cpf": "000.000.000-00",
		           "estadoCivil": "Em relacionamento com minha cama",
		           "dataNascimento": "00/00/0000",
		           "endereco": {
						  "logradouro": "Rua do meio",
						  "numero": 2233,
						  "complemento": "",
						  "cep": "00000-00",
						  "bairro": "Caminito",
						  "cidade": "Buenos Aires",
						  "estado": "Buenos Aires",
						  "pais": "Argentina"
						},
		         }
		       ],
		       "cliente": [
		         {
		           "nome": "Cliente52",
		           "cpf": "000.000.000-00",
		           "estadoCivil": "Em relacionamento com minha cama",
		           "dataNascimento": "00/00/0000",
		           "endereco": {
						  "logradouro": "Rua do meio",
						  "numero": 2233,
						  "complemento": "",
						  "cep": "00000-00",
						  "bairro": "Caminito",
						  "cidade": "Buenos Aires",
						  "estado": "Buenos Aires",
						  "pais": "Argentina"
						},
		         }
		       ],
		       "cliente": [
		         {
		           "nome": "Cliente53",
		           "cpf": "000.000.000-00",
		           "estadoCivil": "Em relacionamento com minha cama",
		           "dataNascimento": "00/00/0000",
		           "endereco": {
						  "logradouro": "Rua do meio",
						  "numero": 2233,
						  "complemento": "",
						  "cep": "00000-00",
						  "bairro": "Caminito",
						  "cidade": "Buenos Aires",
						  "estado": "Buenos Aires",
						  "pais": "Argentina"
						},
		         }
		       ],
		       "cliente": [
		         {
		           "nome": "Cliente54",
		           "cpf": "000.000.000-00",
		           "estadoCivil": "Em relacionamento com minha cama",
		           "dataNascimento": "00/00/0000",
		           "endereco": {
						  "logradouro": "Rua do meio",
						  "numero": 2233,
						  "complemento": "",
						  "cep": "00000-00",
						  "bairro": "Caminito",
						  "cidade": "Buenos Aires",
						  "estado": "Buenos Aires",
						  "pais": "Argentina"
						},
		         }
		       ],
		       "cliente": [
		         {
		           "nome": "Cliente55",
		           "cpf": "000.000.000-00",
		           "estadoCivil": "Em relacionamento com minha cama",
		           "dataNascimento": "00/00/0000",
		           "endereco": {
						  "logradouro": "Rua do meio",
						  "numero": 2233,
						  "complemento": "",
						  "cep": "00000-00",
						  "bairro": "Caminito",
						  "cidade": "Buenos Aires",
						  "estado": "Buenos Aires",
						  "pais": "Argentina"
						},
		         }
		       ],
		       "cliente": [
		         {
		           "nome": "Cliente56",
		           "cpf": "000.000.000-00",
		           "estadoCivil": "Em relacionamento com minha cama",
		           "dataNascimento": "00/00/0000",
		           "endereco": {
						  "logradouro": "Rua do meio",
						  "numero": 2233,
						  "complemento": "",
						  "cep": "00000-00",
						  "bairro": "Caminito",
						  "cidade": "Buenos Aires",
						  "estado": "Buenos Aires",
						  "pais": "Argentina"
						},
		         }
		       ],
		       "cliente": [
		         {
		           "nome": "Cliente57",
		           "cpf": "000.000.000-00",
		           "estadoCivil": "Em relacionamento com minha cama",
		           "dataNascimento": "00/00/0000",
		           "endereco": {
						  "logradouro": "Rua do meio",
						  "numero": 2233,
						  "complemento": "",
						  "cep": "00000-00",
						  "bairro": "Caminito",
						  "cidade": "Buenos Aires",
						  "estado": "Buenos Aires",
						  "pais": "Argentina"
						},
		         }
		       ],
		       "cliente": [
		         {
		           "nome": "Cliente58",
		           "cpf": "000.000.000-00",
		           "estadoCivil": "Em relacionamento com minha cama",
		           "dataNascimento": "00/00/0000",
		           "endereco": {
						  "logradouro": "Rua do meio",
						  "numero": 2233,
						  "complemento": "",
						  "cep": "00000-00",
						  "bairro": "Caminito",
						  "cidade": "Buenos Aires",
						  "estado": "Buenos Aires",
						  "pais": "Argentina"
						},
		         }
		       ],
		       "cliente": [
		         {
		           "nome": "Cliente59",
		           "cpf": "000.000.000-00",
		           "estadoCivil": "Em relacionamento com minha cama",
		           "dataNascimento": "00/00/0000",
		           "endereco": {
						  "logradouro": "Rua do meio",
						  "numero": 2233,
						  "complemento": "",
						  "cep": "00000-00",
						  "bairro": "Caminito",
						  "cidade": "Buenos Aires",
						  "estado": "Buenos Aires",
						  "pais": "Argentina"
						},
		         }
		       ],
		       "cliente": [
		         {
		           "nome": "Cliente60",
		           "cpf": "000.000.000-00",
		           "estadoCivil": "Em relacionamento com minha cama",
		           "dataNascimento": "00/00/0000",
		           "endereco": {
						  "logradouro": "Rua do meio",
						  "numero": 2233,
						  "complemento": "",
						  "cep": "00000-00",
						  "bairro": "Caminito",
						  "cidade": "Buenos Aires",
						  "estado": "Buenos Aires",
						  "pais": "Argentina"
						},
		         }
		       ],
		       "movimentacoes": [
		         {
		           "tipo": "crebito",
		           "valor": 1200.00
		         },
		         {
		           "tipo": "a combinar",
		           "valor": 1000.00
		         },
		         {
		           "tipo": "dedito",
		           "valor": 100.00
		         }
		       ]
		     }
		   ]
		 }
	  ]
	}
	/////////////////////
	{
	  "codigo": "307",
	  "nome": "Omega",
	  "agencia": [
		 {
		   "codigo": "4567",
		   "nome": "Hermana",
		   "endereco": {
		     "logradouro": "Rua do meio",
		     "numero": 2233,
		     "complemento": "",
		     "cep": "00000-00",
		     "bairro": "Caminito",
		     "cidade": "Buenos Aires",
		     "estado": "Buenos Aires",
		     "pais": "Argentina"
		   },
		   "consolidacao": [
		     {
		       "saldoAtual": 0,
		       "saque": 0,
		       "deposito": 0,
		       "numeroCorrentistas": 0
		     }
		   ],
		   "conta": [
		     {
		       "codigo": "7",
		       "tipoConta": "PF",
		       "saldo": 0.0,
		       "gerente": [
		         {
		           "nome": "Gerente7",
		           "cpf": "000.000.000-00",
		           "estadoCivil": "Em relacionamento com minha cama",
		           "dataNascimento": "00/00/0000",
		           "codigoFuncionario": "0000012",
		           "tipoGerente": "GC",
		           "endereco": {
						  "logradouro": "Rua do meio",
						  "numero": 2233,
						  "complemento": "",
						  "cep": "00000-00",
						  "bairro": "Caminito",
						  "cidade": "Buenos Aires",
						  "estado": "Buenos Aires",
						  "pais": "Argentina"
						},
		         }
		       ],
		       "cliente": [
		         {
		           "nome": "Cliente61",
		           "cpf": "000.000.000-00",
		           "estadoCivil": "Em relacionamento com minha cama",
		           "dataNascimento": "00/00/0000",
		           "endereco": {
						  "logradouro": "Rua do meio",
						  "numero": 2233,
						  "complemento": "",
						  "cep": "00000-00",
						  "bairro": "Caminito",
						  "cidade": "Buenos Aires",
						  "estado": "Buenos Aires",
						  "pais": "Argentina"
						},
		         }
		       ],
		       "cliente": [
		         {
		           "nome": "Cliente62",
		           "cpf": "000.000.000-00",
		           "estadoCivil": "Em relacionamento com minha cama",
		           "dataNascimento": "00/00/0000",
		           "endereco": {
						  "logradouro": "Rua do meio",
						  "numero": 2233,
						  "complemento": "",
						  "cep": "00000-00",
						  "bairro": "Caminito",
						  "cidade": "Buenos Aires",
						  "estado": "Buenos Aires",
						  "pais": "Argentina"
						},
		         }
		       ],
		       "cliente": [
		         {
		           "nome": "Cliente63",
		           "cpf": "000.000.000-00",
		           "estadoCivil": "Em relacionamento com minha cama",
		           "dataNascimento": "00/00/0000",
		           "endereco": {
						  "logradouro": "Rua do meio",
						  "numero": 2233,
						  "complemento": "",
						  "cep": "00000-00",
						  "bairro": "Caminito",
						  "cidade": "Buenos Aires",
						  "estado": "Buenos Aires",
						  "pais": "Argentina"
						},
		         }
		       ],
		       "cliente": [
		         {
		           "nome": "Cliente64",
		           "cpf": "000.000.000-00",
		           "estadoCivil": "Em relacionamento com minha cama",
		           "dataNascimento": "00/00/0000",
		           "endereco": {
						  "logradouro": "Rua do meio",
						  "numero": 2233,
						  "complemento": "",
						  "cep": "00000-00",
						  "bairro": "Caminito",
						  "cidade": "Buenos Aires",
						  "estado": "Buenos Aires",
						  "pais": "Argentina"
						},
		         }
		       ],
		       "cliente": [
		         {
		           "nome": "Cliente65",
		           "cpf": "000.000.000-00",
		           "estadoCivil": "Em relacionamento com minha cama",
		           "dataNascimento": "00/00/0000",
		           "endereco": {
						  "logradouro": "Rua do meio",
						  "numero": 2233,
						  "complemento": "",
						  "cep": "00000-00",
						  "bairro": "Caminito",
						  "cidade": "Buenos Aires",
						  "estado": "Buenos Aires",
						  "pais": "Argentina"
						},
		         }
		       ],
		       "cliente": [
		         {
		           "nome": "Cliente66",
		           "cpf": "000.000.000-00",
		           "estadoCivil": "Em relacionamento com minha cama",
		           "dataNascimento": "00/00/0000",
		           "endereco": {
						  "logradouro": "Rua do meio",
						  "numero": 2233,
						  "complemento": "",
						  "cep": "00000-00",
						  "bairro": "Caminito",
						  "cidade": "Buenos Aires",
						  "estado": "Buenos Aires",
						  "pais": "Argentina"
						},
		         }
		       ],
		       "cliente": [
		         {
		           "nome": "Cliente67",
		           "cpf": "000.000.000-00",
		           "estadoCivil": "Em relacionamento com minha cama",
		           "dataNascimento": "00/00/0000",
		           "endereco": {
						  "logradouro": "Rua do meio",
						  "numero": 2233,
						  "complemento": "",
						  "cep": "00000-00",
						  "bairro": "Caminito",
						  "cidade": "Buenos Aires",
						  "estado": "Buenos Aires",
						  "pais": "Argentina"
						},
		         }
		       ],
		       "cliente": [
		         {
		           "nome": "Cliente68",
		           "cpf": "000.000.000-00",
		           "estadoCivil": "Em relacionamento com minha cama",
		           "dataNascimento": "00/00/0000",
		           "endereco": {
						  "logradouro": "Rua do meio",
						  "numero": 2233,
						  "complemento": "",
						  "cep": "00000-00",
						  "bairro": "Caminito",
						  "cidade": "Buenos Aires",
						  "estado": "Buenos Aires",
						  "pais": "Argentina"
						},
		         }
		       ],
		       "cliente": [
		         {
		           "nome": "Cliente69",
		           "cpf": "000.000.000-00",
		           "estadoCivil": "Em relacionamento com minha cama",
		           "dataNascimento": "00/00/0000",
		           "endereco": {
						  "logradouro": "Rua do meio",
						  "numero": 2233,
						  "complemento": "",
						  "cep": "00000-00",
						  "bairro": "Caminito",
						  "cidade": "Buenos Aires",
						  "estado": "Buenos Aires",
						  "pais": "Argentina"
						},
		         }
		       ],
		       "cliente": [
		         {
		           "nome": "Cliente70",
		           "cpf": "000.000.000-00",
		           "estadoCivil": "Em relacionamento com minha cama",
		           "dataNascimento": "00/00/0000",
		           "endereco": {
						  "logradouro": "Rua do meio",
						  "numero": 2233,
						  "complemento": "",
						  "cep": "00000-00",
						  "bairro": "Caminito",
						  "cidade": "Buenos Aires",
						  "estado": "Buenos Aires",
						  "pais": "Argentina"
						},
		         }
		       ],
		       "movimentacoes": [
		         {
		           "tipo": "crebito",
		           "valor": 1200.00
		         },
		         {
		           "tipo": "a combinar",
		           "valor": 1000.00
		         },
		         {
		           "tipo": "dedito",
		           "valor": 100.00
		         }
		       ]
		     }
		   ]
		 }
	  ]
	}
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


db.banco.find();

db.banco.update( { _id: ObjectId("") }, { %set: {nome: "Alfa2"}  } );

/*
	$unset para deletar o campo;
	$ins para incrementar um valor especifico;
	$mul para multiplicar o valor do campo com um valor especifico que eu vou por;
	$rename :)
*/



db.banco.deleteOne({ nome:"Alfa" });










































































