package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.ClienteEntity;
import br.com.dbccompany.coworking.Entity.ContatoEntity;
import br.com.dbccompany.coworking.Entity.TipoContatoEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
public class ClienteRepositoryTest {

    @Autowired
    private ClientesRepository clientesRepository;

    @Test
    public void salvarCliente() {
        ClienteEntity cliente = new ClienteEntity();
        ContatoEntity contato = new ContatoEntity();
        TipoContatoEntity tipoContato = new TipoContatoEntity();

        cliente.setNome("giovanni");
        cliente.setCpf("12345678944");
        cliente.setDataNascimento(LocalDate.of(1997, 01, 16));

        clientesRepository.save(cliente);
        assertEquals(cliente.getNome(), clientesRepository.findByNome(cliente.getNome()));

    }

}
