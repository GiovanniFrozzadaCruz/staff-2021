package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.PacoteEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
public class PacoteRepositoryTest {

    @Autowired
    private PacotesRepository pacotesRepository;

    @Test
    public void salvarPacote() {
        PacoteEntity pacote = new PacoteEntity();
        pacote.setValor(5000.0);
        pacotesRepository.save(pacote);
        assertEquals(pacote.getValor(), pacotesRepository.findByValor(pacote.getValor()));
    }

}