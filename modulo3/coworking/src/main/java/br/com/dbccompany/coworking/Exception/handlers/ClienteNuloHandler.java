package br.com.dbccompany.coworking.Exception.handlers;

import br.com.dbccompany.coworking.Exception.Exceptions.ClienteNuloException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

public class ClienteNuloHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler( ClienteNuloException.class )
    public ResponseEntity handleException(ClienteNuloException e ) {
        return new ResponseEntity(e, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
