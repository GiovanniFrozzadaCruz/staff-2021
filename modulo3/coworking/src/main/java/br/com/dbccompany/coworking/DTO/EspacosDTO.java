package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.EspacoEntity;

public class EspacosDTO {
    protected String nome;
    protected int qtdPessoas;
    protected double valor;

    public EspacosDTO( EspacoEntity espacoEntity) {
        this.nome = espacoEntity.getNome();
        this.qtdPessoas = espacoEntity.getQtdPessoas();
        this.valor = espacoEntity.getValor();
    }

    public EspacosDTO(){}

    public EspacoEntity converter() {
        EspacoEntity espacos = new EspacoEntity();
        espacos.setNome( this.nome );
        espacos.setQtdPessoas( this.qtdPessoas );
        espacos.setValor( this.valor );
        return espacos;
    }


    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getQtdPessoas() {
        return qtdPessoas;
    }

    public void setQtdPessoas(int qtdPessoas) {
        this.qtdPessoas = qtdPessoas;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }
}
