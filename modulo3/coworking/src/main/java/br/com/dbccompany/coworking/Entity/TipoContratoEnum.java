package br.com.dbccompany.coworking.Entity;

public enum TipoContratoEnum {
    MINUTO, HORA, TURNO, DIARIA, SEMANA, MES
}
