package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.PagamentoEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface PagamentoRepository extends CrudRepository<PagamentoEntity, Integer> {

    Optional<PagamentoEntity> findById(Integer codigoPagamento );
}
