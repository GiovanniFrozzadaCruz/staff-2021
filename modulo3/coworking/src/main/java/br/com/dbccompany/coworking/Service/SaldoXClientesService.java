package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.Entity.SaldoXClienteId;
import br.com.dbccompany.coworking.Entity.SaldoXClientesEntity;
import br.com.dbccompany.coworking.Repository.SaldoXClientesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

import java.time.LocalDateTime;

@Service
public class SaldoXClientesService {

    @Autowired
    private SaldoXClientesRepository saldoXClientesRepository;

    @Autowired
    private ClientesService clientesService;

    @Autowired
    private EspacosService espacosService;

    public SaldoXClientesEntity save(@RequestBody SaldoXClientesEntity saldoXClientesDTO ) {
        return null;
    }

        /*
        EspacoEntity espaco = this.espacosService.findById( saldoXClientesDTO.getEspacos() );
        ClienteEntity cliente = this.clientesService.findById( saldoXClientesDTO.getClientes() );

        if( cliente == null) {
            throw new ClienteNuloException();
        }
        if( espaco == null ) {
            throw new EspacoNuloNaContratacaoException();
        }

        SaldoXClienteId id = new SaldoXClienteId();
        id.setCliente(cliente);
        id.setEspacos(espaco);


        SaldoXClientesEntity saldoXClientes = saldoXClientesDTO.converter();
        return this.saldoXClientesRepository.save(saldoXClientes);

    }
     */

    @Transactional(rollbackFor = Exception.class)
    public SaldoXClientesEntity findById( Integer idCliente, Integer idEspaco ) {
        SaldoXClienteId id = new SaldoXClienteId();
        id.setCliente(clientesService.findById(idCliente));
        id.setEspacos(espacosService.findById(idEspaco));
        return null;//saldoXClientesRepository.findById(id).get();
    }

    public LocalDateTime calcularDataVencimento(Integer data){
        LocalDateTime agora = LocalDateTime.now();
        return agora.plusDays(data);
    }

}
