package br.com.dbccompany.coworking.Exception.Exceptions;

public class EspacoNuloException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public EspacoNuloException() {
        super("Id do espaco invalido.");
    }
}
