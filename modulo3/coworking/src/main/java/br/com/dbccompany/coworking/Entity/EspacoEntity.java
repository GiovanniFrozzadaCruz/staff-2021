package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class EspacoEntity {
    @Id
    @SequenceGenerator( name = "ESPACO_SEQ", sequenceName = "ESPACO_SEQ" )
    @GeneratedValue( generator = "ESPACO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer Id;

    @Column( nullable = false, unique = true )
    private String nome;

    @Column( nullable = false )
    private int qtdPessoas;

    @Column( nullable = false )
    private double valor;

    @OneToMany( cascade = CascadeType.PERSIST )
    private List<EspacoXPacoteEntity> espacoXPacoteEntities;

    @OneToMany( cascade = CascadeType.PERSIST )
    private List<ClienteEntity> clientesEntities;

    @OneToMany
    private List<SaldoXClientesEntity> saldoXClientes;

    public EspacoEntity(String nome, int qtdPessoas, double valor) {
        this.nome = nome;
        this.qtdPessoas = qtdPessoas;
        this.valor = valor;
    }

    public EspacoEntity() {

    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getQtdPessoas() {
        return qtdPessoas;
    }

    public void setQtdPessoas(int qtdPessoas) {
        this.qtdPessoas = qtdPessoas;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public List<EspacoXPacoteEntity> getEspacoXPacoteEntities() {
        return espacoXPacoteEntities;
    }

    public void setEspacoXPacoteEntities(List<EspacoXPacoteEntity> espacoXPacoteEntities) {
        this.espacoXPacoteEntities = espacoXPacoteEntities;
    }

    public List<ClienteEntity> getClientesEntities() {
        return clientesEntities;
    }

    public void setClientesEntities(List<ClienteEntity> clientesEntities) {
        this.clientesEntities = clientesEntities;
    }

    public List<SaldoXClientesEntity> getSaldoXClientes() {
        return saldoXClientes;
    }

    public void setSaldoXClientes(List<SaldoXClientesEntity> saldoXClientes) {
        this.saldoXClientes = saldoXClientes;
    }
}
