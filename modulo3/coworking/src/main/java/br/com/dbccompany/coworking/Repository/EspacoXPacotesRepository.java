package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.EspacoXPacoteEntity;
import br.com.dbccompany.coworking.Entity.PacoteEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface EspacoXPacotesRepository extends CrudRepository<EspacoXPacoteEntity, Integer> {
    List<EspacoXPacoteEntity> findByPacotes(PacoteEntity pacote);
}
