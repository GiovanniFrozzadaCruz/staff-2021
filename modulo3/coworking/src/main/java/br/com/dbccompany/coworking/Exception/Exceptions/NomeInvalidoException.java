package br.com.dbccompany.coworking.Exception.Exceptions;

public class NomeInvalidoException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public NomeInvalidoException() {
        super("Nome invalido.");
    }
}
