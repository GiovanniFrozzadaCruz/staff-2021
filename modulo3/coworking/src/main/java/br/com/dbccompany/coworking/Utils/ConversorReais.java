package br.com.dbccompany.coworking.Utils;

import java.text.NumberFormat;
import java.util.Locale;

public class ConversorReais {

    public static String toReais( double valor ){
        NumberFormat numberFormat = NumberFormat.getCurrencyInstance(new Locale( "pt", "BR" ));
        return numberFormat.format( valor );
    }

    public static Double toDouble( String valor ){
        return Double.parseDouble( valor );
    }

}
