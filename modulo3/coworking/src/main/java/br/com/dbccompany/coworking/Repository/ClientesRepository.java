package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.DTO.ClientesDTO;
import br.com.dbccompany.coworking.Entity.ClienteEntity;
import org.springframework.data.repository.CrudRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface ClientesRepository extends CrudRepository<ClienteEntity, Integer> {

    Optional<ClienteEntity> findById(Integer codigoCliente );
    List<ClientesDTO> findByNome (String descricao );
    ClientesDTO findByCpf (String cpf );
    List<ClientesDTO> findAllByDataNascimento( LocalDate dataNascimento );
}
