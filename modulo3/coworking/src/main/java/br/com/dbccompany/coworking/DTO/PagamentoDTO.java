package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.PagamentoEntity;
import br.com.dbccompany.coworking.Entity.TipoPagamentoEnum;

public class PagamentoDTO {
    private TipoPagamentoEnum tipoPagamentoEnum;
    private Integer codigoClienteXPacote;
    private Integer codigoContratacao;

    public PagamentoDTO( PagamentoEntity pagamentoEntity ) {
        this.tipoPagamentoEnum = pagamentoEntity.getTipoPagamentoEnum();
    }

    public PagamentoDTO(){}


    public PagamentoEntity converter() {
        PagamentoEntity pagamento = new PagamentoEntity();
        pagamento.setTipoPagamentoEnum( this.tipoPagamentoEnum );
        return pagamento;
    }

    public Integer getCodigoClienteXPacote() {
        return codigoClienteXPacote;
    }

    public void setCodigoClienteXPacote(Integer codigoClienteXPacote) {
        this.codigoClienteXPacote = codigoClienteXPacote;
    }

    public Integer getCodigoContratacao() {
        return codigoContratacao;
    }

    public void setCodigoContratacao(Integer codigoContratacao) {
        this.codigoContratacao = codigoContratacao;
    }

    public TipoPagamentoEnum getTipoPagamentoEnum() {
        return tipoPagamentoEnum;
    }

    public void setTipoPagamentoEnum(TipoPagamentoEnum tipoPagamentoEnum) {
        this.tipoPagamentoEnum = tipoPagamentoEnum;
    }
}
