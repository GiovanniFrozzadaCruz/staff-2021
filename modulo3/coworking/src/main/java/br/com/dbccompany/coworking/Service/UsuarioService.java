package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.UsuarioDTO;
import br.com.dbccompany.coworking.Entity.UsuarioEntity;
import br.com.dbccompany.coworking.Repository.UsuarioRepository;
import br.com.dbccompany.coworking.Utils.Criptografia;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class UsuarioService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Transactional(rollbackFor = Exception.class)
    public UsuarioEntity save( UsuarioEntity usuarioEntity ) throws Exception {
        String senha = usuarioEntity.getSenha();
        if( senha.length() < 6 ) {
            throw new Exception("Senha tem menos de 6 caracteres");
        }
        usuarioEntity.setSenha(Criptografia.criptografar(usuarioEntity.getSenha()));

        return this.usuarioRepository.save(usuarioEntity);
    }

    @Transactional(rollbackFor = Exception.class)
    public List<UsuarioDTO> findAll() {
        List<UsuarioEntity> usuarios = (List<UsuarioEntity>) this.usuarioRepository.findAll();
        return this.converteLista(usuarios);
    }

    private List<UsuarioDTO> converteLista( List<UsuarioEntity> usuarios ) {
        ArrayList<UsuarioDTO> listaUsuario = new ArrayList<>();
        for(UsuarioEntity usuario : usuarios ) {
            listaUsuario.add( new UsuarioDTO( usuario ) );
        }
        return listaUsuario;
    }

    @Transactional(rollbackFor = Exception.class)
    public UsuarioEntity findByNome( String nome ){
        return this.usuarioRepository.findByNome( nome );
    }

}
