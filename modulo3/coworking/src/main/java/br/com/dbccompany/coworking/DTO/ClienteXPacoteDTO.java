package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.ClienteXPacoteEntity;

public class ClienteXPacoteDTO {
    private Integer codigoCliente;
    private Integer codigoPacote;
    private int quantidade;

    public ClienteXPacoteDTO( ClienteXPacoteEntity clienteXPacoteEntity ) {
        this.quantidade = clienteXPacoteEntity.getQuantidade();
    }

    public ClienteXPacoteDTO() {}

    public ClienteXPacoteEntity converter() {
        ClienteXPacoteEntity clienteXPacote = new ClienteXPacoteEntity();
        clienteXPacote.setQuantidade( this.quantidade );
        return clienteXPacote;
    }

    public Integer getCodigoCliente() {
        return codigoCliente;
    }

    public void setCodigoCliente(Integer codigoCliente) {
        this.codigoCliente = codigoCliente;
    }

    public Integer getCodigoPacote() {
        return codigoPacote;
    }

    public void setCodigoPacote(Integer codigoPacote) {
        this.codigoPacote = codigoPacote;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }
}
