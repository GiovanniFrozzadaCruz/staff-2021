package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.EspacosDTO;
import br.com.dbccompany.coworking.Service.EspacosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping( path = "/Espacos")
public class EspacosController {

    private EspacosService espacosService;

    @Autowired
    public EspacosController( EspacosService espacosService ) {
        this.espacosService = espacosService;
    }

    @PostMapping
    @ResponseBody
    public EspacosDTO save( @RequestBody EspacosDTO espacosDTO ) {
        return this.espacosService.save( espacosDTO.converter() );
    }

    @ResponseBody
    @GetMapping
    public List<EspacosDTO> findAll(){
        return this.espacosService.findAll();
    }
}
