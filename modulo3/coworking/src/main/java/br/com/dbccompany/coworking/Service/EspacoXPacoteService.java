package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.EspacosXpacotesDTO;
import br.com.dbccompany.coworking.Entity.EspacoXPacoteEntity;
import br.com.dbccompany.coworking.Entity.PacoteEntity;
import br.com.dbccompany.coworking.Repository.EspacoXPacotesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class EspacoXPacoteService {

    @Autowired
    private EspacoXPacotesRepository espacoXPacotesRepository;

    @Transactional(rollbackFor = Exception.class)
    public EspacosXpacotesDTO save(EspacoXPacoteEntity espacoXPacoteEntity ) {
        EspacoXPacoteEntity novoEspacoXPacote = this.espacoXPacotesRepository.save( espacoXPacoteEntity );
        return new EspacosXpacotesDTO( novoEspacoXPacote );
    }

    @Transactional(rollbackFor = Exception.class)
    public List<EspacosXpacotesDTO> findAll() {
        List<EspacoXPacoteEntity> contatos = (List<EspacoXPacoteEntity>) this.espacoXPacotesRepository.findAll();
        return this.converteLista(contatos);
    }

    private List<EspacosXpacotesDTO> converteLista( List<EspacoXPacoteEntity> contatos ) {
        ArrayList<EspacosXpacotesDTO> listaContatos = new ArrayList<>();
        for(EspacoXPacoteEntity contato : contatos ) {
            listaContatos.add( new EspacosXpacotesDTO( contato ) );
        }
        return listaContatos;
    }

    public Optional<EspacoXPacoteEntity> findById(Integer codigoEspacoXPacote ) {
        return this.espacoXPacotesRepository.findById(codigoEspacoXPacote);
    }

    public List<EspacoXPacoteEntity> findByPacote(PacoteEntity pacote) {
        return this.espacoXPacotesRepository.findByPacotes( pacote );
    }
}
