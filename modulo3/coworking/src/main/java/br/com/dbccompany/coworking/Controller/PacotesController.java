package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.PacoteDTO;
import br.com.dbccompany.coworking.Service.PacotesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping( path = "/Pacote")
public class PacotesController {

    @Autowired
    private PacotesService pacotesService;

    @ResponseBody
    @PostMapping
    public PacoteDTO save(@RequestBody PacoteDTO pacoteDTO ) {
        return this.pacotesService.save( pacoteDTO.converter() );
    }

    @ResponseBody
    @GetMapping
    public List<PacoteDTO> findAll(){
        return this.pacotesService.findAll();
    }
}
