package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.ClienteEntity;
import br.com.dbccompany.coworking.Entity.ContatoEntity;
import br.com.dbccompany.coworking.Entity.PagamentoEntity;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

public class ClientesDTO {
    protected String nome;
    protected String cpf;
    protected LocalDate dataNascimento;
    protected String email;
    protected String telefone;
    protected List<ContatoEntity> contatos;

    public ClientesDTO(ClienteEntity clienteEntity) {
        this.nome = clienteEntity.getNome();
        this.cpf = clienteEntity.getCpf();
        this.dataNascimento = clienteEntity.getDataNascimento();
        this.contatos = clienteEntity.getContatoEntity();
    }

    public ClientesDTO(  ) {
    }

    public ClienteEntity converter( ) {
        ClienteEntity cliente = new ClienteEntity();
        cliente.setNome( this.nome );
        cliente.setCpf( this.cpf );
        cliente.setDataNascimento( this.dataNascimento );
        return cliente;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public LocalDate getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(LocalDate dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public List<ContatoEntity> getContatos() {
        return contatos;
    }

    public void setContatos(List<ContatoEntity> contatos) {
        this.contatos = contatos;
    }
}
