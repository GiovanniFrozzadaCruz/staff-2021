package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.ContratacaoDTO;
import br.com.dbccompany.coworking.Entity.ContratacaoEntity;
import br.com.dbccompany.coworking.Entity.TipoContratoEnum;
import br.com.dbccompany.coworking.Service.ClientesService;
import br.com.dbccompany.coworking.Service.ContratacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping( path = "/Contratacao")
public class ContratacaoController {

    private ContratacaoService contratacaoService;

    @Autowired
    private ClientesService clientesService;

    @Autowired
    public ContratacaoController( ContratacaoService contratacaoService ) {
        this.contratacaoService = contratacaoService;
    }

    @PostMapping
    @ResponseBody
    public String save(@RequestBody ContratacaoDTO contratacaoDTO ) {
        return this.contratacaoService.salvaContratacao( contratacaoDTO );
    }

    @ResponseBody
    @GetMapping
    public List<ContratacaoEntity> findAll(){
        return this.contratacaoService.findAll();
    }

    @ResponseBody
    @PostMapping(path = "/findById/{id}")
    public ContratacaoEntity findById(@PathVariable Integer id ) {
        return this.contratacaoService.findById( id );
    }

    @ResponseBody
    @PostMapping(path = "/findByTipoEnum")
    public List<ContratacaoDTO> findByTipoContratoEnum(@RequestBody ContratacaoDTO contratacaoDTO ) {
        return this.contratacaoService.findByTipoContratoEnum( contratacaoDTO );
    }


}
