package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.ClienteXPacoteDTO;
import br.com.dbccompany.coworking.Service.ClienteXPacoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping( path = "/ClienteXPacote")
public class ClienteXPacoteController {

    @Autowired
    private ClienteXPacoteService clienteXPacoteService;

    @PostMapping
    @ResponseBody
    public double save (@RequestBody ClienteXPacoteDTO clienteXPacoteDTO ) {
        return this.clienteXPacoteService.retornaValorAPagar( clienteXPacoteDTO );
    }


}
