package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.PagamentoDTO;
import br.com.dbccompany.coworking.Entity.PagamentoEntity;
import br.com.dbccompany.coworking.Service.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping( path = "/Pagamento")
public class PagamentoController {

    @Autowired
    private PagamentoService pagamentoService;

    @PostMapping
    @ResponseBody
    public PagamentoEntity save(@RequestBody PagamentoDTO pagamentoDTO ) {
        return this.pagamentoService.salvaPagamento( pagamentoDTO );
    }



    @ResponseBody
    @GetMapping
    public List<PagamentoDTO> findAll(){
        return this.pagamentoService.findAll();
    }



}
