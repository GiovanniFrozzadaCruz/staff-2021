package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.EspacoXPacoteEntity;
import br.com.dbccompany.coworking.Entity.EspacoEntity;
import br.com.dbccompany.coworking.Entity.PacoteEntity;
import br.com.dbccompany.coworking.Entity.TipoContratoEnum;

public class EspacosXpacotesDTO {
    private TipoContratoEnum tipoContratoEnum;
    private int quantidade;
    private int prazo;
    private EspacoEntity espacos;
    private PacoteEntity pacotes;

    public EspacosXpacotesDTO( EspacoXPacoteEntity espacoXPacoteEntity ) {
        this.tipoContratoEnum = espacoXPacoteEntity.getTipoContratoEnum();
        this.quantidade = espacoXPacoteEntity.getQuantidade();
        this.prazo = espacoXPacoteEntity.getPrazo();
        this.espacos = espacoXPacoteEntity.getEspacos();
        this.pacotes = espacoXPacoteEntity.getPacotes();
    }

    public EspacosXpacotesDTO(){}

    public EspacoXPacoteEntity converter() {
        EspacoXPacoteEntity espacoXPacote = new EspacoXPacoteEntity();
        espacoXPacote.setTipoContratoEnum( this.tipoContratoEnum );
        espacoXPacote.setQuantidade( this.quantidade );
        espacoXPacote.setPrazo( this.prazo );
        espacoXPacote.setEspacos( this.espacos );
        espacoXPacote.setPacotes( this.pacotes );
        return espacoXPacote;
    }

    public TipoContratoEnum getTipoContratoEnum() {
        return tipoContratoEnum;
    }

    public void setTipoContratoEnum(TipoContratoEnum tipoContratoEnum) {
        this.tipoContratoEnum = tipoContratoEnum;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public int getPrazo() {
        return prazo;
    }

    public void setPrazo(int prazo) {
        this.prazo = prazo;
    }

    public EspacoEntity getEspacos() {
        return espacos;
    }

    public void setEspacos(EspacoEntity espacos) {
        this.espacos = espacos;
    }

    public PacoteEntity getPacotes() {
        return pacotes;
    }

    public void setPacotes(PacoteEntity pacotes) {
        this.pacotes = pacotes;
    }
}
