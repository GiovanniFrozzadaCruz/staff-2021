package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.ContatoDTO;
import br.com.dbccompany.coworking.Service.ContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping( path = "/Contato")
public class ContatoController {

    private ContatoService contatoService;

    @Autowired
    public ContatoController( ContatoService contatoService ){
        this.contatoService = contatoService;
    }

    @ResponseBody
    @PostMapping
    public ContatoDTO save(@RequestBody ContatoDTO contatoDTO ) {
        return this.contatoService.save( contatoDTO.converter() );
    }

    @ResponseBody
    @GetMapping
    public List<ContatoDTO> findAll(){
        return this.contatoService.findAll();
    }



}
