package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;

@Entity
public class EspacoXPacoteEntity {
    @Id
    @SequenceGenerator( name = "ESPACOXPACOTE_SEQ", sequenceName = "ESPACOXPACOTE_SEQ" )
    @GeneratedValue( generator = "ESPACOXPACOTE_SEQ", strategy = GenerationType.SEQUENCE)
    protected Integer Id;

    @ManyToOne( cascade = CascadeType.PERSIST)
    private EspacoEntity espacos;

    @ManyToOne( cascade = CascadeType.PERSIST)
    private PacoteEntity pacotes;

    @Column(nullable = false)
    private TipoContratoEnum tipoContratoEnum;

    @Column(nullable = false)
    private int quantidade;

    @Column(nullable = false)
    private Integer prazo;

    public EspacoXPacoteEntity(EspacoEntity espacos, PacoteEntity pacotes, TipoContratoEnum tipoContratoEnum, int quantidade, Integer prazo) {
        this.espacos = espacos;
        this.pacotes = pacotes;
        this.tipoContratoEnum = tipoContratoEnum;
        this.quantidade = quantidade;
        this.prazo = prazo;
    }

    public EspacoXPacoteEntity() {

    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public EspacoEntity getEspacos() {
        return espacos;
    }

    public void setEspacos(EspacoEntity espacos) {
        this.espacos = espacos;
    }

    public PacoteEntity getPacotes() {
        return pacotes;
    }

    public void setPacotes(PacoteEntity pacotes) {
        this.pacotes = pacotes;
    }

    public TipoContratoEnum getTipoContratoEnum() {
        return tipoContratoEnum;
    }

    public void setTipoContratoEnum(TipoContratoEnum tipoContratoEnum) {
        this.tipoContratoEnum = tipoContratoEnum;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public Integer getPrazo() {
        return prazo;
    }

    public void setPrazo(Integer prazo) {
        this.prazo = prazo;
    }
}
