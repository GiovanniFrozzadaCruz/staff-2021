package br.com.dbccompany.coworking.Exception.handlers;

import br.com.dbccompany.coworking.Exception.Exceptions.EspacoNuloException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

public class EspacoNuloHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler( EspacoNuloException.class )
    public ResponseEntity handleException( EspacoNuloException e ) {
        return new ResponseEntity(e, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
