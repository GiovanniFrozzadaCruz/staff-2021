package br.com.dbccompany.coworking.Exception.Exceptions;

public class EspacoNuloNaContratacaoException extends RuntimeException{
    private static final long serialVersionUID = 1L;

    public EspacoNuloNaContratacaoException() {
        super("Espaco invalido na contratacao.");
    }
}
