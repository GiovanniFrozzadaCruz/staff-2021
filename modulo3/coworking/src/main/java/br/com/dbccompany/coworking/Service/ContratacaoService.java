package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.ClientesDTO;
import br.com.dbccompany.coworking.DTO.ContratacaoDTO;
import br.com.dbccompany.coworking.Entity.*;
import br.com.dbccompany.coworking.Exception.Exceptions.ClienteNuloException;
import br.com.dbccompany.coworking.Exception.Exceptions.EspacoNuloNaContratacaoException;
import br.com.dbccompany.coworking.Repository.ContratacaoRepository;
import br.com.dbccompany.coworking.Utils.ConversorReais;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ContratacaoService {

    @Autowired
    private ContratacaoRepository contratacaoRepository;

    @Autowired
    private ClientesService clientesService;

    @Autowired
    private EspacosService espacosService;

    @Transactional(rollbackFor = Exception.class)
    public ContratacaoEntity save(ContratacaoEntity contratacaoEntity ) {
        return this.contratacaoRepository.save( contratacaoEntity );
    }

    @Transactional(rollbackFor = Exception.class)
    public List<ContratacaoEntity> findAll() {
        return (List<ContratacaoEntity>) this.contratacaoRepository.findAll();
    }

    private List<ContratacaoDTO> converteLista( List<ContratacaoEntity> contratacoes ) {
        ArrayList<ContratacaoDTO> listaContratacao = new ArrayList<>();
        for(ContratacaoEntity contratacao : contratacoes ) {
            listaContratacao.add( new ContratacaoDTO( contratacao ) );
        }
        return listaContratacao;
    }

    public String salvaContratacao( ContratacaoDTO contratacaoDTO ) {
        EspacoEntity espaco = this.espacosService.findById( contratacaoDTO.getCodigoEspaco() );
        ClienteEntity cliente = this.clientesService.findById( contratacaoDTO.getCodigoCliente() );

        if( cliente == null) {
            throw new ClienteNuloException();
        }
        if( espaco == null ) {
            throw new EspacoNuloNaContratacaoException();
        }

        ContratacaoEntity contratacao = contratacaoDTO.converter();
        contratacao.setCliente( cliente );
        contratacao.setEspaco( espaco );
        this.save( contratacao );

        double diariaEspaco = espaco.getValor();
        double valorTabelado;

        switch (contratacaoDTO.getTipoContratoEnum()) {
            case MES: valorTabelado = diariaEspaco * 30; break;
            case SEMANA: valorTabelado = diariaEspaco * 7; break;
            case DIARIA: valorTabelado = diariaEspaco; break;
            case TURNO: valorTabelado = diariaEspaco * ( 5/24 ); break;
            case HORA: valorTabelado = diariaEspaco / 24; break;
            case MINUTO: valorTabelado = diariaEspaco / 1440; break;
            default: return "";
        }
        double custo = valorTabelado * contratacaoDTO.getQuantidade() - contratacaoDTO.getDesconto();
        return ConversorReais.toReais( custo );
    }

    public ContratacaoEntity findById( Integer codigoContratacao ) {
        Optional<ContratacaoEntity> contratacaoOPT = this.contratacaoRepository.findById( codigoContratacao );
        if(contratacaoOPT.isPresent()) {
            return contratacaoOPT.get();
        }
        return null;
    }

    public List<ContratacaoDTO> findByTipoContratoEnum( ContratacaoDTO contratacaoDTO ) {
        return this.contratacaoRepository.findByTipoContratoEnum( contratacaoDTO.getTipoContratoEnum() );
    }
}
