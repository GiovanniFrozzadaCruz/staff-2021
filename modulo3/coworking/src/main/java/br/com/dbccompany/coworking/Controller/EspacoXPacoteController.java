package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.ContatoDTO;
import br.com.dbccompany.coworking.DTO.EspacosXpacotesDTO;
import br.com.dbccompany.coworking.Service.EspacoXPacoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping( path = "/EspacoXPacote" )
public class EspacoXPacoteController {

    @Autowired
    private EspacoXPacoteService espacoXPacoteService;

    @PostMapping
    @ResponseBody
    public EspacosXpacotesDTO save (@RequestBody EspacosXpacotesDTO espacosXpacotesDTO ) {
        return this.espacoXPacoteService.save( espacosXpacotesDTO.converter() );
    }


    @ResponseBody
    @GetMapping
    public List<EspacosXpacotesDTO> findAll(){
        return this.espacoXPacoteService.findAll();
    }


}
