package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.PacoteEntity;

public class PacoteDTO {
    private double valor;

    public PacoteDTO( PacoteEntity pacoteEntity ) {
        this.valor = pacoteEntity.getValor();
    }

    public PacoteDTO() {
    }

    public PacoteEntity converter() {
        PacoteEntity pacoteEntity = new PacoteEntity();
        pacoteEntity.setValor( this.valor );
        return pacoteEntity;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }
}
