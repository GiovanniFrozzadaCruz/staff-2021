package br.com.dbccompany.coworking.Exception.handlers;

import br.com.dbccompany.coworking.Exception.Exceptions.EmailInvalidoException;
import br.com.dbccompany.coworking.Exception.Exceptions.TelefoneInvalidoException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class TelefoneInvalidoHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler( TelefoneInvalidoException.class )
    public ResponseEntity handleException( Exception e ) {
        return new ResponseEntity( e, HttpStatus.INTERNAL_SERVER_ERROR );
    }

}
