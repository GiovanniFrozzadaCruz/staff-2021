package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.PagamentoDTO;
import br.com.dbccompany.coworking.Entity.*;
import br.com.dbccompany.coworking.Repository.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class PagamentoService {

    @Autowired
    private PagamentoRepository pagamentoRepository;

    @Autowired
    private ClientesService clientesService;

    @Autowired
    private ClienteXPacoteService clienteXPacoteService;

    @Autowired
    private ContratacaoService contratacaoService;

    @Autowired
    private SaldoXClientesService saldoXClientesService;

    @Autowired
    private EspacosService espacoService;

    @Autowired
    private EspacoXPacoteService espacoXPacoteService;

    @Transactional(rollbackFor = Exception.class)
    public PagamentoEntity save( PagamentoEntity pagamentoEntity ) {
        return this.pagamentoRepository.save( pagamentoEntity );
    }

    @Transactional(rollbackFor = Exception.class)
    public PagamentoEntity salvaPagamento(PagamentoDTO pagamentoDTO ) {
        if(pagamentoDTO.getCodigoClienteXPacote() != null) {
            ClienteXPacoteEntity clienteXPacote = clienteXPacoteService.findById(pagamentoDTO.getCodigoClienteXPacote());
            ClienteEntity cliente = clienteXPacote.getClientes();
            PacoteEntity pacote = clienteXPacote.getPacote();
            List<EspacoXPacoteEntity> espacoXPacote = espacoXPacoteService.findByPacote(pacote);
            for( EspacoXPacoteEntity e : espacoXPacote ) {
                SaldoXClienteId id = new SaldoXClienteId();
                id.setCliente(cliente);
                id.setEspacos(e.getEspacos());
                SaldoXClientesEntity saldocliente = new SaldoXClientesEntity();
                saldocliente.setSaldoXClienteId(id);
                saldocliente.setQuantidade(clienteXPacote.getQuantidade());
                saldocliente.setVencimento(saldoXClientesService.calcularDataVencimento(e.getPrazo()));
                saldocliente.setTipoContratoEnum(e.getTipoContratoEnum());
                saldoXClientesService.save(saldocliente);
            }
        } else if( pagamentoDTO.getCodigoContratacao() != null ) {
            SaldoXClienteId id = new SaldoXClienteId();
            SaldoXClientesEntity saldocliente = new SaldoXClientesEntity();
            ContratacaoEntity contratacaoEntity = contratacaoService.findById(pagamentoDTO.getCodigoContratacao());
            pagamentoDTO.setCodigoContratacao(contratacaoEntity.getId());
            Integer idCliente = contratacaoEntity.getCliente().getId();
            Integer idEspaco = contratacaoEntity.getEspaco().getId();
            id.setCliente(clientesService.findById(idCliente));
            id.setEspacos(espacoService.findById(idEspaco));
            saldocliente.setQuantidade(contratacaoEntity.getQuantidade());
            saldocliente.setTipoContratoEnum(contratacaoEntity.getTipoContratoEnum());
            saldocliente.setVencimento(saldoXClientesService.calcularDataVencimento(contratacaoEntity.getPrazo()));
            if(saldoXClientesService.findById(id.getCliente().getId(), id.getEspacos().getId()) != null) {
                saldoXClientesService.save(saldocliente);
            }
        }
        return this.pagamentoRepository.save(pagamentoDTO.converter());
    }


    @Transactional(rollbackFor = Exception.class)
    public List<PagamentoDTO> findAll() {
        List<PagamentoEntity> pagamentos = (List<PagamentoEntity>) this.pagamentoRepository.findAll();
        return this.converteLista(pagamentos);
    }

    private List<PagamentoDTO> converteLista( List<PagamentoEntity> pagamentos ) {
        ArrayList<PagamentoDTO> listaPagamentos = new ArrayList<>();
        for(PagamentoEntity pagamento : pagamentos ) {
            listaPagamentos.add( new PagamentoDTO( pagamento ) );
        }
        return listaPagamentos;
    }


}
