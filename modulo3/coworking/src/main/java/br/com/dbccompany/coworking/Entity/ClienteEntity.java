package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;
import javax.xml.crypto.Data;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Entity
public class ClienteEntity {
    @Id
    @SequenceGenerator( name = "CLIENTES_SEQ", sequenceName = "CLIENTES_SEQ" )
    @GeneratedValue( generator = "CLIENTES_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column( name = "nome", nullable = false )
    private String nome;

    @Column( name = "cpf", nullable = false, unique = true )
    private String cpf;

    @Column( name = "dataNascimento", nullable = false )
    private LocalDate dataNascimento;

    @OneToMany( cascade=CascadeType.PERSIST )
    @JoinColumn( name = "Contato_id" )
    private List<ContatoEntity> contatos;

    @OneToMany( cascade=CascadeType.PERSIST )
    @JoinColumn( name = "Contratacao_id" )
    private List<ContratacaoEntity> contratacao;



    public ClienteEntity(Integer id, String nome, String cpf, LocalDate dataNascimento, List<ContatoEntity> contatos) {
        this.id = id;
        this.nome = nome;
        this.cpf = cpf;
        this.dataNascimento = dataNascimento;
        this.contatos = contatos;
    }

    public ClienteEntity() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public LocalDate getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(LocalDate dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public List<ContatoEntity> getContatoEntity() {
        return contatos;
    }

    public void setContatoEntity(List<ContatoEntity> contatoEntity) {
        this.contatos = contatoEntity;
    }

    public List<ContatoEntity> getContatos() {
        return contatos;
    }

    public void setContatos(List<ContatoEntity> contatos) {
        this.contatos = contatos;
    }

    public List<ContratacaoEntity> getContratacao() {
        return contratacao;
    }

    public void setContratacao(List<ContratacaoEntity> contratacao) {
        this.contratacao = contratacao;
    }
}
