package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
public class AcessoEntity {
    @Id
    @SequenceGenerator( name = "ACESSO_SEQ", sequenceName = "ACESSO_SEQ" )
    @GeneratedValue( generator = "ACESSO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @ManyToOne(cascade = CascadeType.MERGE)
    private SaldoXClientesEntity saldoXCliente;

    @Column(name = "is_entrada")
    private boolean isEntrada = false;

    @Column(name = "is_excecao")
    private boolean isExcecao = false;

    private LocalDateTime data = LocalDateTime.now();

    public AcessoEntity(Integer id, SaldoXClientesEntity saldoXCliente, boolean isEntrada, boolean isExcecao, LocalDateTime data) {
        this.id = id;
        this.saldoXCliente = saldoXCliente;
        this.isEntrada = isEntrada;
        this.isExcecao = isExcecao;
        this.data = data;
    }

    public AcessoEntity() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public SaldoXClientesEntity getSaldoXCliente() {
        return saldoXCliente;
    }

    public void setSaldoXCliente(SaldoXClientesEntity saldoXCliente) {
        this.saldoXCliente = saldoXCliente;
    }

    public boolean isEntrada() {
        return isEntrada;
    }

    public void setEntrada(boolean entrada) {
        isEntrada = entrada;
    }

    public boolean isExcecao() {
        return isExcecao;
    }

    public void setExcecao(boolean excecao) {
        isExcecao = excecao;
    }

    public LocalDateTime getData() {
        return data;
    }

    public void setData(LocalDateTime data) {
        this.data = data;
    }
}
