package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;

@Entity
public class ContatoEntity {
    @Id
    @SequenceGenerator( name = "CONTRATACAO_SEQ", sequenceName = "CONTRATACAO_SEQ" )
    @GeneratedValue( generator = "CONTRATACAO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer Id;

    @ManyToOne( cascade=CascadeType.PERSIST )
    @JoinColumn(name = "tipoContato_id" )
    private TipoContatoEntity tipoContato;

    @Column( name = "valor" )
    private String valor;

    public ContatoEntity(TipoContatoEntity tipoContato, String valor) {
        this.tipoContato = tipoContato;
        this.valor = valor;
    }

    public ContatoEntity() {}

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public TipoContatoEntity getTipoContato() {
        return tipoContato;
    }

    public void setTipoContato(TipoContatoEntity tipoContato) {
        this.tipoContato = tipoContato;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }
}
