package br.com.dbccompany.coworking;

import br.com.dbccompany.coworking.Entity.UsuarioEntity;
import br.com.dbccompany.coworking.Service.UsuarioService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class CoworkingApplication {

	public static void main(String[] args) {
		SpringApplication.run(CoworkingApplication.class, args);
	}

	@Bean
	public CommandLineRunner hml(UsuarioService service){
		UsuarioEntity principal = new UsuarioEntity();
		principal.setEmail("dbc@admin.com");
		principal.setNome("Administrador");
		principal.setLogin("dbccompany");
		principal.setSenha("dbccompany");
		return args -> {
			service.save(principal);
		};
	}
}
