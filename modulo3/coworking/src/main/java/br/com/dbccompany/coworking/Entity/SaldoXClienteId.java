package br.com.dbccompany.coworking.Entity;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Embeddable
public class SaldoXClienteId implements Serializable {
    private static final long serialVersionUID = 1L;

    @JoinColumn( name = "cliente_id")
    @ManyToOne(optional = false)
    private ClienteEntity cliente;

    @JoinColumn( name = "espaco_id")
    @ManyToOne(optional = false)
    private EspacoEntity espacos;

    public SaldoXClienteId(ClienteEntity cliente, EspacoEntity espacos) {
        this.cliente = cliente;
        this.espacos = espacos;
    }

    public SaldoXClienteId() {

    }

    public ClienteEntity getCliente() {
        return cliente;
    }

    public void setCliente(ClienteEntity cliente) {
        this.cliente = cliente;
    }

    public EspacoEntity getEspacos() {
        return espacos;
    }

    public void setEspacos(EspacoEntity espacos) {
        this.espacos = espacos;
    }


}
