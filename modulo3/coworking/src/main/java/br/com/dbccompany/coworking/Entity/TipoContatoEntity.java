package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;

@Entity
public class TipoContatoEntity {
    @Id
    @SequenceGenerator( name = "TIPOCONTATO_SEQ", sequenceName = "TIPOCONTATO_SEQ" )
    @GeneratedValue( generator = "TIPOCONTATO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer Id;

    @Column( nullable = false, unique = true )
    private TipoContatoEnum tipoContatoEnum;

    public TipoContatoEntity( TipoContatoEnum tipoContatoEnum ) {
        this.tipoContatoEnum = tipoContatoEnum;
    }

    public TipoContatoEntity() {

    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public TipoContatoEnum getTipoContatoEnum() {
        return tipoContatoEnum;
    }

    public void setTipoContatoEnum(TipoContatoEnum tipoContatoEnum) {
        this.tipoContatoEnum = tipoContatoEnum;
    }

}
