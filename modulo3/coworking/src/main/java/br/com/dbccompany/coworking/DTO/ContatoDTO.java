package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.ContatoEntity;
import br.com.dbccompany.coworking.Entity.TipoContatoEntity;

public class ContatoDTO {
    protected TipoContatoEntity tipoContato;
    protected String valor;

    public ContatoDTO( ContatoEntity contatoEntity ) {
        this.tipoContato = contatoEntity.getTipoContato();
        this.valor = contatoEntity.getValor();
    }

    public ContatoDTO() {

    }

    public ContatoEntity converter() {
        ContatoEntity contato = new ContatoEntity();
        contato.setTipoContato( this.tipoContato );
        contato.setValor( this.valor );
        return contato;
    }

    public TipoContatoEntity getTipoContato() {
        return tipoContato;
    }

    public void setTipoContato(TipoContatoEntity tipoContato) {
        this.tipoContato = tipoContato;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }
}
