package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.SaldoXClientesDTO;
import br.com.dbccompany.coworking.Entity.SaldoXClientesEntity;
import br.com.dbccompany.coworking.Service.SaldoXClientesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping( path = "/saldo" )
public class SaldoXClientesController {

    @Autowired
    private SaldoXClientesService saldoXClientesService;

    @PostMapping
    @ResponseBody
    public SaldoXClientesEntity save (@RequestBody SaldoXClientesDTO saldoXClientesDTO ) {
        return this.saldoXClientesService.save( saldoXClientesDTO.converter() );
    }

}
