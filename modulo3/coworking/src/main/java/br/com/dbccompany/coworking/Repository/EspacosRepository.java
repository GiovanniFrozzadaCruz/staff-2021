package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.EspacoEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface EspacosRepository extends CrudRepository<EspacoEntity, Integer> {

    Optional<EspacoEntity> findById(Integer codigoEspaco );
}
