package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.ContatoDTO;
import br.com.dbccompany.coworking.DTO.TipoContatoDTO;
import br.com.dbccompany.coworking.DTO.UsuarioDTO;
import br.com.dbccompany.coworking.Entity.TipoContatoEntity;
import br.com.dbccompany.coworking.Entity.TipoContatoEnum;
import br.com.dbccompany.coworking.Service.TipoContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping( path = "/TipoContato")
public class TipoContatoController {

    private TipoContatoService tipoContatoService;

    @Autowired
    public TipoContatoController( TipoContatoService tipoContatoService ) {
        this.tipoContatoService = tipoContatoService;
    }

    @PostMapping
    @ResponseBody
    public TipoContatoDTO save(@RequestBody TipoContatoDTO tipoContatoDTO ) {
        return this.tipoContatoService.save( tipoContatoDTO.converter() );
    }

    @ResponseBody
    @PostMapping(path = "/TipoContatoEnum")
    public List<TipoContatoDTO> findAllByTipoContatoEnum(@RequestBody TipoContatoDTO tipoContatoDTO ) {
        return this.tipoContatoService.findAllByTipoContatoEnum( tipoContatoDTO );
    }
}
