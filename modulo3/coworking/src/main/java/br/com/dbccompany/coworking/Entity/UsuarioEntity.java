package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;

@Entity
public class UsuarioEntity {
    @Id
    @SequenceGenerator( name = "USUARIO_SEQ", sequenceName = "USUARIO_SEQ" )
    @GeneratedValue( generator = "USUARIO_SEQ", strategy = GenerationType.SEQUENCE)
    protected Integer Id;

    @Column( nullable = false )
    protected String nome;

    @Column( nullable = false, unique = true )
    protected String email;

    @Column( nullable = false, unique = true )
    protected String login;

    @Column( nullable = false )
    protected String senha;

    public UsuarioEntity(String nome, String email, String login, String senha) {
        this.nome = nome;
        this.email = email;
        this.login = login;
        this.senha = senha;
    }

    public UsuarioEntity() {
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
}

