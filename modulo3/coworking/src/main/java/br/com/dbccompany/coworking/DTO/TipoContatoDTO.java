package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.TipoContatoEntity;
import br.com.dbccompany.coworking.Entity.TipoContatoEnum;

public class TipoContatoDTO {
    private TipoContatoEnum tipoContatoEnum;

    public TipoContatoDTO( TipoContatoEntity tipoContatoEntity ) {
        this.tipoContatoEnum = tipoContatoEntity.getTipoContatoEnum();
    }

    public TipoContatoDTO(){}

    public TipoContatoEntity converter() {
        TipoContatoEntity tipoContatoEntity = new TipoContatoEntity();
        tipoContatoEntity.setTipoContatoEnum( this.tipoContatoEnum );
        return tipoContatoEntity;
    }

    public TipoContatoEnum getTipoContatoEnum() {
        return tipoContatoEnum;
    }

    public void setTipoContatoEnum(TipoContatoEnum tipoContatoEnum) {
        this.tipoContatoEnum = tipoContatoEnum;
    }
}
