package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.EspacosDTO;
import br.com.dbccompany.coworking.Entity.EspacoEntity;
import br.com.dbccompany.coworking.Exception.Exceptions.NomeInvalidoException;
import br.com.dbccompany.coworking.Repository.EspacosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class EspacosService {

    @Autowired
    private EspacosRepository espacosRepository;


    @Transactional(rollbackFor = Exception.class)
    public EspacosDTO save( EspacoEntity espacoEntity) {
        if(espacoEntity.getNome().isEmpty()){
            throw new NomeInvalidoException();
        }
        EspacoEntity novoEspaco = this.espacosRepository.save(espacoEntity);
        return new EspacosDTO( novoEspaco );
    }

    @Transactional(rollbackFor = Exception.class)
    public List<EspacosDTO> findAll() {
        List<EspacoEntity> espacos = (List<EspacoEntity>) this.espacosRepository.findAll();
        return this.converteLista(espacos);
    }

    private List<EspacosDTO> converteLista( List<EspacoEntity> espacos ) {
        ArrayList<EspacosDTO> listaEspacos = new ArrayList<>();
        for(EspacoEntity espaco : espacos ) {
            listaEspacos.add( new EspacosDTO( espaco ) );
        }
        return listaEspacos;
    }

    public EspacoEntity findById( Integer codigoEspaco ) {
        Optional<EspacoEntity> espacoOPT = this.espacosRepository.findById( codigoEspaco );
        if(espacoOPT.isPresent()) {
            return espacoOPT.get();
        }
        return null;
    }

}
