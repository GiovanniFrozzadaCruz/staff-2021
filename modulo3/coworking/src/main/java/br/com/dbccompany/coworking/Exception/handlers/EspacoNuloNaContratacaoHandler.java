package br.com.dbccompany.coworking.Exception.handlers;

import br.com.dbccompany.coworking.Exception.Exceptions.EmailInvalidoException;
import br.com.dbccompany.coworking.Exception.Exceptions.EspacoNuloNaContratacaoException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

public class EspacoNuloNaContratacaoHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler( EspacoNuloNaContratacaoException.class )
    public ResponseEntity handleException(EspacoNuloNaContratacaoException e ) {
        return new ResponseEntity(e, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}