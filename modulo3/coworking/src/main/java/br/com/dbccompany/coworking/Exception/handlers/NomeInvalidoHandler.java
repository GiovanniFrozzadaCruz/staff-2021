package br.com.dbccompany.coworking.Exception.handlers;


import br.com.dbccompany.coworking.Exception.Exceptions.NomeInvalidoException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class NomeInvalidoHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler( NomeInvalidoException.class )
    public ResponseEntity handleException(NomeInvalidoHandler e ) {
        return new ResponseEntity(e, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
