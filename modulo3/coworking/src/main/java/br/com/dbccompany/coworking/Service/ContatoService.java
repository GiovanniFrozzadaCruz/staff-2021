package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.ContatoDTO;
import br.com.dbccompany.coworking.DTO.UsuarioDTO;
import br.com.dbccompany.coworking.Entity.ContatoEntity;
import br.com.dbccompany.coworking.Entity.UsuarioEntity;
import br.com.dbccompany.coworking.Repository.ContatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class ContatoService {

    @Autowired
    private ContatoRepository contatoRepository;

    public ContatoDTO save( ContatoEntity contatoEntity ) {
        ContatoEntity novoContato = this.contatoRepository.save( contatoEntity );
        return new ContatoDTO(novoContato);
    }

    @Transactional(rollbackFor = Exception.class)
    public List<ContatoDTO> findAll() {
        List<ContatoEntity> contatos = (List<ContatoEntity>) this.contatoRepository.findAll();
        return this.converteLista(contatos);
    }

    private List<ContatoDTO> converteLista( List<ContatoEntity> contatos ) {
        ArrayList<ContatoDTO> listaContatos = new ArrayList<>();
        for(ContatoEntity contato : contatos ) {
            listaContatos.add( new ContatoDTO( contato ) );
        }
        return listaContatos;
    }



}
