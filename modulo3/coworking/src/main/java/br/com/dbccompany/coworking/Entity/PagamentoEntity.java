package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;
import java.util.Optional;

@Entity
public class PagamentoEntity {
    @Id
    @SequenceGenerator( name = "PAGAMENTO_SEQ", sequenceName = "PAGAMENTO_SEQ" )
    @GeneratedValue( generator = "PAGAMENTO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @ManyToOne( cascade = CascadeType.PERSIST )
    private ClienteXPacoteEntity clienteXPacote;

    @ManyToOne( cascade = CascadeType.PERSIST )
    private ContratacaoEntity contratacao;

    private TipoPagamentoEnum tipoPagamentoEnum;

    public PagamentoEntity(ClienteXPacoteEntity clienteXPacote, ContratacaoEntity contratacao, TipoPagamentoEnum tipoPagamentoEnum) {
        this.clienteXPacote = clienteXPacote;
        this.contratacao = contratacao;
        this.tipoPagamentoEnum = tipoPagamentoEnum;
    }

    public PagamentoEntity() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ClienteXPacoteEntity getClienteXPacote() {
        return clienteXPacote;
    }

    public void setClienteXPacote(ClienteXPacoteEntity clienteXPacote) {
        this.clienteXPacote = clienteXPacote;
    }

    public ContratacaoEntity getContratacao() {
        return contratacao;
    }

    public void setContratacao(ContratacaoEntity contratacao) {
        this.contratacao = contratacao;
    }

    public TipoPagamentoEnum getTipoPagamentoEnum() {
        return tipoPagamentoEnum;
    }

    public void setTipoPagamentoEnum(TipoPagamentoEnum tipoPagamentoEnum) {
        this.tipoPagamentoEnum = tipoPagamentoEnum;
    }
}
