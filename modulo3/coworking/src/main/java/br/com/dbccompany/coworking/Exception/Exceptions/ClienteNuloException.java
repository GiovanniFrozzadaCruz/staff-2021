package br.com.dbccompany.coworking.Exception.Exceptions;

public class ClienteNuloException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public ClienteNuloException() {
        super("Cliente invalido.");
    }
}
