package br.com.dbccompany.coworking.Entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

@Entity
public class SaldoXClientesEntity {

    @EmbeddedId
    private SaldoXClienteId saldoXClienteId;

    @Column( nullable = false )
    private TipoContratoEnum tipoContratoEnum;

    @Column( nullable = false )
    private int quantidade;

    @Column( nullable = false )
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime vencimento;

    public SaldoXClientesEntity(SaldoXClienteId saldoXClienteId, TipoContratoEnum tipoContratoEnum, int quantidade, LocalDateTime vencimento) {
        this.saldoXClienteId = saldoXClienteId;
        this.tipoContratoEnum = tipoContratoEnum;
        this.quantidade = quantidade;
        this.vencimento = vencimento;
    }

    public SaldoXClientesEntity() {

    }

    public SaldoXClienteId getSaldoXClienteId() {
        return saldoXClienteId;
    }

    public void setSaldoXClienteId(SaldoXClienteId saldoXClienteId) {
        this.saldoXClienteId = saldoXClienteId;
    }

    public TipoContratoEnum getTipoContratoEnum() {
        return tipoContratoEnum;
    }

    public void setTipoContratoEnum(TipoContratoEnum tipoContratoEnum) {
        this.tipoContratoEnum = tipoContratoEnum;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public LocalDateTime getVencimento() {
        return vencimento;
    }

    public void setVencimento(LocalDateTime vencimento) {
        this.vencimento = vencimento;
    }
}
