package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.UsuarioDTO;
import br.com.dbccompany.coworking.Entity.UsuarioEntity;
import br.com.dbccompany.coworking.Service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping( path = "/Usuario" )
public class UsuarioController {

    private UsuarioService usuarioService;

    @Autowired
    public UsuarioController( UsuarioService usuarioService ) {
        this.usuarioService = usuarioService;
    }

    @PostMapping
    @ResponseBody
    public UsuarioEntity save(@RequestBody UsuarioEntity usuarioEntity ) throws Exception {
        if(usuarioEntity != null){
            return this.usuarioService.save( usuarioEntity );
        } else {
            return null;
        }

    }

    @ResponseBody
    @GetMapping
    public List<UsuarioDTO> findAll(){
        return this.usuarioService.findAll();
    }

}
