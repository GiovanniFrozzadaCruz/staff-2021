package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.SaldoXClientesEntity;
import br.com.dbccompany.coworking.Entity.TipoContratoEnum;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

public class SaldoXClientesDTO {
    private Integer clientes;
    private Integer espacos;
    private TipoContratoEnum tipoContratoEnum;
    private int quantidade;
    private LocalDateTime vencimento;


    public SaldoXClientesDTO(SaldoXClientesEntity saldoXClientesEntity ) {
        this.tipoContratoEnum = saldoXClientesEntity.getTipoContratoEnum();
        this.quantidade = saldoXClientesEntity.getQuantidade();
        this.vencimento = saldoXClientesEntity.getVencimento();
    }

    public SaldoXClientesDTO( ) {
    }

    public SaldoXClientesEntity converter() {
        SaldoXClientesEntity saldoXClientes = new SaldoXClientesEntity();
        saldoXClientes.setTipoContratoEnum( this.tipoContratoEnum );
        saldoXClientes.setQuantidade( this.quantidade );
        saldoXClientes.setVencimento( this.vencimento );
        return saldoXClientes;
    }

    public Integer getClientes() {
        return clientes;
    }

    public void setClientes(Integer clientes) {
        this.clientes = clientes;
    }

    public Integer getEspacos() {
        return espacos;
    }

    public void setEspacos(Integer espacos) {
        this.espacos = espacos;
    }

    public TipoContratoEnum getTipoContratoEnum() {
        return tipoContratoEnum;
    }

    public void setTipoContratoEnum(TipoContratoEnum tipoContratoEnum) {
        this.tipoContratoEnum = tipoContratoEnum;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public LocalDateTime getVencimento() {
        return vencimento;
    }

    public void setVencimento(LocalDateTime vencimento) {
        this.vencimento = vencimento;
    }
}
