package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.DTO.PacoteDTO;
import br.com.dbccompany.coworking.Entity.PacoteEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PacotesRepository extends CrudRepository<PacoteEntity, Integer> {
    List<PacoteDTO> findByValor(double valor );

/*

    List<PacoteDTO> findByPacote(PacoteDTO pacoteDTO);



 */
}
