package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.ClienteXPacoteEntity;
import br.com.dbccompany.coworking.Entity.EspacoXPacoteEntity;
import br.com.dbccompany.coworking.Entity.PacoteEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface ClienteXPacoteRepository extends CrudRepository<ClienteXPacoteEntity, Integer> {
    Optional<ClienteXPacoteEntity> findById (Integer codigo );
    //List<EspacoXPacoteEntity> findByPacote(PacoteEntity pacote);
}

