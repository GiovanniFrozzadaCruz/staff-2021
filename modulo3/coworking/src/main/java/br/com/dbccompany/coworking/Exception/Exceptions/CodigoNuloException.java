package br.com.dbccompany.coworking.Exception.Exceptions;

public class CodigoNuloException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public CodigoNuloException() {
        super("Codigo invalido.");
    }

}
