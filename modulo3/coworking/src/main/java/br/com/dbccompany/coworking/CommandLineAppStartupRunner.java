package br.com.dbccompany.coworking;

import br.com.dbccompany.coworking.Entity.TipoContatoEntity;
import br.com.dbccompany.coworking.Entity.TipoContatoEnum;
import br.com.dbccompany.coworking.Service.TipoContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@Profile( "!uniti-test" )
public class CommandLineAppStartupRunner implements CommandLineRunner {

    @Autowired
    private TipoContatoService tipoContatoService;

    public void run(String... args) {
        tipoContatoService.save(new TipoContatoEntity(TipoContatoEnum.EMAIL));
        tipoContatoService.save(new TipoContatoEntity(TipoContatoEnum.TELEFONE));
    }
}
