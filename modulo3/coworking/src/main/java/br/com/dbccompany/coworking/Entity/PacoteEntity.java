package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class PacoteEntity {
    @Id
    @SequenceGenerator( name = "PACOTES_SEQ", sequenceName = "PACOTES_SEQ" )
    @GeneratedValue( generator = "PACOTES_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer Id;

    @Column( name = "valor",  nullable = false )
    private double valor;

    @OneToMany
    private List<EspacoXPacoteEntity> espacoXPacote;

    @OneToMany
    private List<ClienteXPacoteEntity> clienteXPacote;

    public PacoteEntity(double valor) {
        this.valor = valor;
    }

    public PacoteEntity() {
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public List<EspacoXPacoteEntity> getEspacoXPacote() {
        return espacoXPacote;
    }

    public void setEspacoXPacote(List<EspacoXPacoteEntity> espacoXPacote) {
        this.espacoXPacote = espacoXPacote;
    }

    public List<ClienteXPacoteEntity> getClienteXPacote() {
        return clienteXPacote;
    }

    public void setClienteXPacote(List<ClienteXPacoteEntity> clienteXPacote) {
        this.clienteXPacote = clienteXPacote;
    }
}
