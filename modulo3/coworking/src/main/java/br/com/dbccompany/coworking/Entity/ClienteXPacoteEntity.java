package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;

@Entity
public class ClienteXPacoteEntity {
    @Id
    @SequenceGenerator( name = "CLIENTEXPACOTE_SEQ", sequenceName = "CLIENTEXPACOTE_SEQ" )
    @GeneratedValue( generator = "CLIENTEXPACOTE_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @ManyToOne
    public ClienteEntity clientes;

    @ManyToOne
    public PacoteEntity pacote;

    @Column( nullable = false )
    private int quantidade;

    public ClienteXPacoteEntity(ClienteEntity clientes, PacoteEntity pacote, int quantidade) {
        this.clientes = clientes;
        this.pacote = pacote;
        this.quantidade = quantidade;
    }

    public ClienteXPacoteEntity() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ClienteEntity getClientes() {
        return clientes;
    }

    public void setClientes(ClienteEntity clientes) {
        this.clientes = clientes;
    }

    public PacoteEntity getPacote() {
        return pacote;
    }

    public void setPacote(PacoteEntity pacote) {
        this.pacote = pacote;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }
}
