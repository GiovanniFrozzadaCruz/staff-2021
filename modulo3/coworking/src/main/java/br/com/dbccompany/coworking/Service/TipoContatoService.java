package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.ContatoDTO;
import br.com.dbccompany.coworking.DTO.TipoContatoDTO;
import br.com.dbccompany.coworking.Entity.TipoContatoEntity;
import br.com.dbccompany.coworking.Entity.TipoContatoEnum;
import br.com.dbccompany.coworking.Repository.TipoContatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class TipoContatoService {

    @Autowired
    private TipoContatoRepository tipoContatoRepository;

    @Transactional(rollbackFor = Exception.class)
    public TipoContatoDTO save( TipoContatoEntity tipoContatoEntity ) {
        TipoContatoEntity novoTipoContato = this.tipoContatoRepository.save( tipoContatoEntity );
        return new TipoContatoDTO( novoTipoContato );
    }

    @Transactional(rollbackFor = Exception.class)
    public TipoContatoEntity findByTipoContatoEnum(TipoContatoEnum tipoContatoEnum) {
        return this.tipoContatoRepository.findByTipoContatoEnum(tipoContatoEnum);
    }

    @Transactional(rollbackFor = Exception.class)
    public List<TipoContatoDTO> findAllByTipoContatoEnum(TipoContatoDTO tipoContatoDTO) {
        return this.tipoContatoRepository.findAllByTipoContatoEnum(tipoContatoDTO.getTipoContatoEnum());
    }
}
