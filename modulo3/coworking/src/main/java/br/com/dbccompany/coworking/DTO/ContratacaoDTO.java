package br.com.dbccompany.coworking.DTO;

import br.com.dbccompany.coworking.Entity.ContratacaoEntity;
import br.com.dbccompany.coworking.Entity.TipoContratoEnum;


public class ContratacaoDTO {
    private TipoContratoEnum tipoContratoEnum;
    private int quantidade;
    private double desconto;
    private int prazo;
    private Integer codigoEspaco;
    private Integer codigoCliente;

    public ContratacaoDTO( ContratacaoEntity contratacaoEntity ) {
        this.tipoContratoEnum = contratacaoEntity.getTipoContratoEnum();
        this.quantidade = contratacaoEntity.getQuantidade();
        this.desconto = contratacaoEntity.getDesconto();
        this.prazo = contratacaoEntity.getPrazo();
    }

    public ContratacaoDTO( ) {
    }

    public ContratacaoEntity converter() {
        ContratacaoEntity contrato = new ContratacaoEntity();
        contrato.setTipoContratoEnum( this.tipoContratoEnum );
        contrato.setQuantidade( this.quantidade );
        contrato.setDesconto( this.desconto );
        contrato.setPrazo( this.prazo );
        return contrato;
    }

    public TipoContratoEnum getTipoContratoEnum() {
        return tipoContratoEnum;
    }

    public void setTipoContratoEnum(TipoContratoEnum tipoContratoEnum) {
        this.tipoContratoEnum = tipoContratoEnum;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public double getDesconto() {
        return desconto;
    }

    public void setDesconto(double desconto) {
        this.desconto = desconto;
    }

    public int getPrazo() {
        return prazo;
    }

    public void setPrazo(int prazo) {
        this.prazo = prazo;
    }

    public Integer getCodigoEspaco() {
        return codigoEspaco;
    }

    public void setCodigoEspaco(Integer codigoEspaco) {
        this.codigoEspaco = codigoEspaco;
    }

    public Integer getCodigoCliente() {
        return codigoCliente;
    }

    public void setCodigoCliente(Integer codigoCliente) {
        this.codigoCliente = codigoCliente;
    }
}
