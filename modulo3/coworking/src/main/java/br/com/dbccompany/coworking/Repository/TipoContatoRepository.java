package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.DTO.TipoContatoDTO;
import br.com.dbccompany.coworking.Entity.TipoContatoEntity;
import br.com.dbccompany.coworking.Entity.TipoContatoEnum;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TipoContatoRepository extends CrudRepository<TipoContatoEntity, Integer> {

    TipoContatoEntity findByTipoContatoEnum(TipoContatoEnum tipoContatoEnum);
    List<TipoContatoDTO> findAllByTipoContatoEnum(TipoContatoEnum tipoContatoEnum);
}
