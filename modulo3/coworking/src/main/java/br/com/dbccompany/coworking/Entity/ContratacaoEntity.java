package br.com.dbccompany.coworking.Entity;

import javax.persistence.*;

@Entity
public class ContratacaoEntity {
    @Id
    @SequenceGenerator( name = "CONTRATACAO_SEQ", sequenceName = "CONTRATACAO_SEQ" )
    @GeneratedValue( generator = "CONTRATACAO_SEQ", strategy = GenerationType.SEQUENCE)
    protected Integer Id;

    @ManyToOne(cascade=CascadeType.PERSIST)
    @JoinColumn(name = "id_espaco")
    private EspacoEntity espaco;

    @ManyToOne(cascade=CascadeType.PERSIST)
    @JoinColumn(name = "id_cliente")
    private ClienteEntity cliente;

    @Column( name = "tipoContratoEnum", nullable = false )
    private TipoContratoEnum tipoContratoEnum;

    @Column( name = "quantidade", nullable = false )
    private int quantidade;

    @Column( name = "desconto", nullable = false )
    private double desconto;

    @Column( name = "prazo", nullable = false )
    private int prazo;

    public ContratacaoEntity(TipoContratoEnum tipoContratoEnum, int quantidade, double desconto, int prazo) {
        this.tipoContratoEnum = tipoContratoEnum;
        this.quantidade = quantidade;
        this.desconto = desconto;
        this.prazo = prazo;
    }

    public ContratacaoEntity() {

    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public TipoContratoEnum getTipoContratoEnum() {
        return tipoContratoEnum;
    }

    public void setTipoContratoEnum(TipoContratoEnum tipoContratoEnum) {
        this.tipoContratoEnum = tipoContratoEnum;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public double getDesconto() {
        return desconto;
    }

    public void setDesconto(double desconto) {
        this.desconto = desconto;
    }

    public int getPrazo() {
        return prazo;
    }

    public void setPrazo(int prazo) {
        this.prazo = prazo;
    }

    public EspacoEntity getEspaco() {
        return espaco;
    }

    public void setEspaco(EspacoEntity espaco) {
        this.espaco = espaco;
    }

    public ClienteEntity getCliente() {
        return cliente;
    }

    public void setCliente(ClienteEntity cliente) {
        this.cliente = cliente;
    }
}
