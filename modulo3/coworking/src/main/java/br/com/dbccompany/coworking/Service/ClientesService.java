package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.ClientesDTO;
import br.com.dbccompany.coworking.Entity.ClienteEntity;
import br.com.dbccompany.coworking.Entity.ContatoEntity;
import br.com.dbccompany.coworking.Entity.TipoContatoEntity;
import br.com.dbccompany.coworking.Entity.TipoContatoEnum;
import br.com.dbccompany.coworking.Exception.Exceptions.EmailInvalidoException;
import br.com.dbccompany.coworking.Exception.Exceptions.TelefoneInvalidoException;
import br.com.dbccompany.coworking.Repository.ClientesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Service
public class ClientesService {

    @Autowired
    private ClientesRepository clientesRepository;

    @Autowired
    private ContatoService contatoService;

    @Autowired
    private TipoContatoService tipoContatoService;

    @Transactional(rollbackFor = Exception.class)
    public ClientesDTO save( ClientesDTO clientesDTO ) {
        ClienteEntity clienteEntity = clientesDTO.converter();
        if(clientesDTO.getEmail() == null || clientesDTO.getEmail().isEmpty() ) {
            throw new EmailInvalidoException();
        }
        if(clientesDTO.getTelefone() == null || clientesDTO.getTelefone().isEmpty() ) {
            throw new TelefoneInvalidoException();
        }

        TipoContatoEntity email = tipoContatoService.findByTipoContatoEnum(TipoContatoEnum.EMAIL);
        ContatoEntity contato1 = new ContatoEntity();
        contato1.setTipoContato(email);
        contato1.setValor( clientesDTO.getEmail() );

        TipoContatoEntity telefone = tipoContatoService.findByTipoContatoEnum(TipoContatoEnum.TELEFONE);
        ContatoEntity contato2 = new ContatoEntity();
        contato2.setTipoContato(telefone);
        contato2.setValor( clientesDTO.getTelefone() );

        List<ContatoEntity> listaContatos = Arrays.asList(contato1, contato2);
        clienteEntity.setContatoEntity(listaContatos);

        ClienteEntity novoCliente = this.clientesRepository.save(clienteEntity);
        return new ClientesDTO( novoCliente );
    }


    @Transactional(rollbackFor = Exception.class)
    public List<ClientesDTO> findAll() {
        List<ClienteEntity> clientes = (List<ClienteEntity>) this.clientesRepository.findAll();
        return this.converteLista(clientes);
    }

    private List<ClientesDTO> converteLista( List<ClienteEntity> clientes ) {
        ArrayList<ClientesDTO> lisatClientes = new ArrayList<>();
        for(ClienteEntity cliente : clientes ) {
            lisatClientes.add( new ClientesDTO( cliente ) );
        }
        return lisatClientes;
    }

    public ClienteEntity findById( Integer codigoCliente ) {
        Optional<ClienteEntity> clienteOpt = this.clientesRepository.findById( codigoCliente );
        if(clienteOpt.isPresent()){
            return clienteOpt.get();
        }
        return null;
    }

    public List<ClientesDTO> findByNome ( ClientesDTO clientesDTO ) {
        return this.clientesRepository.findByNome( clientesDTO.getNome() );
    }

    public ClientesDTO findByCpf ( ClientesDTO clientesDTO ) {
        return this.clientesRepository.findByCpf( clientesDTO.getCpf() );
    }

    public List<ClientesDTO> findAllByDataNascimento ( ClientesDTO clientesDTO ) {
        return this.clientesRepository.findAllByDataNascimento( clientesDTO.getDataNascimento() );
    }
}
