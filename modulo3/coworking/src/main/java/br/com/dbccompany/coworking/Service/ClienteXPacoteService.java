package br.com.dbccompany.coworking.Service;
import br.com.dbccompany.coworking.DTO.ClienteXPacoteDTO;
import br.com.dbccompany.coworking.DTO.PacoteDTO;
import br.com.dbccompany.coworking.Entity.ClienteEntity;
import br.com.dbccompany.coworking.Entity.ClienteXPacoteEntity;
import br.com.dbccompany.coworking.Entity.EspacoXPacoteEntity;
import br.com.dbccompany.coworking.Entity.PacoteEntity;
import br.com.dbccompany.coworking.Exception.Exceptions.ClienteNuloException;
import br.com.dbccompany.coworking.Exception.Exceptions.EspacoNuloException;
import br.com.dbccompany.coworking.Repository.ClienteXPacoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class ClienteXPacoteService {

    @Autowired
    private ClienteXPacoteRepository clienteXPacoteRepository;

    @Autowired
    private ClientesService clientesService;

    @Autowired
    private PacotesService pacotesService;

    @Transactional(rollbackFor = Exception.class)
    public ClienteXPacoteEntity save(ClienteXPacoteEntity clienteXPacoteEntity ) {
        return this.clienteXPacoteRepository.save( clienteXPacoteEntity );
    }

    public double retornaValorAPagar( ClienteXPacoteDTO clienteXPacoteDTO ) {
        ClienteEntity cliente = this.clientesService.findById( clienteXPacoteDTO.getCodigoCliente() );
        PacoteEntity pacote = this.pacotesService.findById( clienteXPacoteDTO.getCodigoPacote() );

        if( cliente == null ) {
            throw new ClienteNuloException();
        }
        if( pacote == null ) {
            throw new EspacoNuloException();
        }

        ClienteXPacoteEntity clienteXPacote = clienteXPacoteDTO.converter();
        clienteXPacote.setClientes( cliente );
        clienteXPacote.setPacote( pacote );
        this.save(clienteXPacote);

        return clienteXPacote.getPacote().getValor() * clienteXPacote.getQuantidade();
    }

    public ClienteXPacoteEntity findById(Integer codigoCLienteXPacote ){
        Optional<ClienteXPacoteEntity> clienteXPacoteOPT = this.clienteXPacoteRepository.findById( codigoCLienteXPacote );
        if(clienteXPacoteOPT.isPresent()) {
            return clienteXPacoteOPT.get();
        }
        return null;
    }

    /*
    public List<EspacoXPacoteEntity> findByPacote(PacoteEntity pacote) {
        return this.clienteXPacoteRepository.findByPacote( pacote );
    }

     */

}
