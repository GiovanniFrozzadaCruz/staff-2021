package br.com.dbccompany.coworking.Service;

import br.com.dbccompany.coworking.DTO.PacoteDTO;
import br.com.dbccompany.coworking.Entity.ClienteEntity;
import br.com.dbccompany.coworking.Entity.PacoteEntity;
import br.com.dbccompany.coworking.Repository.PacotesRepository;
import br.com.dbccompany.coworking.Utils.ConversorReais;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class PacotesService {

    @Autowired
    private PacotesRepository pacotesRepository;

    public PacoteDTO save( PacoteEntity pacoteEntity ) {
        PacoteEntity novoPacote = this.pacotesRepository.save( pacoteEntity );
        return new PacoteDTO( novoPacote );
    }

    @Transactional(rollbackFor = Exception.class)
    public List<PacoteDTO> findAll() {
        List<PacoteEntity> pacotes = (List<PacoteEntity>) this.pacotesRepository.findAll();
        return this.converteLista(pacotes);
    }

    private List<PacoteDTO> converteLista( List<PacoteEntity> pacotes ) {
        ArrayList<PacoteDTO> listaPacotes = new ArrayList<>();
        for(PacoteEntity pacote : pacotes ) {
            listaPacotes.add( new PacoteDTO( pacote ) );
        }
        return listaPacotes;
    }
    
    public PacoteEntity findById( Integer codigoPacote ) {
        Optional<PacoteEntity> codigoOpt = this.pacotesRepository.findById( codigoPacote );
        if(codigoOpt.isPresent()){
            return codigoOpt.get();
        }
        return null;
    }

    public List<PacoteDTO> findByValor( double valor ) {
        return this.pacotesRepository.findByValor( valor );
    }
/*
    public List<PacoteDTO> findByPacote( PacoteDTO pacoteDTO ) {
        return this.pacotesRepository.findByPacote( pacoteDTO );
    }


 */

}
