package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.Entity.SaldoXClientesEntity;
import org.springframework.data.repository.CrudRepository;

public interface SaldoXClientesRepository extends CrudRepository<SaldoXClientesEntity, Integer> {

}
