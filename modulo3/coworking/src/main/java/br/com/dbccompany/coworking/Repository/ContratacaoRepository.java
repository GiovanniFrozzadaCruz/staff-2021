package br.com.dbccompany.coworking.Repository;

import br.com.dbccompany.coworking.DTO.ContratacaoDTO;
import br.com.dbccompany.coworking.Entity.ContratacaoEntity;
import br.com.dbccompany.coworking.Entity.TipoContratoEnum;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface ContratacaoRepository extends CrudRepository<ContratacaoEntity, Integer> {

    Optional<ContratacaoEntity> findById(Integer codigoContratacao );
    List<ContratacaoDTO> findByTipoContratoEnum(TipoContratoEnum tipoContratoEnum);
}
