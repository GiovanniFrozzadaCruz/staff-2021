package br.com.dbccompany.coworking.Controller;

import br.com.dbccompany.coworking.DTO.ClientesDTO;
import br.com.dbccompany.coworking.Entity.ClienteEntity;
import br.com.dbccompany.coworking.Service.ClientesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping( path = "/Clientes" )
public class ClientesController {

    private ClientesService clientesService;

    @Autowired
    public ClientesController( ClientesService clientesService ) {
        this.clientesService = clientesService;
    }

    @PostMapping
    @ResponseBody
    public ClientesDTO save (@RequestBody ClientesDTO clientesDTO ) {
        return this.clientesService.save( clientesDTO );
    }

    @ResponseBody
    @GetMapping
    public List<ClientesDTO> findAll(){
        return this.clientesService.findAll();
    }

    @ResponseBody
    @GetMapping(value = "/findById/{id}")
    public ClienteEntity findById(@PathVariable Integer id ) {
        return this.clientesService.findById( id );
    }

    @ResponseBody
    @PostMapping(value = "/findByNome")
    public List<ClientesDTO> findByNome( @RequestBody ClientesDTO clientesDTO ) {
        return this.clientesService.findByNome( clientesDTO );
    }

    @ResponseBody
    @PostMapping(value = "/findByCpf")
    public ClientesDTO findByCpf( @RequestBody ClientesDTO clientesDTO ) {
        return this.clientesService.findByCpf( clientesDTO );
    }

    @ResponseBody
    @PostMapping(value = "/findByDataNascimento")
    public List<ClientesDTO> findAllByDataNascimento( @RequestBody ClientesDTO clientesDTO ) {
        return this.clientesService.findAllByDataNascimento( clientesDTO );
    }


}
