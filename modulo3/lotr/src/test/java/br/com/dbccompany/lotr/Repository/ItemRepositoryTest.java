package br.com.dbccompany.lotr.Repository;

import br.com.dbccompany.lotr.Entity.ItemEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Profile;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@Profile("test")
public class ItemRepositoryTest {

    @Autowired
    private ItemRepository repository;

    @Test
    public void salvarItem() {
        ItemEntity item = new ItemEntity();
        item.setDescricao("Arco");
        repository.save(item);
        assertEquals(item.getDescricao(), repository.findByDescricao(item.getDescricao()));
    }

    @Test
    public void itemNaoExistente() {
        String item = "Flecha";
        assertNull(repository.findByDescricao(item));
    }
}
