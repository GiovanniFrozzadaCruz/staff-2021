package br.com.dbccompany.lotr.Repository;

import br.com.dbccompany.lotr.Entity.AnaoEntity;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Repository;

@Repository
@EnableJpaRepositories
public interface AnaoRepository extends PersonagemRepository<AnaoEntity> {
    AnaoEntity save( AnaoEntity anaoEntity );
}


