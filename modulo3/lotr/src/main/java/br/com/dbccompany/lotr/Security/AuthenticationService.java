package br.com.dbccompany.lotr.Security;

import br.com.dbccompany.lotr.Entity.UsuarioEntity;
import br.com.dbccompany.lotr.Repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Repository;


@Repository
public class AuthenticationService implements UserDetailsService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Override
    public UserDetails loadUserByUsername(String username)	throws UsernameNotFoundException, DataAccessException {
        UsuarioEntity usuario = usuarioRepository.findByUsername(username);
        if( usuario == null ) {
            throw new UsernameNotFoundException("Usuario nao encontrado.");
        }
        return usuario;
    }
}
