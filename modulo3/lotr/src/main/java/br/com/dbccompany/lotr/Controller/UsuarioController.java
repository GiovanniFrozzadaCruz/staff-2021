package br.com.dbccompany.lotr.Controller;

import br.com.dbccompany.lotr.DTO.ItemDTO;
import br.com.dbccompany.lotr.DTO.UsuarioDTO;
import br.com.dbccompany.lotr.Service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/usuario")
public class UsuarioController {

    @Autowired
    private UsuarioService usuarioService;

    public UsuarioController( UsuarioService usuarioService ) {
        this.usuarioService = usuarioService;
    }

    @PostMapping
    @ResponseBody
    public UsuarioDTO save(@RequestBody UsuarioDTO usuarioDTO ) {
        return this.usuarioService.save( usuarioDTO.converter() );
    }

    /*
    @ResponseBody
    @PostMapping(value = "/findByUsername")
    public List<UsuarioDTO> findByUsername(@RequestBody UsuarioDTO usuarioDTO ) {
        return usuarioService.findByUsername(usuarioDTO);
    }

    */

}
