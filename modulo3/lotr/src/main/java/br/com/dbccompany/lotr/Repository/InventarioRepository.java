package br.com.dbccompany.lotr.Repository;

import br.com.dbccompany.lotr.Entity.InventarioEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface InventarioRepository extends CrudRepository<InventarioEntity, Integer> {
    Optional<InventarioEntity> findById( Integer id );
    List<InventarioEntity> findAllByIdIn(List<Integer> listaId);
}
