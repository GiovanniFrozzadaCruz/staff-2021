package br.com.dbccompany.lotr.Controller;

import br.com.dbccompany.lotr.DTO.AnaoDTO;
import br.com.dbccompany.lotr.DTO.ItemDTO;
import br.com.dbccompany.lotr.Entity.ItemEntity;
import br.com.dbccompany.lotr.Exception.ItemNaoEncontrado;
import br.com.dbccompany.lotr.Service.AnaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/anao")
public class AnaoController {

    private AnaoService anaoService;

    @Autowired
    public AnaoController(AnaoService anaoService) {
        this.anaoService = anaoService;
    }

    @PostMapping
    @ResponseBody
    public AnaoDTO save(@RequestBody AnaoDTO anaoDTO ) throws ItemNaoEncontrado {
        try {
            return this.anaoService.save( anaoDTO.converter() );
        } catch ( Exception e ) {
            return null;
        }

    }

    @GetMapping
    @ResponseBody
    public List<AnaoDTO> findAll() throws ItemNaoEncontrado {
        try {
            return this.anaoService.findAll();
        } catch ( Exception e ) {
            return null;
        }

    }

    @ResponseBody
    @PostMapping(value = "/edit/{id}")
    public AnaoDTO edit( @PathVariable Integer id, @RequestBody AnaoDTO anaoDTO ) throws ItemNaoEncontrado {
        try {
            return this.anaoService.edit( id, anaoDTO );
        } catch ( Exception e ) {
            throw new ItemNaoEncontrado();
        }

    }

    @ResponseBody
    @GetMapping(value = "/find/{id}")
    public AnaoDTO findById( @PathVariable Integer id ) throws ItemNaoEncontrado {
        try {
            return this.anaoService.findById( id );
        } catch ( Exception e ) {
            throw new ItemNaoEncontrado();
        }
    }

    @ResponseBody
    @GetMapping(value = "/findNome/{nome}")
    public AnaoDTO findByNome( @PathVariable String nome ) throws ItemNaoEncontrado {
        try {
            return this.anaoService.findByNome( nome );
        } catch ( Exception e ) {
            throw new ItemNaoEncontrado();
        }
    }

    @ResponseBody
    @PostMapping(value = "/findTodosId")
    public List<AnaoDTO> buscarTodosPorIdIn( @RequestBody List<Integer> listaId ) throws ItemNaoEncontrado {
        try {
            return this.anaoService.findAllByIdIn( listaId );
        } catch ( Exception e ) {
            throw new ItemNaoEncontrado();
        }
    }

    @ResponseBody
    @GetMapping(value = "/findTodosPorVida/{vida}")
    public List<AnaoDTO> findAllByVida ( @PathVariable Double vida ) throws ItemNaoEncontrado  {
        try {
            return this.anaoService.findAllByVida( vida );
        } catch ( Exception e ) {
            throw new ItemNaoEncontrado();
        }
    }

    @ResponseBody
    @GetMapping(value = "/findTodosPorExperiencia/{experiencia}")
    public List<AnaoDTO> findAllByExperiencia ( @PathVariable Integer experiencia ) throws ItemNaoEncontrado {
        try {
            return this.anaoService.findAllByExperiencia( experiencia );
        } catch ( Exception e ) {
            throw new ItemNaoEncontrado();
        }
    }

    @ResponseBody
    @GetMapping(value = "/findTodosPorQtdDano/{qtdDano}")
    public List<AnaoDTO> findAllByQtdDano( @PathVariable Double qtdDano ) throws ItemNaoEncontrado {
        try {
            return this.anaoService.findAllByQtdDano ( qtdDano );
        } catch ( Exception e ) {
            throw new ItemNaoEncontrado();
        }
    }

}
