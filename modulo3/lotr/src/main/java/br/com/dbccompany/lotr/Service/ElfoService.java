package br.com.dbccompany.lotr.Service;

import br.com.dbccompany.lotr.DTO.ElfoDTO;
import br.com.dbccompany.lotr.Entity.ElfoEntity;
import br.com.dbccompany.lotr.Repository.ElfoRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ElfoService extends PersonagemService<ElfoRepository, ElfoEntity> {

    public ElfoDTO save( ElfoEntity elfoEntity ) {
        ElfoEntity novoElfo = this.salvar( elfoEntity );
        return new ElfoDTO( novoElfo );
    }

    public List<ElfoDTO> findAll() {
        List<ElfoEntity> elfos = this.trazerTodosOsPersonagens();
        return this.converteLista(elfos);
    }

    private List<ElfoDTO> converteLista(List<ElfoEntity> elfos ) {
        ArrayList<ElfoDTO> listaElfos = new ArrayList<>();
        for(ElfoEntity elfo : elfos ) {
            listaElfos.add( new ElfoDTO( elfo ) );
        }
        return listaElfos;
    }

    public ElfoDTO edit( Integer id, ElfoDTO elfoDTO ) {
        ElfoEntity elfoEntity = this.editar( elfoDTO.converter(), id );
        return new ElfoDTO(elfoEntity);
    }
    
    public ElfoDTO findById( Integer id ) {
        ElfoEntity elfoEntity = this.buscarPorId( id );
        return new ElfoDTO(elfoEntity);
    }

    public ElfoDTO findByNome( String nome ) {
        ElfoEntity elfoEntity = this.buscarPorNome( nome );
        return new ElfoDTO( elfoEntity );
    }

    public List<ElfoDTO> findAllByIdIn( List<Integer> listaId ) {
        List<ElfoEntity> elfos = this.buscarTodosPorIdIn( listaId );
        return this.converteLista(elfos);
    }

    public List<ElfoDTO> findAllByVida (Double vida ) {
        List<ElfoEntity> elfos = this.buscarTodosPorVida( vida );
        return this.converteLista(elfos);
    }

    public List<ElfoDTO> findAllByExperiencia ( Integer experiencia ) {
        List<ElfoEntity> elfos = this.buscarTodosPorExperiencia( experiencia );
        return this.converteLista(elfos);
    }

    public List<ElfoDTO> findAllByQtdDano ( Double qtdDano ) {
        List<ElfoEntity> elfos = this.buscarTodosPorQtdDano( qtdDano );
        return this.converteLista(elfos);
    }
}
