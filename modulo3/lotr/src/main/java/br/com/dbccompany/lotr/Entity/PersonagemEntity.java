package br.com.dbccompany.lotr.Entity;

import javax.persistence.*;

@Entity
@Inheritance( strategy = InheritanceType.SINGLE_TABLE )
@DiscriminatorValue( value = "personagem" )
public class PersonagemEntity {
    @Id
    @SequenceGenerator( name = "PERSONAGEM_SEQ", sequenceName = "PERSONAGEM_SEQ")
    @GeneratedValue( generator = "PERSONAGEM_SEQ", strategy = GenerationType.SEQUENCE)
    protected Integer id;

    @Column( unique = false, nullable = false )
    protected String nome;

    @Column( nullable = false, precision = 4, scale = 2 )
    protected Double vida = 110.0;

    @Enumerated( EnumType.STRING )
    protected StatusEnum status;

    @Column( nullable = false, precision = 4, scale = 2, columnDefinition = "double default 0.0" )
    protected Double qtdDano = 0.0;

    @Column( nullable = false )
    protected Integer experiencia = 0;

    @Column( nullable = false )
    protected int qtdExperienciaPorAtaque = 1;

    @OneToOne( cascade = CascadeType.ALL )
    @JoinColumn( name = "id_inventario" )
    private InventarioEntity inventario;

    public PersonagemEntity() {
    }

    public PersonagemEntity( String nome ) {
        this.nome = nome;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Double getVida() {
        return vida;
    }

    public void setVida(Double vida) {
        this.vida = vida;
    }

    public StatusEnum getStatus() {
        return status;
    }

    public void setStatus(StatusEnum status) {
        this.status = status;
    }

    public Double getQtdDano() {
        return qtdDano;
    }

    public void setQtdDano(Double qtdDano) {
        this.qtdDano = qtdDano;
    }

    public Integer getExperiencia() {
        return experiencia;
    }

    public void setExperiencia(Integer experiencia) {
        this.experiencia = experiencia;
    }

    public int getQtdExperienciaPorAtaque() {
        return qtdExperienciaPorAtaque;
    }

    public void setQtdExperienciaPorAtaque(int qtdExperienciaPorAtaque) {
        this.qtdExperienciaPorAtaque = qtdExperienciaPorAtaque;
    }
}
