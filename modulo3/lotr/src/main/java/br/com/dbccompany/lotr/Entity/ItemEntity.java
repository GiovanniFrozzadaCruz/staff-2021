package br.com.dbccompany.lotr.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class ItemEntity {
    @Id
    @SequenceGenerator( name = "ITEM_SEQ", sequenceName = "ITEM_SEQ" )
    @GeneratedValue( generator = "ITEM_SEQ", strategy = GenerationType.SEQUENCE)
    protected int id;

    @Column( nullable = false )
    protected String descricao;

    @OneToMany( mappedBy = "item")
    private List<Inventario_X_ItemEntity> inventarioItem;

    public ItemEntity( String descricao ) {
        this.descricao = descricao;
    }

    public ItemEntity() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public List<Inventario_X_ItemEntity> getInventarioItem() {
        return inventarioItem;
    }

}
