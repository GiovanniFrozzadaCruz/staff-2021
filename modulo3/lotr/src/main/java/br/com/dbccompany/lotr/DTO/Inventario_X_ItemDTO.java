package br.com.dbccompany.lotr.DTO;

import br.com.dbccompany.lotr.Entity.Inventario_X_ItemEntity;

public class Inventario_X_ItemDTO {
    private int quantidade;

    public Inventario_X_ItemDTO( Inventario_X_ItemEntity inventario ) {
        this.quantidade = inventario.getQuantidade();
    }

    public Inventario_X_ItemDTO( ) {

    }

    public Inventario_X_ItemEntity converter () {
        Inventario_X_ItemEntity inventario = new Inventario_X_ItemEntity();
        inventario.setQuantidade(this.quantidade);
        return inventario;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }
}
