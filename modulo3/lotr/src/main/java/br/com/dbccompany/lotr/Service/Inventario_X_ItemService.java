package br.com.dbccompany.lotr.Service;

import br.com.dbccompany.lotr.Entity.Inventario_X_ItemEntity;
import br.com.dbccompany.lotr.Repository.Inventario_X_ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class Inventario_X_ItemService {

    @Autowired
    private Inventario_X_ItemRepository inventario_x_itemRepository;

    @Transactional( rollbackFor = Exception.class )
    public Inventario_X_ItemEntity save( Inventario_X_ItemEntity inventario_x_itemRepository ) {
        return this.inventario_x_itemRepository.save(inventario_x_itemRepository);
    }
}
