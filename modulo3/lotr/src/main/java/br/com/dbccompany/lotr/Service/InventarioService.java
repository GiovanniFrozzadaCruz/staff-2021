package br.com.dbccompany.lotr.Service;

import br.com.dbccompany.lotr.DTO.InventarioDTO;
import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Repository.InventarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class InventarioService {

    @Autowired
    private InventarioRepository inventarioRepository;

    public InventarioService( InventarioRepository inventarioRepository ) {
        this.inventarioRepository = inventarioRepository;
    }

    public InventarioDTO save( InventarioEntity inventarioEntity ) {
        InventarioEntity novoInventario = this.inventarioRepository.save( inventarioEntity );
        return new InventarioDTO( novoInventario );
    }

    public List<InventarioEntity> findAll() {
        return (List<InventarioEntity>) this.inventarioRepository.findAll();
    }

    public Optional<InventarioEntity> findById(Integer id ) {
        return this.inventarioRepository.findById( id );
    }

    public List<InventarioEntity> findAllByIdIn(List<Integer> listaId) {
        return this.inventarioRepository.findAllByIdIn( listaId );
    }
}
