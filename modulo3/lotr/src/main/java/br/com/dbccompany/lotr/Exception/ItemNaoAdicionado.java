package br.com.dbccompany.lotr.Exception;

public class ItemNaoAdicionado extends Exception {

    public ItemNaoAdicionado() {
        super("Esse item nao foi adicionado corretamente");
    }
}
