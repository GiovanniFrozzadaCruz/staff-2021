package br.com.dbccompany.lotr.Service;

import br.com.dbccompany.lotr.DTO.ItemDTO;
import br.com.dbccompany.lotr.Entity.ItemEntity;
import br.com.dbccompany.lotr.Exception.ItemNaoEncontrado;
import br.com.dbccompany.lotr.Repository.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class ItemService<E, R, DTO> {

    private ItemRepository itemRepository;

    @Autowired
    public ItemService( ItemRepository itemRepository ) {
        this.itemRepository = itemRepository;
    }

    @Transactional(rollbackFor = Exception.class)
    public ItemDTO save( ItemEntity itemEntity ) {
        try {
            ItemEntity itemNovo = this.itemRepository.save(itemEntity);
            return new ItemDTO(itemNovo);
        } catch ( Exception e ) {
            return null;
        }

    }

    @Transactional(rollbackFor = Exception.class)
    public List<ItemDTO> findAll() {
        try {
            List<ItemEntity> itens = (List<ItemEntity>) this.itemRepository.findAll();
            return this.converteLista(itens);
        } catch ( Exception e ) {
            return null;
        }
    }

    private List<ItemDTO> converteLista(List<ItemEntity> itens ) {
        ArrayList<ItemDTO> listaItens = new ArrayList<>();
        for(ItemEntity item : itens ) {
            listaItens.add( new ItemDTO( item ) );
        }
        return listaItens;
    }

    @Transactional( rollbackFor = Exception.class )
    public ItemDTO editar(ItemEntity item, Integer id ) {
        try {
            item.setId(id);
            return this.save(item);
        } catch ( Exception e ) {
            return null;
        }
    }

    public ItemDTO findById(int id ) throws ItemNaoEncontrado {
        try {
            return new ItemDTO(itemRepository.findById(id).get());
        } catch (Exception e) {
            throw new ItemNaoEncontrado();
        }
    }

    public List<ItemDTO> findByDescricao (ItemDTO itemEntity ) throws ItemNaoEncontrado {
        try {
            return itemRepository.findByDescricao(itemEntity.getDescricao());
        } catch ( Exception e ) {
            return null;
        }
    }

    public List<ItemDTO> findAllByIdIn( List<Integer> listaId) throws ItemNaoEncontrado {
        try {
            return this.itemRepository.findAllByIdIn( listaId );
        } catch ( Exception e ) {
            return null;
        }
    }

}
