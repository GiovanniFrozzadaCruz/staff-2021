package br.com.dbccompany.lotr.DTO;

import br.com.dbccompany.lotr.Entity.ItemEntity;
import br.com.dbccompany.lotr.Entity.UsuarioEntity;

public class UsuarioDTO {
    private String username;
    private String password;

    public UsuarioDTO(UsuarioEntity usuario) {
        this.username = usuario.getUsername();
        this.password = usuario.getPassword();
    }

    public UsuarioDTO(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public UsuarioDTO() {
    }

    public UsuarioEntity converter() {
        UsuarioEntity usuario = new UsuarioEntity();
        usuario.setUsername(this.username);
        return usuario;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}

