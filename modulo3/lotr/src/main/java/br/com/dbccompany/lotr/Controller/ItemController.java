package br.com.dbccompany.lotr.Controller;

import br.com.dbccompany.lotr.DTO.ItemDTO;
import br.com.dbccompany.lotr.Entity.ItemEntity;
import br.com.dbccompany.lotr.Exception.ItemNaoAdicionado;
import br.com.dbccompany.lotr.Exception.ItemNaoEncontrado;
import br.com.dbccompany.lotr.Service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping( path = "/item" )
public class ItemController {

    private ItemService itemService;

    @Autowired
    public ItemController( ItemService itemService) {
        this.itemService = itemService;
    }

    @PostMapping
    @ResponseBody
    public ItemDTO save(@RequestBody ItemDTO itemDTO ) throws ItemNaoAdicionado {
        try {
            return this.itemService.save( itemDTO.converter() );
        } catch ( Exception e ) {
            throw new ItemNaoAdicionado();
        }
    }

    @ResponseBody
    @GetMapping
    public ResponseEntity<Object> findAll() {
        try {
            return new ResponseEntity<>(this.itemService.findAll(), HttpStatus.ACCEPTED );
        } catch ( Exception e ) {
            System.err.println(e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @ResponseBody
    @PostMapping(value = "/edit/{id}")
    public ResponseEntity<ItemDTO> edit(@PathVariable int id, @RequestBody ItemEntity itemEntity ) throws ItemNaoEncontrado{
        try {
            return new ResponseEntity<>(this.itemService.editar(itemEntity, id), HttpStatus.ACCEPTED );
        } catch ( Exception e ) {
            throw new ItemNaoEncontrado();
        }
    }

    @ResponseBody
    @PutMapping( value = "/find/{id}")
    public ResponseEntity<ItemDTO> findById( @PathVariable int id ) throws ItemNaoEncontrado {
        try {
            return new ResponseEntity<>(itemService.findById( id ), HttpStatus.ACCEPTED );
        } catch ( Exception e) {
            throw new ItemNaoEncontrado();
        }
    }

    @ResponseBody
    @GetMapping(value = "/find/{id}")
    public ItemDTO findById( @RequestBody Integer id ) throws ItemNaoEncontrado{
        try {
            return itemService.findById(id);
        } catch ( Exception e) {
            throw new ItemNaoEncontrado();
        }
    }

    @ResponseBody
    @PostMapping(value = "/findByDescricao")
    public List<ItemDTO> findByDescricao( @RequestBody ItemDTO itemDTO ) throws ItemNaoEncontrado {
        try {
            return itemService.findByDescricao(itemDTO);
        } catch ( ItemNaoEncontrado e) {
            throw new ItemNaoEncontrado();
        }
    }

    @ResponseBody
    @PostMapping(value = "/findTodosId")
    public List<ItemDTO> buscarTodosPorIdIn( @RequestBody List<Integer> listaId ) {
        try {
            return this.itemService.findAllByIdIn( listaId );
        } catch ( ItemNaoEncontrado e) {
            System.err.println(e.getMensagem());
            return null;
        }
    }

}
