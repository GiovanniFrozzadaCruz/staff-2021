package br.com.dbccompany.lotr.Repository;

import br.com.dbccompany.lotr.DTO.ItemDTO;
import br.com.dbccompany.lotr.Entity.ItemEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ItemRepository extends CrudRepository<ItemEntity, Integer> {
    Optional<ItemEntity> findById( Integer id );
    List<ItemDTO> findByDescricao(String descricao );
    List<ItemDTO> findAllByIdIn(List<Integer> listaId);
}
