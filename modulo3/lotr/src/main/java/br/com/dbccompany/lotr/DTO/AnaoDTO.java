package br.com.dbccompany.lotr.DTO;

import br.com.dbccompany.lotr.Entity.AnaoEntity;

public class AnaoDTO {
    private String nome;
    private Double vida;

    public AnaoDTO(AnaoEntity anaoEntity) {
        this.nome = anaoEntity.getNome();
        this.vida = anaoEntity.getVida();
    }

    public AnaoEntity converter() {
        AnaoEntity anaoEntity = new AnaoEntity();
        anaoEntity.setNome( this.nome );
        anaoEntity.setVida( this.vida );
        return anaoEntity;
    }

    public AnaoDTO( ) {

    }

    public String getNome() {
        return nome;
    }

    public Double getVida() {
        return vida;
    }

    public void setVida(Double vida) {
        this.vida = vida;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
