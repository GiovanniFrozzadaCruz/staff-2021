package br.com.dbccompany.lotr.Repository;

import br.com.dbccompany.lotr.Entity.PersonagemEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PersonagemRepository<T extends PersonagemEntity> extends CrudRepository<T, Integer> {
    Optional<T> findById( Integer id );
    List<T> findAllByIdIn( List<Integer> ids );
    T findByNome ( String nome );
    List<T> findAllByNomeIn ( List<String> name );
    List<T> findAllByExperiencia (Integer experiencia );
    List<T> findAllByVida ( Double vida );
    List<T> findAllByQtdDano ( Double qtdDano );
}