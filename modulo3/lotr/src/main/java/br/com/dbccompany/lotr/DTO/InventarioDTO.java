package br.com.dbccompany.lotr.DTO;

import br.com.dbccompany.lotr.Entity.InventarioEntity;

public class InventarioDTO {
    private Integer id;

    public InventarioDTO( InventarioEntity inventarioEntity ) {
        this.id = inventarioEntity.getId( );
    }

    public InventarioDTO() {
    }

    public InventarioEntity converter() {
        InventarioEntity inventarioEntity = new InventarioEntity();
        inventarioEntity.setId( this.id );
        return inventarioEntity;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
