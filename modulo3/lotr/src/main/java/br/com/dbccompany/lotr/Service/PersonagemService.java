package br.com.dbccompany.lotr.Service;

import br.com.dbccompany.lotr.Entity.PersonagemEntity;
import br.com.dbccompany.lotr.Repository.PersonagemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public abstract class PersonagemService<R extends PersonagemRepository<E>, E extends PersonagemEntity> {

   @Autowired
    private R repository;

    public List<E> trazerTodosOsPersonagens() {
        return (List<E>) repository.findAll();
    }

    @Transactional( rollbackFor = Exception.class )
    public E salvar( E e ) {
        return this.salvarEEditar( e );
    }

    private E salvarEEditar(E e) {
        return repository.save( e );
    }

    @Transactional( rollbackFor = Exception.class )
    public E editar( E e , Integer id ) {
        e.setId( id );
        return this.salvarEEditar( e );
    }

    public E buscarPorId( Integer id ) {
        Optional<E> e = repository.findById(id);
        if( e.isPresent() ) {
            return repository.findById( id ).get();
        }
        return null;
    }

    public E buscarPorNome( String nome ) {
        E e = repository.findByNome( nome );
        return e;
    }

    public List<E> buscarTodosPorIdIn( List<Integer> listaId ) {
        List<E> e = repository.findAllByIdIn( listaId );
        return e;
    }

    public List<E> buscarTodosPorVida( Double vida ) {
        List<E> e = repository.findAllByVida( vida );
        return e;
    }

    public List<E> buscarTodosPorExperiencia( Integer experiencia ) {
        return repository.findAllByExperiencia( experiencia );
    }

    public List<E> buscarTodosPorQtdDano( Double qtdAtaque ) {
        return repository.findAllByQtdDano( qtdAtaque );
    }
}