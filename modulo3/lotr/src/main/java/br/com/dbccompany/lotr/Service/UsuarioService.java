package br.com.dbccompany.lotr.Service;

import br.com.dbccompany.lotr.DTO.UsuarioDTO;
import br.com.dbccompany.lotr.Entity.UsuarioEntity;
import br.com.dbccompany.lotr.Repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class UsuarioService {

    private UsuarioRepository usuarioRepository;

    @Autowired
    public UsuarioService( UsuarioRepository usuarioRepository ) {
        this.usuarioRepository = usuarioRepository;
    }

    @Transactional(rollbackFor = Exception.class)
    public UsuarioDTO save( UsuarioEntity usuarioEntity ) {
        UsuarioEntity usuarioNovo = this.usuarioRepository.save(usuarioEntity);
        return new UsuarioDTO(usuarioNovo);
    }

    /*
    public List<UsuarioDTO> findByUsername(String usuarioDTO ) {
        return usuarioRepository.findByUsername(usuarioDTO.getUsername());
    }
     */

}


