package br.com.dbccompany.lotr.Controller;

import br.com.dbccompany.lotr.DTO.InventarioDTO;
import br.com.dbccompany.lotr.Entity.InventarioEntity;
import br.com.dbccompany.lotr.Service.InventarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/inventario")
public class InventarioController {

    private InventarioService inventarioService;

    @Autowired
    public InventarioController( InventarioService inventarioService ) {
        this.inventarioService = inventarioService;
    }

    @PostMapping
    @ResponseBody
    public InventarioDTO save(@RequestBody InventarioDTO inventarioDTO ) {
        return this.inventarioService.save( inventarioDTO.converter() );
    }

    @ResponseBody
    @GetMapping
    public List<InventarioEntity> findAll() {
        return (List<InventarioEntity>) this.inventarioService.findAll();
    }

    @ResponseBody
    @GetMapping(value = "/find/{id}")
    public Optional<InventarioEntity> findById(@PathVariable Integer id ) {
        return this.inventarioService.findById( id );
    }

    @ResponseBody
    @PostMapping(value = "/findTodosId")
    public List<InventarioEntity> findAllByIdIn( @RequestBody List<Integer> listaId) {
        return this.inventarioService.findAllByIdIn( listaId );
    }

}
