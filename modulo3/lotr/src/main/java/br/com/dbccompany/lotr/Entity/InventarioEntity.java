package br.com.dbccompany.lotr.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class InventarioEntity {
    @Id
    @SequenceGenerator( name = "INVENTARIO_SEQ", sequenceName = "INVENTARIO_SEQ")
    @GeneratedValue( generator = "INVENTARIO_SEQ", strategy = GenerationType.SEQUENCE)
    protected Integer id;

    @OneToMany( mappedBy = "inventario")
    private List<Inventario_X_ItemEntity> inventarioItem;

    @OneToOne( cascade = CascadeType.ALL )
    @JoinColumn( name = "id_personagem" )
    private PersonagemEntity personagem;

    public InventarioEntity(Integer id, List<Inventario_X_ItemEntity> inventarioItem, PersonagemEntity personagem) {
        this.id = id;
        this.inventarioItem = inventarioItem;
        this.personagem = personagem;
    }

    public InventarioEntity() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Inventario_X_ItemEntity> getInventarioItem() {
        return inventarioItem;
    }

    public void setInventarioItem(List<Inventario_X_ItemEntity> inventarioItem) {
        this.inventarioItem = inventarioItem;
    }

    public PersonagemEntity getPersonagem() {
        return personagem;
    }

    public void setPersonagem(PersonagemEntity personagem) {
        this.personagem = personagem;
    }
}