package br.com.dbccompany.lotr.Controller;

import br.com.dbccompany.lotr.Entity.Inventario_X_ItemEntity;
import br.com.dbccompany.lotr.Service.Inventario_X_ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/Inventario_X_ItemController")
public class Inventario_X_ItemController {

    private Inventario_X_ItemService inventario_X_ItemService;

    @Autowired
    public Inventario_X_ItemController( Inventario_X_ItemService inventario_X_ItemService ) {
        this.inventario_X_ItemService = inventario_X_ItemService;
    }

    @PostMapping
    @ResponseBody
    public Inventario_X_ItemEntity save( @RequestBody Inventario_X_ItemEntity inventario_x_itemRepository ) {
        return this.inventario_X_ItemService.save(inventario_x_itemRepository);
    }

}
