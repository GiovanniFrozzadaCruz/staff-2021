package br.com.dbccompany.lotr.Repository;

import br.com.dbccompany.lotr.Entity.Inventario_X_ItemEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface Inventario_X_ItemRepository extends CrudRepository<Inventario_X_ItemEntity, Integer> {
    Inventario_X_ItemEntity save(Inventario_X_ItemEntity inventario_x_itemEntity);
}
