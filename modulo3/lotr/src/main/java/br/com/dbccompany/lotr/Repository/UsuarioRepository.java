package br.com.dbccompany.lotr.Repository;

import br.com.dbccompany.lotr.DTO.UsuarioDTO;
import br.com.dbccompany.lotr.Entity.UsuarioEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UsuarioRepository extends CrudRepository<UsuarioEntity, String> {
    UsuarioEntity findByUsername(String username);
}
