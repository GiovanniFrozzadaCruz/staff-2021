package br.com.dbccompany.lotr.Controller;

import java.util.List;

import br.com.dbccompany.lotr.DTO.ElfoDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.dbccompany.lotr.Service.ElfoService;

@RestController
@RequestMapping("/elfo")
public class ElfoController {
    
    private ElfoService elfoService;

    @Autowired
    public ElfoController( ElfoService elfoService ) {
        this.elfoService = elfoService;
    }

    @PostMapping
    @ResponseBody
    public ElfoDTO save(@RequestBody ElfoDTO elfoDTO ) {
        return this.elfoService.save( elfoDTO.converter() );
    }

    @GetMapping
    @ResponseBody
    public List<ElfoDTO> findAll() {
        return this.elfoService.findAll();
    }

    @ResponseBody
    @PostMapping(value = "/edit/{id}")
    public ElfoDTO edit( @RequestBody ElfoDTO elfoDTO, @PathVariable Integer id ) {
        return this.elfoService.edit( id, elfoDTO );
    }

    @ResponseBody
    @GetMapping(value = "/find/{id}")
    public ElfoDTO findById( @PathVariable Integer id ) {
        return this.elfoService.findById( id );
    }

    @ResponseBody
    @GetMapping(value = "/findNome/{nome}")
    public ElfoDTO findByNome( @PathVariable String nome ) {
        return this.elfoService.findByNome( nome );
    }

    @ResponseBody
    @PostMapping(value = "/findTodosId")
    public List<ElfoDTO> buscarTodosPorIdIn( @RequestBody List<Integer> listaId ) {
        return this.elfoService.findAllByIdIn( listaId );
    }

    @ResponseBody
    @GetMapping(value = "/findTodosPorVida/{vida}")
    public List<ElfoDTO> findAllByVida (@PathVariable Double vida ) {
        return this.elfoService.findAllByVida( vida );
    }

    @ResponseBody
    @GetMapping(value = "/findTodosPorExperiencia/{experiencia}")
    public List<ElfoDTO> findAllByExperiencia (@PathVariable Integer experiencia ) {
        return this.elfoService.findAllByExperiencia( experiencia );
    }

    @ResponseBody
    @GetMapping(value = "/findTodosPorQtdDano/{qtdDano}")
    public List<ElfoDTO> findAllByQtdDano( @PathVariable Double qtdDano ) {
        return this.elfoService.findAllByQtdDano ( qtdDano );
    }

}