package br.com.dbccompany.lotr.Service;

import br.com.dbccompany.lotr.DTO.AnaoDTO;
import br.com.dbccompany.lotr.Entity.AnaoEntity;
import br.com.dbccompany.lotr.Repository.AnaoRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AnaoService extends PersonagemService<AnaoRepository, AnaoEntity> {

    public AnaoDTO save( AnaoEntity anaoEntity ) {
        try {
            AnaoEntity novoAnao = this.salvar( anaoEntity);
            return new AnaoDTO( novoAnao );
        } catch ( Exception e ) {
            return null;
        }

    }

    public List<AnaoDTO> findAll() {
        try {
            List<AnaoEntity> anoes = this.trazerTodosOsPersonagens();
            return this.converteLista(anoes);
        } catch ( Exception e ) {
            return null;
        }
    }

    private List<AnaoDTO> converteLista( List<AnaoEntity> anoes ) {
        ArrayList<AnaoDTO> listaAnoes = new ArrayList<>();
        for(AnaoEntity anao : anoes ) {
            listaAnoes.add( new AnaoDTO( anao ) );
        }
        return listaAnoes;
    }

    public AnaoDTO edit( Integer id, AnaoDTO anaoDTO ) {
        try {
            AnaoEntity anaoEntity = this.editar( anaoDTO.converter(), id );
            return new AnaoDTO(anaoEntity);
        } catch ( Exception e ) {
            return null;
        }
    }

    public AnaoDTO findById( Integer id ) {
        try {
            AnaoEntity anaoEntity = this.buscarPorId( id );
            return new AnaoDTO(anaoEntity);
        } catch ( Exception e ) {
            return null;
        }
    }

    public AnaoDTO findByNome( String nome ) {
        try {
            AnaoEntity anaoEntity = this.buscarPorNome( nome );
            return new AnaoDTO( anaoEntity );
        } catch ( Exception e ) {
            return null;
        }
    }


    public List<AnaoDTO> findAllByIdIn( List<Integer> listaId) {
        try {
            List<AnaoEntity> anoes = this.buscarTodosPorIdIn( listaId );
            return this.converteLista(anoes);
        } catch ( Exception e ) {
            return null;
        }
    }

    public List<AnaoDTO> findAllByVida ( Double vida ) {
        try {
            List<AnaoEntity> anoes = this.buscarTodosPorVida( vida );
            return this.converteLista(anoes);
        } catch ( Exception e ) {
            return null;
        }
    }

    public List<AnaoDTO> findAllByExperiencia ( Integer experiencia ) {
        try {
            List<AnaoEntity> anoes = this.buscarTodosPorExperiencia( experiencia );
            return this.converteLista(anoes);
        } catch ( Exception e ) {
            return null;
        }
    }

    public List<AnaoDTO> findAllByQtdDano ( Double qtdDano ) {
        try {
            List<AnaoEntity> anoes = this.buscarTodosPorQtdDano( qtdDano );
            return this.converteLista(anoes);
        } catch ( Exception e ) {
            return null;
        }
    }

}
