package br.com.dbccompany.lotr.DTO;

import br.com.dbccompany.lotr.Entity.ElfoEntity;

public class ElfoDTO {
    private String nome;
    private Double vida;

    public ElfoDTO(ElfoEntity elfoEntity) {
        this.nome = elfoEntity.getNome();
        this.vida = elfoEntity.getVida();
    }

    public ElfoDTO( ) {

    }

    public ElfoEntity converter() {
        ElfoEntity elfoEntity = new ElfoEntity();
        elfoEntity.setNome( this.nome );
        elfoEntity.setVida( this.vida );
        return elfoEntity;
    }

    public String getNome() {
        return nome;
    }

    public Double getVida() {
        return vida;
    }

    public void setVida(Double vida) {
        this.vida = vida;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
