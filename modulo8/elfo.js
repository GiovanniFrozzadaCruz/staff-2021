class Elfo {
    constructor( nome ) {
        this._nome = nome;
    }

    get nome() {
        return this._nome;
    }

    matarElfo() {
        this.status = "morto";
    }

    get estaMorto() {
        return this.status = "morto";
    }

    atacarComFlecha() {
        //depois de 2000milissegundos ele ataca
        setTimeout( () => {
            console.log("Atacou");
        }, 2000);
    }

}

let legolas = new Elfo("Legolas");
legolas.matarElfo();
legolas.estaMorto;
legolas.atacarComFlecha();


