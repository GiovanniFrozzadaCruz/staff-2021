import { Form, Input, Button } from 'antd';

const FormEspacoPacote = props => {

  const { salvar, acaoSecundaria } = props;

  return (
    <div>
      <h1 type='flex' align='middle' justify='center'>Adicionar contratações de espaços</h1>
      <Form
        name="basic"
        layout='horizontal'
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 11 }}
        onFinish={salvar}
      >
        <Form.Item
          label='Id do Espaco'
          name="espaco"
        >
          <Input />
        </Form.Item>
        
        <Form.Item
          label='Contratação'
          name="tipoContratacao"
        >
          <Input placeholder='MINUTO, HORA, TURNO, DIARIA, SEMANA, MES' />
        </Form.Item>
        <Form.Item
          label='Quantidade'
          name="quantidade"
        >
          <Input />
        </Form.Item>
        <Form.Item
          label='Prazo (em dias)'
          name="prazo"
        >
          <Input />
        </Form.Item>
        <Form.Item
          wrapperCol={{
            offset: 8,
            span: 6,
          }}
        >
          <Button type="primary" htmlType="submit">
            Adicionar contratação ao Pacote
          </Button>
        </Form.Item>
      </Form>
      <Button type='secondary' onClick={ acaoSecundaria } >Limpar contratações</Button>
    </div>
  )
}

export default FormEspacoPacote;