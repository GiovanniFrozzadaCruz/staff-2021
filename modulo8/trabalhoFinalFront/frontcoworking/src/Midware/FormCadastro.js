import { Form, Input, Button, Checkbox, Row, Col } from 'antd';

import { Link } from 'react-router-dom';

import React from 'react';

const FormCadastro = props => {
  const { onFinish } = props;
  return (
    <Form
      name="basic"
      labelCol={{ span: 4 }}
      wrapperCol={{ span: 16 }}
      initialValues={{ remember: true }}
      onFinish={onFinish}
    >
      <h1 type='flex' align='middle' justify='center'>Cadastramento de usuário</h1>

      <Row type='flex' align='middle' justify='center' >
        <Col span={12}>
          <Form.Item
            label="Nome"
            name="nome"
            rules={[{ required: true, message: 'Digite o seu nome!' }]}
          >
            <Input />
          </Form.Item>
        </Col>
      </Row>
      <Row type='flex' align='middle' justify='center' >
        <Col span={12}>
          <Form.Item
            label="Email"
            name="email"
            rules={[{ required: true, message: 'Digite o seu nome!' }]}
          >
            <Input />
          </Form.Item>
        </Col>
      </Row>
      <Row type='flex' align='middle' justify='center' >
        <Col span={12}>
          <Form.Item
            label="Login"
            name="login"
            rules={[{ required: true, message: 'Digite o seu nome!' }]}
          >
            <Input />
          </Form.Item>
        </Col>
      </Row>
      <Row type='flex' align='middle' justify='center' >
        <Col span={12}>
          <Form.Item
            label="Senha"
            name="senha"
            rules={[{ required: true, message: 'Digite a sua senha!' }]}
          >
            <Input.Password />
          </Form.Item>
        </Col>
      </Row>
      <Row type='flex' align='middle' justify='center' >
        <Col span={12} >
          <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
            <Button type="primary" htmlType="submit">
              Cadastrar Usuário
            </Button>
            <Link to='/'>
              <Button type="primary" danger >
                Voltar para Login
              </Button>
            </Link>

          </Form.Item>
        </Col>
      </Row>
    </Form>
  )
}

export default FormCadastro;
