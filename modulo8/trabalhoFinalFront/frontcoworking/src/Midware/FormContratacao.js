import { Form, Input, Button } from 'antd';

const FormContratacao = props => {

  const { salvar } = props;

  return (
    <div>
      <h1 type='flex' align='middle' justify='center'>Manipulador de Contratações</h1>
      <p>Para atualizar uma Contratação já existente, preencha novamente todos os campos deste formulário
        e ao fim informe o ID da Contratação.</p>
      <Form
        name="basic"
        layout='horizontal'
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 12 }}
        onFinish={salvar}
      >

        <Form.Item
          label='ID Espaço'
          name="espaco"
        >
          <Input />
        </Form.Item>
        <Form.Item
          label='ID Cliente'
          name="cliente"
        >
          <Input />
        </Form.Item>
        <Form.Item
          label='Contratação'
          name="tipoContratacao"
        >
          <Input placeholder='MINUTO, HORA, DIA, TURNO, DIARIA, SEMANA, MES' />
        </Form.Item>
        <Form.Item
          label='Quantidade'
          name="quantidade"
        >
          <Input />
        </Form.Item>
        <Form.Item
          label='Desconto'
          name="desconto"
        >
          <Input placeholder='Em decimal' />
        </Form.Item>
        <Form.Item
          label='Prazo'
          name="prazo"
        >
          <Input />
        </Form.Item>
        <Form.Item
          label='ID da Contratação'
          name="id"
        >
          <Input placeholder='APENAS PARA ATUALIZAR' />
        </Form.Item>
        <Form.Item
          wrapperCol={{
            offset: 8,
            span: 6,
          }}
        >
          <Button type="primary" htmlType="submit">
            Salvar Contratação
          </Button>
        </Form.Item>
      </Form>
    </div>

  )
}

export default FormContratacao;