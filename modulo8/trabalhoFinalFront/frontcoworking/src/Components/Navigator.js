import React from "react";
import BotaoAzul from "../Midware/BotaoAzul";

import MeuBotao from "../Midware/MeuBotao";

export default class Navigator extends React.Component {

  sair() {
    localStorage.setItem('token', '');
  }

  render() {
    return (
      <div className='nav-container' >
        <nav>
          <ul>
            <li>
              <MeuBotao endereco="/home" nome='Home' />
            </li>
            <li>
              <MeuBotao endereco="/tipo-contato" nome='Tipos Contato' />
            </li>
            <li>
              <MeuBotao endereco="/cliente" nome='Clientes' />
            </li>
            <li>
              <MeuBotao endereco="/espaco" nome='Espaços' />
            </li>
            <li>
              <MeuBotao endereco="/pacote" nome='Pacotes' />
            </li>
            <li>
              <MeuBotao endereco="/contratacao" nome='Contratações' />
            </li>
            <li>
              <MeuBotao endereco="/cliente-pacote" nome={'Clientes & Pacotes'} />
            </li>
            <li>
              <MeuBotao endereco="/pagamento" nome='Pagamentos' />
            </li>
            <li>
              <MeuBotao endereco="/saldo-cliente" nome='Saldos Clientes' />
            </li>
            <li>
              <MeuBotao endereco="/acessos" nome='Acessos' />
            </li>
            <li>
              <BotaoAzul endereco="/" nome='Sair' metodo={this.sair.bind(this)} />
            </li>
          </ul>
        </nav>
      </div>
    )
  }
}
