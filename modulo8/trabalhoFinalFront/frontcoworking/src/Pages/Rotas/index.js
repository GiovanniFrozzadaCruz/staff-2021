import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import { RotasPrivadas } from './RotasPrivadas';

import Cadastro from '../Cadastro';
import Login from '../Login';
import Home from '../Home'
import TipoContato from '../TipoContato';
import CadastroCliente from '../Cliente/CadastroCliente'
import Cliente from '../Cliente/Cliente';
import Espaco from '../Espaco'
import Pacote from '../Pacote/Pacote';
import CadastroPacote from '../Pacote/CadastroPacote';
import Contratacao from '../Contratacao';

export default class Rotas extends React.Component{
  render(){
    return(
      <Router>
        <Route path='/cadastro' exact component={ Cadastro } />
        <Route path='/' exact component={ Login }/>
        <RotasPrivadas path='/home' exact component={ Home } />
        <RotasPrivadas path='/tipo-contato' exact component={ TipoContato } />
        <RotasPrivadas path='/cadastro-cliente' exact component={ CadastroCliente } />
        <RotasPrivadas path='/cliente' exact component={ Cliente }/>
        <RotasPrivadas path='/espaco' exact component={ Espaco } />
        <RotasPrivadas path='/pacote' exact component={ Pacote } />
        <RotasPrivadas path='/cadastro-pacote' exact component={ CadastroPacote } />
        <RotasPrivadas path='/contratacao' exact component={ Contratacao } />
      </Router>
    )
  }
}