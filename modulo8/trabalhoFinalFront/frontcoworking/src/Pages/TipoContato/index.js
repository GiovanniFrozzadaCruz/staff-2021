import React from "react";

import 'antd/dist/antd.css';

import ApiBuscar from "../../API/ApiBuscar";
import ApiSalvar from "../../API/ApiSalvar";
import TipoContatoModel from "../../Models/TipoContatoModel";
import Cabecalho from "../../Components/Cabecalho";
import { Row, Col } from "antd";

import '../Pages.css'

import Navigator from "../../Components/Navigator";
import InputSalvarEditar from "../../Midware/InputSalvarEditar";
import Tabela from "../../Midware/Tabela";

export default class TipoContato extends React.Component {
  constructor(props) {
    super(props);
    this.salvarTipoContato = this.salvarTipoContato.bind(this);
    this.apiSalvar = new ApiSalvar();
    this.apiBuscar = new ApiBuscar();
    this.state = {
      todos: '',
      idAEditar: '',
      tipoContatoEditado: ''
    }
  }

  componentDidMount() {
    this.buscarTodos();
  }


  salvarTipoContato(nome) {
    if (nome !== '') {
      const novoTipoContato = new TipoContatoModel(nome.nome);
      this.apiSalvar.salvarTipoContato(novoTipoContato).then(() =>
        this.buscarTodos());
    }
  }

  editarTipoContato(editado) {
    if (editado.id !== null && editado.nome !== null) {
      const tipoContatoEditado = new TipoContatoModel(editado.nome);
      this.apiSalvar.editarTipoContato(tipoContatoEditado, editado.id).then(() =>
        this.buscarTodos());
    }
  }

  buscarTodos() {
    this.apiBuscar.buscarTiposContato().then(tiposContato => {
      this.setState(state => { return { ...state, todos: tiposContato } });
    })
  }

  render() {
    return (
      <div className="general">
        <header>
          <Cabecalho nomePagina='Tipos Contato' />
          <Row>
            <Col span={2} className='nav' >
              <Navigator />
            </Col>
            <Col span={8} className='tirar-do-header text-align inline-block' >
              <InputSalvarEditar
                titulo='Manipulador de tipos de contato'
                salvar={this.salvarTipoContato.bind(this)}
                labelSalvar='Digite o novo valor'
                editar={this.editarTipoContato.bind(this)}
                labelId='Digite o Id do item'
                labelValor='Digite o novo valor'
              />
            </Col>
            <Col span={8} className='tirar-do-header' >
                {!this.state.todos ?
                  <h3>Carregando...</h3> : (
                    <Tabela dataSource={this.state.todos} colunas={[
                      {
                        title: 'Id',
                        dataIndex: 'id',
                        key: 'id'
                      },
                      {
                        title: 'Tipo Contato',
                        dataIndex: 'nome',
                        key: 'nome'
                      }
                    ]} />
                  )
                }
              </Col>
          </Row>

        </header>
      </div>
    )
  }
}
