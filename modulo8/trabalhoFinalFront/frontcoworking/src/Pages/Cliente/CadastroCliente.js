import React from 'react';

import 'antd/dist/antd.css';

import ApiBuscar from "../../API/ApiBuscar";
import ApiSalvar from "../../API/ApiSalvar";
import ClienteModel from '../../Models/ClienteModel';
import Cabecalho from "../../Components/Cabecalho";
import { Row, Col } from "antd";

import '../Pages.css'

import Navigator from "../../Components/Navigator";
import FormCliente from '../../Midware/FormCliente';
import FormContatos from '../../Midware/FormContatos';
import MeuBotao from '../../Midware/MeuBotao';
import BotaoAzul from '../../Midware/BotaoAzul';

export default class CadastroCliente extends React.Component {
  constructor(props) {
    super(props);
    this.apiSalvar = new ApiSalvar();
    this.apiBuscar = new ApiBuscar();
    this.state = {
      dadosCadastrais: '',
      contatos: [],
      clienteSalvo: '',
      mensagem: '',
      mensagemSucesso: ''
    }
  }

  salvarDadosCadastrais(dadosCadastrais) {
    this.setState(state => {
      return {
        ...state,
        dadosCadastrais: dadosCadastrais,
        clienteSalvo: ''
      }
    })
  }

  salvarContatos(novoContato) {
    let contatos = this.state.contatos;
    contatos.push(novoContato);
    this.setState(state => {
      return {
        ...state,
        contatos: contatos,
        clienteSalvo: ''
      }
    })
  }

  registrarCliente() {
    const dados = this.state.dadosCadastrais;
    const contatos = this.state.contatos;
    if (dados.nome !== undefined
      && dados.cpf !== undefined
      && dados.dataNascimento !== undefined
      && contatos.length > 0) {

      const cpfFormatado = this.formatarCpf(dados.cpf);
      const dataFormatada = this.formatarData(dados.dataNascimento);
      const contatosFormatados = this.formatarContatos(contatos);
      const novoCliente = new ClienteModel(dados.nome, cpfFormatado, dataFormatada, contatosFormatados);
      if (dados.id === undefined) {
        this.apiSalvar.salvarCliente(novoCliente).then(res => {
          this.setState(state => {
            return {
              ...state,
              dadosCadastrais: '',
              contatos: '',
              clienteSalvo: res,
              mensagemSucesso: 'Cliente cadastrado com sucesso!'
            }
          })
        }).catch(() => {
          this.dadosInvalidos();
        }
        )

      } else {
        this.editarCliente(novoCliente, dados.id);
      }
    } else {
      this.dadosInvalidos()
    }
  }

  editarCliente(cliente, id) {
    this.apiSalvar.editarCliente(cliente, id).then(cliente => {
      this.setState(state => {
        return {
          ...state,
          dadosCadastrais: '',
          contatos: '',
          clienteSalvo: cliente,
          mensagemSucesso: 'Cliente atualizado com sucesso!'
        }
      })
    }).catch( () => {
      this.dadosInvalidos();
    } )
  }

  formatarCpf(cpf) {
    return cpf.replaceAll('.', '').replaceAll('-', '');
  }

  formatarData(data) {
    return data.split('/').reverse().join('-');
  }

  formatarContatos(contatos) {
    return contatos.map(contato => {
      let tipoContato = new Object();
      tipoContato.id = contato.tipoContato;
      let contatoFormatado = new Object();
      contatoFormatado.contato = contato.contato;
      contatoFormatado.tipoContato = tipoContato;
      return contatoFormatado;
    })
  }

  dadosInvalidos() {
    this.setState(state => {
      return {
        ...state,
        mensagem: 'Dados rejeitados pelo servidor. \n Verifique-os e tente novamente.',
      }
    });
    setTimeout(() => {
      this.setState(state => {
        return {
          ...state,
          mensagem: ''
        }
      })
    }, 5000)
  }

  limparContatos() {
    this.setState(state => {
      return {
        ...state,
        contatos: []
      }
    })
  }

  apagarDados() {
    this.setState(state => {
      return {
        ...state,
        clienteSalvo: ''
      }
    })
  }

  render() {
    return (
      <div className="general">
        <header>
          <Cabecalho nomePagina='Cadastro Cliente' />
          <Row>
            <Col span={2} className='nav' >
              <Navigator />
            </Col>
            <Col span={8} className='tirar-do-header text-align inline-block' >
              <FormCliente salvar={this.salvarDadosCadastrais.bind(this)} acaoSecundaria={this.limparContatos.bind(this)} />
            </Col>
            <Col className='tirar-do-header text-align inline-block' >
              <Row>
                <Col>
                  <FormContatos salvar={this.salvarContatos.bind(this)} />
                </Col>

              </Row>
              <Col>
                {this.state.dadosCadastrais || this.state.contatos > 0 ? (
                  <div className='background-salmon' >
                    <h3>Confira seus dados antes de enviar</h3>
                    <p>{`Nome: ${this.state.dadosCadastrais.nome}`}</p>
                    <p>{`CPF: ${this.state.dadosCadastrais.cpf}`}</p>
                    <p>{`Data de Nascimento: ${this.state.dadosCadastrais.dataNascimento}`}</p>
                    {!this.state.contatos ?
                      (<h3></h3>) :
                      this.state.contatos.map(contato => {
                        return (
                          <p>Contato: {contato.contato} / Id tipo contato: {contato.tipoContato}</p>
                        )
                      })}
                    <MeuBotao nome='Dados estão corretos' metodo={this.registrarCliente.bind(this)} />
                    <h4 className='vermelho' >{this.state.mensagem}</h4>
                  </div>

                ) : ''}
                {this.state.clienteSalvo ? (
                  <div className='background-salmon' >
                    <h3>{ this.state.mensagemSucesso }</h3>
                    <p>{`Id gerado: ${this.state.clienteSalvo.id}`}</p>
                    <p>{`Nome: ${this.state.clienteSalvo.nome}`}</p>
                    <p>{`CPF: ${this.state.clienteSalvo.cpf}`}</p>
                    <p>{`Data de Nascimento: ${this.state.clienteSalvo.dataNascimento}`}</p>
                    <h4>Contatos:</h4>
                    {this.state.clienteSalvo.contatos ? this.state.clienteSalvo.contatos.map(contato => {
                      return (
                        <p>{contato.contato}</p>
                      )
                    })
                    : ''}
                    <BotaoAzul nome='Registrar um novo cliente' metodo={this.apagarDados.bind(this)} />
                  </div>
                ) : ''}
              </Col>
              <Row>
              </Row>
            </Col>
            <Col>
            </Col>
          </Row>
        </header>
      </div>
    )
  }
}
