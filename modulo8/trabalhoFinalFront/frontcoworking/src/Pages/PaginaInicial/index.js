import React from 'react'
import { Route, Link } from 'react-router-dom'

import { Button, Col, Row } from 'antd';

import './home.css';

const index = () => {
  return (
    <React.Fragment>

      <Row justify="space-around" align="middle">
        <Col span={7}>
          <h1>
            Trabalho de Front-End <br />
              Giovanni Frozza
          </h1>
        </Col>
        <Col span={5}>
          <Button type="primary" block>
            <Link to='/login'>Login</Link>
          </Button>
          <Button type="primary" block>
            <Link to='/cadastro'>Cadastro</Link>
          </Button>
        </Col>
      </Row>

      {/* <div className='paginaInicial'>
        <div style={{margin:'20px'}} type='flex' align='middle' justify='center'>
          <h1>
            Trabalho de Front-End <br />
            Giovanni Frozza
          </h1>
        </div>
        <Row className='row' type='flex' align='middle' justify='center'>
          <Col span={12}>
            <Button type="primary" block>
              <Link to='/login'>login</Link>
            </Button>
          </Col>
        </Row>
        <Row className='row' type='flex' align='middle' justify='center'>
          <Col span={12}>
            <Button type="primary" block>
              <Link to='/cadastro'>cadastro</Link>
            </Button>
          </Col>
        </Row>
      </div> */}
    </React.Fragment>
  )
}

export default index
