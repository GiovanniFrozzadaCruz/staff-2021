import React from "react";
import Api from "../../Models/Api";
import LoginModel from "../../Models/LoginModel";
import { Route, Link } from 'react-router-dom'


import { Button, Input, Col, Row } from 'antd';

import './login.css'

export default class Login extends React.Component {
  constructor(props) {
    super(props);
    this.api = new Api();
    this.login = new LoginModel("arturzada", "bbb");
    this.logar();
    this.listarCliente()
  }

  logar() {
    const requisicao = [this.api.autenticarUsuario(this.login)];
    Promise.all(requisicao).then(resposta => {
    })
  }

  listarCliente() {
    const requisicao = [this.api.listarCliente()];
    Promise.all(requisicao).then(resposta => {
      console.log(resposta);
    })
  }

  render() {
    return (
      <React.Fragment>
        <Row type='flex' align='middle' justify='center'>
          <Col span={12}>
            <h1>Login</h1>
            <div className='formulario'>
              <label>Login</label>
              <Input type='text' placeholder='Login'></Input>
            </div>
          </Col>
        </Row>

        <Row style={{ marginTop: '15px' }} type='flex' align='middle' justify='center'>
          <Col span={12}>
            <div className='formulario'>
              <label>Senha</label>
              <Input type='password' placeholder='password'></Input>
            </div>
          </Col>
        </Row>

        <Row style={{ marginTop: '15px' }} type='flex' align='middle' justify='center'>
          <Button type="primary">Cadastrar</Button>
        </Row>
        <Row style={{ marginTop: '15px' }} type='flex' align='middle' justify='center'>
          <Button type="primary" danger>
            <Link to='/'>Cancelar</Link>
          </Button>
        </Row>

      </React.Fragment>
    )
  }
}