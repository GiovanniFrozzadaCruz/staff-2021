import React from 'react';
import Cabecalho from '../../Components/Cabecalho';

import { Row, Col } from 'antd';

import '../Pages.css';
import Navigator from '../../Components/Navigator';

export default class Home extends React.Component {
  render(){
    return(
      <div className="general">
        <header>
        <Cabecalho nomePagina='Home' />
          <Row>
            <Col span={ 3 } className='nav' >
              <Navigator />
            </Col>
          </Row>
        </header>
      </div>
    );
  }
}
