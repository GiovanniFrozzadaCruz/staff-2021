import React, { useState } from 'react'
import Api from '../../Models/Api';
import CadastroModel from '../../Models/CadastroModel';
import { Route, Link } from 'react-router-dom'

import { Button, Input, Form, Row, Col } from 'antd';

import './cadastro.css';

export default class Cadastro extends React.Component {

  constructor(props) {
    super(props)
    this.api = new Api()
    this.state = {
    }
    this.handleNomeChange = this.handleNomeChange.bind(this);
    this.handleEmailChange = this.handleEmailChange.bind(this);
    this.handleLoginChange = this.handleLoginChange.bind(this);
    this.handleSenhaChange = this.handleSenhaChange.bind(this);
    this.cadastrarUsuario = this.cadastrarUsuario.bind(this);
  }

  handleNomeChange(event) {
    this.setState({ nome: event.target.value });
  }

  handleEmailChange(event) {
    this.setState({ email: event.target.value });
  }

  handleLoginChange(event) {
    this.setState({ login: event.target.value });
  }

  handleSenhaChange(event) {
    this.setState({ senha: event.target.value });
  }

  cadastrarUsuario() {
    const { nome, email, login, senha } = this.state;
    let cadastro = new CadastroModel(nome, email, login, senha);
    this.api.cadastrarUsuario(cadastro);
  }

  render() {
    return (
      <React.Fragment>
        <h1 type='flex' align='middle' justify='center'>Cadastro</h1>

        <Form>
          <Row type='flex' align='middle' justify='center'>
            <Col span={12}>
              <Form.Item>
                <label>Nome Completo</label>
                <Input placeholder='Ex. Nome Completo' />
              </Form.Item>
            </Col>
          </Row>

          <Row type='flex' align='middle' justify='center'>
            <Col span={12}>
              <label>E-mail</label>
              <Form.Item
                name="email"
                rules={[
                  {
                    type: 'email',
                    message: 'Este e-mail não é valido!',
                  },
                  {
                    required: true,
                    message: 'Por favor preencha com seu email.',
                  },
                ]}
              >
                <Input placeholder='Ex. seuemail@gmail.com' />
              </Form.Item>
            </Col>
          </Row>
          <Row type='flex' align='middle' justify='center'>
            <Col span={12}>
              <label>Login</label>
              <Form.Item>
                <Input placeholder='Ex. Login de usuario' />
              </Form.Item>
            </Col>
          </Row>
          <Row type='flex' align='middle' justify='center'>
            <Col span={12}>
              <label>Senha</label>
              <Form.Item>
                <Input type='password' placeholder='Ex. Senha de usuario' />
              </Form.Item>
            </Col>
          </Row>

          <Row type='flex' align='middle' justify='center'>
            <Col>
              <Button type="primary">Cadastrar</Button>
            </Col>
            <Col style={{ marginLeft: '50px' }}>
              <Button type="primary" danger>
                <Link to='/'>Cancelar</Link>
              </Button>
            </Col>
          </Row>
        </Form>

      </React.Fragment>
    )
  }
}
