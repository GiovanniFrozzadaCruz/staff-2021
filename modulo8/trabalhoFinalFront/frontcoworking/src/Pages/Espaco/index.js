import React from 'react';
import Cabecalho from '../../Components/Cabecalho';

import { Row, Col } from 'antd';

import '../Pages.css';
import Navigator from '../../Components/Navigator';
import Tabela from '../../Midware/Tabela';
import FormEspaco from '../../Midware/FormEspaco';
import EspacoModel from '../../Models/EspacoModel';
import ApiSalvar from '../../API/ApiSalvar';
import ApiBuscar from '../../API/ApiBuscar';

export default class Home extends React.Component {
  constructor(props) {
    super(props);
    this.apiSalvar = new ApiSalvar();
    this.apiBuscar = new ApiBuscar();
    this.state = {
      todos: '',
      mensagem: ''
    }
  }

  componentDidMount() {
    this.buscarTodos();
  }

  buscarTodos() {
    this.apiBuscar.buscarEspacos().then(espacos => {
      this.setState(state => { return { ...state, todos: espacos } })
    });
  }

  salvarEspaco(espaco) {
    const novoEspaco = new EspacoModel(espaco.nome, espaco.qntPessoas, espaco.valor);
    if (espaco.id === undefined) {
      this.apiSalvar.salvarEspaco(novoEspaco).then(() => {
        this.buscarTodos();
      }).catch(() => {
        this.dadosInvalidos();
      })
    }else{
      this.apiSalvar.editarEspaco( novoEspaco, espaco.id ).then( () => {
        this.buscarTodos();

      } ).catch( () => {
        this.dadosInvalidos();
      } )
    }
  }

  dadosInvalidos() {
    this.setState(state => {
      return {
        ...state,
        mensagem: 'Dados rejeitados pelo servidor. Confira-os e tente novamente.'
      }
    })
    setTimeout(() => {
      this.setState(state => {
        return {
          ...state,
          mensagem: ''
        }
      })
    }, 5000)
  }

  editarEspaco() {
    return null;
  }

  salvar
  render() {
    return (
      <div className="general">
        <header>
          <Cabecalho nomePagina='Espaços' />
          <Row>
            <Col span={3} className='nav' >
              <Navigator />
            </Col>
            <Col span={8} className='tirar-do-header text-align inline-block' >
              <FormEspaco salvar={this.salvarEspaco.bind(this)} />
            </Col>
            <Col span={8} className='tirar-do-header margin-left-20 ' >
              {!this.state.todos ?
                <h3>Carregando...</h3> : (
                  <Tabela dataSource={this.state.todos} colunas={[
                    {
                      title: 'ID',
                      dataIndex: 'id',
                      key: 'id'
                    },
                    {
                      title: 'Nome',
                      dataIndex: 'nome',
                      key: 'nome'
                    },
                    {
                      title: 'Quantidade de pessoas',
                      dataIndex: 'qntPessoas',
                      key: 'qntPessoas'
                    },
                    {
                      title: 'Valor',
                      dataIndex: 'valor',
                      key: 'valor'
                    }
                  ]} />
                )
              }
            </Col>
          </Row>
        </header>
      </div>
    );
  }
}
