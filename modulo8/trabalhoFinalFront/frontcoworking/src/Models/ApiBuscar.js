import axios from "axios";

export default class ApiBuscar{
  constructor(){
    this.url = 'http://localhost:8080/cw';
    this.token = localStorage.getItem( 'token' );
    this.config = { headers: { Authorization: this.token } };
  }

  buscarTiposContato(){
    return axios.get( `${ this.url }/tipoContato/`, this.config ).then( res => res.data );
  }

  buscarClientes(){
    return axios.get( `${ this.url }/cliente/`, this.config ).then( res => res.data );
  }

  buscarEspacos(){
    return axios.get( `${ this.url }/espaco/`, this.config ).then( res => res.data );
  }

  buscarPacotes(){
    return axios.get( `${ this.url }/pacote/`, this.config ).then( res => res.data );
  }

  buscarContratacoes(){
    return axios.get( `${ this.url }/contratacao/`, this.config ).then( res => res.data );
  }

  buscarContratacaoPorId( id ){
    return axios.get( `${ this.url }/contratacao/${ id }`, this.config ).then( res => res.data );
  }
  
}