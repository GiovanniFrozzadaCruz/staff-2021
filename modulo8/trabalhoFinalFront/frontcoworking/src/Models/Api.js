import axios from "axios";

const url = "http://localhost:8080";

const token = localStorage.getItem('userToken');
const config = { headers: { Authorization : token} }

export default class Api {
  constructor(){
    console.log(token)
  } 

  cadastrarUsuario( { nome, email, login, senha } ){
    const response =  axios.post(`${ url }/cadastrar`, { nome, email, login, senha })
    return response;
  }

  autenticarUsuario( { login, senha } ){
    return axios.post( `${url}/login`, { login, senha } ).then( res => localStorage.setItem('userToken', res.headers.authorization));
  }

  listarCliente(){
    return axios.get(`${url}/cw/cliente/`, config )
  } 

}