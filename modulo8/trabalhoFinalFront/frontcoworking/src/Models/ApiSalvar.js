import axios from "axios";

export default class ApiSalvar {
  constructor() {
    this.url = 'http://localhost:8080/cw';
    this.token = localStorage.getItem('token');
    this.config = { headers: { Authorization: this.token } };
  }

  editarTipoContato( tipoContato, id ) {
    return axios.put( `${this.url}/tipoContato/editar/${id}`, tipoContato, this.config ).then( res => res.data );
  }

  editarCliente( cliente, id ) {
    return axios.put( `${this.url}/cliente/editar/${id}`, cliente, this.config ).then( res => res.data );
  }

  editarEspaco( espaco, id ) {
    console.log( id );
    console.log( espaco )
    return axios.put( `${this.url}/espaco/editar/${id}`, espaco, this.config ).then( res => res.data );
  }

  // editarPacote( pacote, id ) {
  //   return axios.put( `${this.url}/pacote/editar/${id}`, pacote, this.config ).then( res => res.data );
  // }

  // editarContratacao( contratacao, id ) {
  //   return axios.put( `${this.url}/contratacao/editar/${id}`, contratacao, this.config ).then( res => res.data );
  // }

  // salvarTipoContato( tipoContato ) {
  //   return axios.post( `${this.url}/tipoContato/salvar`, tipoContato, this.config ).then( res => res.data );
  // };

  // salvarCliente( cliente ) {
  //   return axios.post( `${this.url}/cliente/salvar`, cliente, this.config ).then( res => res.data );
  // }

  // salvarEspaco( espaco ) {
  //   return axios.post( `${this.url}/espaco/salvar`, espaco, this.config ).then( res => res.data );
  // }

  // salvarPacote( pacote ) {
  //   return axios.post( `${this.url}/pacote/salvar`, pacote, this.config ).then( res => res.data );
  // }

  // salvarContratacao( contratacao ) {
  //   return axios.post( `${this.url}/contratacao/salvar`, contratacao, this.config ).then( res => res.data );
  // }

}