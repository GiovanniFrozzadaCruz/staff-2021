import React from 'react';
import ReactDOM from 'react-dom';

import { configProvider } from 'antd'
import ptBR from 'antd/lib/locale/pt_BR'
import 'antd/dist/antd.css';

import Rotas from './Pages/Rotas'

ReactDOM.render(
  <React.StrictMode>
    <configProvider locale={ptBR}>
      <Rotas />
    </configProvider>
  </React.StrictMode>,
  document.getElementById('root')
);
