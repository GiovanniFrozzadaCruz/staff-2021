/** -------------------Exercicio 1------------------- */

let circulo = {
    raio: 3,
    tipoCalculo: "A"
};

function calcularCirculo( { raio, tipoCalculo:tipo } ) {
    return Math.ceil( tipo == "A" ? Math.PI * Math.pow( raio, 2 ) : 2 * Math.PI * raio );
}

console.log(calcularCirculo( circulo ));

/** -------------------Exercicio 2------------------- */

function naoBissexto( ano ) {
    return (ano % 400 == 0 ) || ( ano % 4== 0 && 100 != 0) ? false : true;
}

function bissexto( ano ) {
    return ( ano %400 == 0 ) || ( ano %4 == 0 && ano %100 != 0) ? true : false;
}

console.log(naoBissexto( 2016 ));
console.log(bissexto( 2017 ));

/** -------------------Exercicio 3------------------- */

function somarPares( numeros ) {
    let resultado = 0;
    for (let i = 0; i < numeros.length; i+=2) {
        resultado += numeros[i];
    }
    return resultado;
}

console.log(somarPares( [1, 56, 4.34, 6, -2] ))

/** -------------------Exercicio 4------------------- */

// function adicionar( valor1 ) {
//     return function( valor2 ) {
//         return valor1 + valor2;
//     }
// }

let adicionar = valor1 => valor2 => valor1 + valor2;

console.log(adicionar(3)(4));

/** -------------------Exercicio Extra------------------- */

//currying
// 2 -> 4, 6, 8, 10

const divisao = divisor => numero => (numero / divisor);
const divisivelPor = divisao(2);

console.log( divisivelPor( 4 ) );
console.log( divisivelPor( 6 ) );
console.log( divisivelPor( 8 ) );
console.log( divisivelPor( 10 ) );

