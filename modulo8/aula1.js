// console.log("Chegou ate aqui!");

// /** VAR */
// console.log(nome);
// var nome = "Giovanni";
// var nome = "Giovanni F";
// console.log(nome);

// /** LET */
// let nome1 = "Giovanni";

// 	{
// 		let nome1 = "Giovanni";
// 		nome1 = "Giovanni F"
// 		console.log(nome1);
// 	}

// console.log(nome1);

// /** CONST */
// const pessoa = {
// 	nome: "Giovanni - Constante"
// };

// Object.freeze(pessoa);

// pessoa.nome = "Giovanni - Constante 2";
// //pessoa.idade = 31;

// console.log(pessoa.nome);
// //console.log(pessoa.idade);

// /** ESPETACULAR */

// console.log("Nossa que incrivel!!!!!");

// let soma = 1 + 2;
// let soma1 = "1" + 2 + 3;
// let soma2 = 1 + "2" + 3;
// let soma3 = 1 + 2 + "3";

// console.log(soma);
// console.log(soma1);
// console.log(soma2);
// console.log(soma3);

// /** FUNCOES */

// let nomeFuncao = "Giovanni";
// let idadeFuncao = 24;
// let semestre = 5;
// let notas = [10.0, 3.0, 5.0, 9.0];


// function funcaoCriarAluno( nomeFuncao, idadeFuncao, semestre, notas = [] ) {
// 	const aluno = {
// 		nome: nomeFuncao,
// 		idade: idadeFuncao,
// 		semestre: semestre,
// 		nota: notas
// 	};

// 	//Factory -> Design Pattern 
// 	function aprovadoOrReprovado( notas ) {
// 		if( notas.length == 0 ) {
// 			return "sem nota";
// 		}

// 		let somatoria = 0;
// 		for (let i = 0; i < notas.length; i++) {
// 			somatoria += notas[i];
// 		}

// 		return (somatoria / nota.length) > 7.0 ? "Aprovado" : "Reprovado"; 
// 	}

// 	aluno.status = aprovadoOrReprovado( notas );
// 	console.log(aluno);
// 	return aluno;
// }

// funcaoCriarAluno(nomeFuncao, idadeFuncao, semestre, notas);
// funcaoCriarAluno(nomeFuncao, idadeFuncao, semestre);

// let alunoExterno = funcaoCriarAluno(nomeFuncao, idadeFuncao, semestre, notas);
// console.log(alunoExterno);

// let i = 2;

// i == 2; // => true
// i == "2"; // => false

// i === 2;
// i === "2";

// /**----------- Template String -----------*/

// let texto = "Texto";
// let outrovalor = "Texto 2";

// console.log(texto + "Aqui eh o meio entre dos texto" + outrovalor + pessoa.nome);

// console.log(`${texto} Aqui eh o meio entre dois texto ${ outrovalor } - ${ pessoa.nome }`)


// /**----------- Destruction -----------*/

// let objeto = {
// 	nome: "Giovanni",
// 	idade: 31,
// 	altura: 1.85
// }

// const { nome:NomeCompleto,altura } = objeto;

// const arrayTeste = ['Gustado', 'Kevin', 'Victor', 'Arthur'];
// //a virgula eh pra pular as posicoes
// let [, posicao2,,posicao4] = arrayTeste;


// // let a = 1;
// // let b = 3;
// // let aux = a;

// // let a = b;
// // let b = aux

// // [a,b] =[b,a];


/**----------- Spread operator -----------*/

let arraySpred = [1, 77, 83, 42];
console.log( ...arraySpred );
console.log( {...arraySpred} );
console.log( {...[1, 77, 83, 42]} );
console.log( ..."MeuNome" );
console.log( {..."MeuNome"} );