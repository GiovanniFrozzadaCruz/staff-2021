const renderizar = ( pokemon ) => {
  const dadosPokemon = document.getElementById( 'dadosPokemon' );

  const nome = dadosPokemon.querySelector( '.nome' );
  nome.innerHTML = pokemon.nome;

  const imagem = dadosPokemon.querySelector( '.thumb' );
  imagem.src = pokemon.imagem;

  const altura = dadosPokemon.querySelector( '.altura' );
  altura.innerHTML = `Altura: ${ pokemon.altura }`;

  const id = dadosPokemon.querySelector( '.id' );
  id.innerHTML = `id:  + ${ pokemon.id }`;

  const weight = dadosPokemon.querySelector( '.peso' );
  weight.innerHTML = `peso:  + ${ pokemon.weight }`;

  this.tiposTabela( pokemon.tipos );

  this.estatisticasTabela( pokemon.estatisticas );
}

// eslint-disable-next-line no-unused-vars
function buscar() {
  const id = document.getElementById( 'input-pokemon' ).value;

  const pokeApi = new PokeApi();
  return pokeApi.buscarEspecifico( id )
    .then( pokemon => {
      const poke = new Pokemon( pokemon );
      renderizar( poke );
    } );
}

// eslint-disable-next-line no-unused-vars
function sortear() {
  const pokeApi = new PokeApi();
  // eslint-disable-next-line no-use-before-define
  const valorRandom = getRandom();

  return pokeApi.buscarEspecifico( valorRandom )
    .then( pokemon => {
      const poke = new Pokemon( pokemon );
      renderizar( poke );
    } );
}

function getRandom() {
  const min = Math.ceil( 1 );
  const max = Math.floor( 893 );
  return Math.floor( Math.random() * ( max - min ) ) + min;
}


// eslint-disable-next-line no-unused-vars
function limpar() {
  const dadosPokemon = document.getElementById( 'dadosPokemon' );

  const nome = dadosPokemon.querySelector( '.nome' );
  nome.innerHTML = '';

  const imagem = dadosPokemon.querySelector( '.thumb' );
  imagem.src = '';

  const altura = dadosPokemon.querySelector( '.altura' );
  altura.innerHTML = '';

  const id = dadosPokemon.querySelector( '.id' );
  id.innerHTML = '';

  const weight = dadosPokemon.querySelector( '.peso' );
  weight.innerHTML = '';

  const baseStat = dadosPokemon.querySelector( '.baseStats' );
  baseStat.innerHTML = '';
}

// eslint-disable-next-line no-unused-vars
function estatisticasTabela( estatisticas ) {
  const tbody = document.getElementById( 'tbodyStats' );
  tbody.innerText = '';

  for ( let i = 0; i < estatisticas.length; i++ ) {
    const tr = tbody.insertRow();

    const tdStat = tr.insertCell();
    const tdBase = tr.insertCell();

    tdStat.innerText = estatisticas[i].stat.name.toUpperCase().replace( '-', ' ' );
    tdBase.innerText = estatisticas[i].base_stat;
  }
}

// eslint-disable-next-line no-unused-vars
function tiposTabela( tipos ) {
  const tbody = document.getElementById( 'tbodyTipo' );
  tbody.innerText = '';
  for ( let i = 0; i < tipos.length; i++ ) {
    const tr = tbody.insertRow();
    const tdNomeTipo = tr.insertCell();
    tdNomeTipo.innerText = tipos[i].toUpperCase();
  }
}
