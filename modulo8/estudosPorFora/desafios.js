/**-------------- Exercicio 1 --------------

// let idade = prompt("Digite sua idade: ");

// if( idade >= 0 && idade < 15 ) {
//     document.write("Crianca");
// } else if( idade >= 15 && idade < 30 ) {
//     document.write("jovem");
// } else if( idade >= 30 && idade < 60 ) {
//     document.write("Adulto")
// } else if( idade >= 60 ) {
//     document.write("Idoso")
// }
*/

/**-------------- Exercicio 2 --------------

let nome = prompt("Qual seu nome?");
let altura = prompt("Qual sua altura em centimetros?");
let peso = prompt("Qual seu peso?");

let novaAltura = altura / 100;
let imc = Math.pow(altura, 2) / peso;

let classificacao = "";

if (imc < 16) {
    classificacao = "Baixo peso muito grave";
} else if (imc >= 16 && imc <= 16.99) {
    classificacao = "Baixo peso grave";
} else if (imc >= 17 && imc <= 18.49) {
    classificacao = "Baixo peso";
} else if (imc >= 18.50 && imc <= 24.99) {
    classificacao = "Peso normal";
} else if (imc >= 25 && imc <= 29.99) {
    classificacao = "Sobrepeso";
} else if (imc >= 30 && imc <= 34.99) {
    classificacao = "Obesidade grau I";
} else if (imc >= 35 && imc <= 39.99) {
    classificacao = "Obesidade grau II";
} else {
    classificacao = "Obesidade grau III"
}

document.write(`${nome} possui indice de massa corporal igual a ${imc}, sendo classificado como: ${classificacao}`);
*/


/**-------------- Exercicio 3 --------------
function frasesMotivacionais(numero) {
    switch (numero) {
        case 1:
            console.log("Frase 1")
            break;
        case 2:
            console.log("Frase 2")
            break;
        case 3:
            console.log("Frase 2")
            break;
        case 4:
            console.log("Frase 2")
            break;
        default:
            break;
    }
}
frasesMotivacionais()
*/