// /**============== Exemplo 1 */
// function calcularAreaTerreno( largura, comprimento ) {
//     return largura * comprimento;;
// }
// //let largura = prompt("Informe o valor da largura do terreno: ");
// //let comprimento = prompt("Informe o valor do comprimento do terreno: ");

// document.write(calcularAreaTerreno(largura,comprimento));

/** ============== Exemplo 2 
function soma( a, b ) {
    return a + b;
}

console.log(soma(7, 7));
console.log(soma(7, 7, 2, 14));

*/


/* ============== FUNCAO ANONIMA
var exerbirFuncao = function( nome ) {
    document.write(`Bom dia ${nome}, tudo bem?`);
}

exerbirFuncao("Giovanni");

*/


/* ============== FUNCAO CALLBACK 
function exibirArtigo( id, callbackSucesso, callbackErro ) {
    //logica: recuperar o id via requisicao http;
    if( id %2==0 ) {
        callbackSucesso('Funcoes de callback em JS', 'Funcoes de callback sao muito utilizadas....');
    } else {
        callbackErro('Erro ao recuperar os dados');
    }
}
let callbackSucesso = function( titulo, descricao ) {
    document.write("<h1>" + titulo + "</h1>");
    document.write("<p>" + descricao + "</p>");
}
let callbackErro = function(erro) {
    document.write("<p>" + erro + "</p>");

}
exibirArtigo(4, callbackSucesso, callbackErro );

*/

/* ============== ARROW FUNCTION 

const nome = ( nome ) => {
    console.log("antes de executar o callback")
    nome()
    console.log("depois de executar o callback")
}

nome( () => {
    console.log("estou em uma callback")
} )

*/


/* ==============  FUNCTION */

function Person( name, idade ) {
    this.name = name;
    this.idade = idade;
    this.study = function() {
        return "studying"
    }
}

const giovanni = new Person("Giovanni", 24);
const filipe = new Person("Filipe", 23);

console.log(giovanni);
console.log(filipe);
console.log(`Giovanni esta ${giovanni.study()}`);
console.log(`Filipe esta ${filipe.study()}`);
