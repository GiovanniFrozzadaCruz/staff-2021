// let nota = prompt("Digite a nota do aluno:");
// let faltas = prompt("Digite a quantiade de faltar:");

// let media = 7;
// let faltas_maximas = 15;

// // if( nota >= 7 && faltas <= faltas_maximas ) {
// //     document.write("Aprovado!");
// // } else {
// //     document.write("Reprovado");
// // }

// //let resultado = <condicao> ? <verdade> : <falso> ;
// let resultado = ( nota >= 7 && faltas <= faltas_maximas ) ? "Aprovado" : "Reprovado";
// document.write(resultado);

let aluno = ({
    nome: "Giovanni",
    idade: 24,
    endereco: [
        {
            tipoEndereco: [
                {
                    tipo: "casa"
                }
            ],
            rua: "Sao Domingos",
            pais: "Brasil",
        },
        {
            tipoEndereco: [
                {
                    tipo: "trabalho"
                }
            ],
            rua: "av protasio alves",
            pais: "Brasil",
        },
        {
            tipoEndereco: [
                {
                    tipo: "trabalho"
                }
            ],
            rua: "av andarai",
            pais: "Brasil",
        }
    ],
    peso: 75,
    altura: "bem alto"
})
console.log(aluno);
console.log(aluno.endereco);
//console.log(aluno.endereco.tipoEndereco);

for (let endereco of aluno.endereco) { 
    console.log(endereco.tipoEndereco);
}

console.log(`${aluno.nome} tem ${aluno.idade} anos`);
